package com.epam.rd.spring2019.homework_19.services.mapper;

import com.epam.rd.spring2019.homework_19.repository.domain.User;
import com.epam.rd.spring2019.homework_19.controllers.dto.UserDTO;
import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;

public class UserMapperTest {

    private UserMapper sut = new UserMapper();

    @Test
    public void toUser() {
        //GIVEN
        UserDTO dto = UserDTO.builder()
                .id("John Wall")
                .email("email")
                .firstName("John")
                .lastName("Wall")
                .registrationDate(LocalDateTime.now().toString())
                .build();
        //WHEN
        User user = sut.toUser(dto);
        //THEN
        assertEquals(dto.getId(), user.getId());
        assertEquals(dto.getEmail(), user.getEmail());
        assertEquals(dto.getFirstName(), user.getFirstName());
        assertEquals(dto.getLastName(), user.getLastName());
        assertEquals(dto.getRegistrationDate(), user.getRegistrationDate().toString());
    }

    @Test
    public void toUserDTO() {
        //GIVEN
        User user = User.builder()
                .id("John Wall")
                .firstName("John")
                .lastName("Wall")
                .email("email")
                .registrationDate(LocalDateTime.now())
                .build();
        //WHEN
        UserDTO dto = sut.toUserDTO(user);
        //THEN
        assertEquals(user.getId(), dto.getId());
        assertEquals(user.getEmail(), dto.getEmail());
        assertEquals(user.getFirstName(), dto.getFirstName());
        assertEquals(user.getLastName(), dto.getLastName());
        assertEquals(user.getRegistrationDate().toString(), dto.getRegistrationDate());
    }
}