package com.epam.rd.spring2019.homework_19.controllers;

import com.epam.rd.spring2019.homework_19.controllers.dto.UserDTO;
import com.epam.rd.spring2019.homework_19.exeption.RepositoryException;
import com.epam.rd.spring2019.homework_19.services.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;

import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;

import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest
@AutoConfigureMockMvc
public class UserControllerTest {

    @MockBean
    private UserService userService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    private UserDTO dto = UserDTO.builder()
            .id("John Wall")
            .firstName("John")
            .lastName("Wall")
            .registrationDate("2019-05-16")
            .email("john_wall@email.com")
            .build();

    @Test
    public void getAll() throws Exception {
        //GIVEN
        UserDTO first = dto;
        UserDTO second = UserDTO.builder()
                .id("James Harden")
                .firstName("James")
                .lastName("Harden")
                .registrationDate("2019-03-15")
                .email("james_harden@email.com")
                .build();

        given(this.userService.getAll())
                .willReturn(Arrays.asList(first, second));
        //WHEN
        mockMvc.perform(get("/user")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(2)))
                .andExpect(jsonPath("$[0].id", Matchers.is(first.getId())))
                .andExpect(jsonPath("$[0].email", Matchers.is(first.getEmail())))
                .andExpect(jsonPath("$[0].firstName", Matchers.is(first.getFirstName())))
                .andExpect(jsonPath("$[0].lastName", Matchers.is(first.getLastName())))
                .andExpect(jsonPath("$[0].registrationDate", Matchers.is(first.getRegistrationDate())))
                .andExpect(jsonPath("$[1].id", Matchers.is(second.getId())))
                .andExpect(jsonPath("$[1].email", Matchers.is(second.getEmail())))
                .andExpect(jsonPath("$[1].firstName", Matchers.is(second.getFirstName())))
                .andExpect(jsonPath("$[1].lastName", Matchers.is(second.getLastName())))
                .andExpect(jsonPath("$[1].registrationDate", Matchers.is(second.getRegistrationDate())));
    }

    @Test
    public void create() throws Exception {
        //GIVEN
        given(this.userService.create(Mockito.eq(dto)))
                .willReturn(dto);
        //WHEN
        mockMvc.perform(post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", Matchers.is(dto.getId())))
                .andExpect(jsonPath("$.email", Matchers.is(dto.getEmail())))
                .andExpect(jsonPath("$.firstName", Matchers.is(dto.getFirstName())))
                .andExpect(jsonPath("$.lastName", Matchers.is(dto.getLastName())))
                .andExpect(jsonPath("$.registrationDate", Matchers.is(dto.getRegistrationDate())));
    }

    @Test
    public void update() throws Exception {
        //GIVEN
        given(this.userService.updateById(Mockito.eq(dto.getId()), Mockito.eq(dto)))
                .willReturn(dto);
        //WHEN
        mockMvc.perform(put("/user/" + dto.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", Matchers.is(dto.getId())))
                .andExpect(jsonPath("$.email", Matchers.is(dto.getEmail())))
                .andExpect(jsonPath("$.firstName", Matchers.is(dto.getFirstName())))
                .andExpect(jsonPath("$.lastName", Matchers.is(dto.getLastName())))
                .andExpect(jsonPath("$.registrationDate", Matchers.is(dto.getRegistrationDate())));
    }

    @Test
    public void delete() throws Exception {
        //GIVEN
        given(this.userService.deleteById(Mockito.eq(dto.getId())))
                .willReturn(dto.getId());
        //WHEN
        mockMvc.perform(MockMvcRequestBuilders.delete("/user/" + dto.getId()))
                .andExpect(status().isOk());
    }

    @Test
    public void exceptionHandler() throws Exception {
        //GIVEN
        given(this.userService.getAll()).willThrow(new RepositoryException("message"));
        //WHEN
        mockMvc.perform(get("/user"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$", Matchers.is("message")));
    }

}