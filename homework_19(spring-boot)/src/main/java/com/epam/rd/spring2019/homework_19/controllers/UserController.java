package com.epam.rd.spring2019.homework_19.controllers;

import com.epam.rd.spring2019.homework_19.controllers.dto.UserDTO;
import com.epam.rd.spring2019.homework_19.exeption.RepositoryException;
import com.epam.rd.spring2019.homework_19.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService service;

    @ExceptionHandler(RepositoryException.class)
    public ResponseEntity<String> handler(RepositoryException ex){
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @GetMapping
    public Collection<UserDTO> getAll(){
        return service.getAll();
    }

    @PostMapping
    public ResponseEntity<UserDTO> create(@RequestBody UserDTO userDTO){
        UserDTO user = service.create(userDTO);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<UserDTO> updateById(@PathVariable String id, @RequestBody UserDTO userDTO){
        UserDTO user = service.updateById(id, userDTO);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity deleteById(@PathVariable String id){
        service.deleteById(id);
        return new ResponseEntity(HttpStatus.OK);
    }

}
