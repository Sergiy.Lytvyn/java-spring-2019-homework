package com.epam.rd.spring2019.homework_19.services;

import com.epam.rd.spring2019.homework_19.controllers.dto.UserDTO;

import java.util.Collection;

public interface UserService {

    UserDTO create(UserDTO dto);

    UserDTO updateById(String id, UserDTO dto);

    String deleteById(String id);

    Collection<UserDTO> getAll();
}
