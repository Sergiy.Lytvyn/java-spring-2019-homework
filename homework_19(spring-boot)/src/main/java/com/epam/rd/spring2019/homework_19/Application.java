package com.epam.rd.spring2019.homework_19;

import com.epam.rd.spring2019.homework_19.repository.domain.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class Application {

    @Bean
    public Map<String, User> userMap(){
        return new HashMap<>();
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
