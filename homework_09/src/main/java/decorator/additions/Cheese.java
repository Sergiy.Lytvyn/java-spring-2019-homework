package decorator.additions;

import decorator.Pizza;
import decorator.PizzaDecorator;

public class Cheese extends PizzaDecorator {

    public Cheese(Pizza pizza) {
        super(pizza);
    }

    @Override
    public Double cost() {
        return pizza.cost() + 0.2;
    }

}
