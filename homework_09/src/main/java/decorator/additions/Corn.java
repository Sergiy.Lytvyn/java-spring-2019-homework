package decorator.additions;

import decorator.Pizza;
import decorator.PizzaDecorator;

public class Corn extends PizzaDecorator {

    public Corn(Pizza pizza) {
        super(pizza);
    }

    @Override
    public Double cost() {
        return pizza.cost() + 0.15;
    }

}
