package decorator.additions;

import decorator.Pizza;
import decorator.PizzaDecorator;

public class Pineapple extends PizzaDecorator {

    public Pineapple(Pizza pizza) {
        super(pizza);
    }

    @Override
    public Double cost() {
        return pizza.cost() + 0.1;
    }
}
