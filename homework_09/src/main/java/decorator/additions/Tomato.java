package decorator.additions;

import decorator.Pizza;
import decorator.PizzaDecorator;

public class Tomato extends PizzaDecorator {

    public Tomato(Pizza pizza) {
        super(pizza);
    }

    @Override
    public Double cost() {
        return pizza.cost() + 0.15;
    }

}
