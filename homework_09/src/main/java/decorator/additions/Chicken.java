package decorator.additions;

import decorator.Pizza;
import decorator.PizzaDecorator;

public class Chicken extends PizzaDecorator {

    public Chicken(Pizza pizza) {
        super(pizza);
    }

    @Override
    public Double cost() {
        return pizza.cost() + 0.3;
    }

}
