package decorator.additions;

import decorator.Pizza;
import decorator.PizzaDecorator;

public class Beef extends PizzaDecorator {

    public Beef(Pizza pizza) {
        super(pizza);
    }

    @Override
    public Double cost() {
        return pizza.cost() + 0.3;
    }
}
