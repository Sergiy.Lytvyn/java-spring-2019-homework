package decorator.additions;

import decorator.Pizza;
import decorator.PizzaDecorator;

public class Salami extends PizzaDecorator {

    public Salami(Pizza pizza) {
        super(pizza);
    }

    @Override
    public Double cost() {
        return pizza.cost() + 0.3;
    }

}
