package decorator;

public abstract class Pizza {

    public abstract Double cost();

    public abstract String getDescription();

}
