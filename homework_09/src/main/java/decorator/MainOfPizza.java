package decorator;

public class MainOfPizza extends Pizza {

    public MainOfPizza(){
    }

    @Override
    public Double cost() {
        return 0.3;
    }

    @Override
    public String getDescription() {
        return "Main of pizza and sauce";
    }
}
