package strategy.ticketType;

import factoryMethod.Ticket;

public interface TicketType {

    void saveTicket(Ticket ticket);

}
