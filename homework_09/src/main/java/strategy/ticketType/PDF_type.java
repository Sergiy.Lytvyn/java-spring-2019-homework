package strategy.ticketType;

import factoryMethod.Ticket;

public class PDF_type implements TicketType {

    @Override
    public void saveTicket(Ticket ticket) {
        System.out.println("ticket save like ticket.pdf");
    }
}
