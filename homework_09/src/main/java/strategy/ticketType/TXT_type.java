package strategy.ticketType;

import factoryMethod.Ticket;

public class TXT_type implements TicketType {

    @Override
    public void saveTicket(Ticket ticket) {
        System.out.println("ticket saved like ticket.txt");
    }
}
