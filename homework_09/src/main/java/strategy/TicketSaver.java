package strategy;

import factoryMethod.Ticket;
import strategy.ticketType.TicketType;

public class TicketSaver {

    private TicketType ticketType;

    public TicketSaver(TicketType ticketType) {
        this.ticketType = ticketType;
    }

    public void setTicketType(TicketType ticketType){
        this.ticketType = ticketType;
    }

    public void saveTicket(Ticket ticket){
        ticketType.saveTicket(ticket);
    }

}
