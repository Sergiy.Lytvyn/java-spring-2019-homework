Идея:
Приложение, которое сохраняет билет.
Отчёт может быть разных форматов.
Реализация:
TicketPrinter имеет поле ticketType тип которого являеться интерфейсом TicketType.
Данный интерфейс имеет один метод printTicket(Ticket ticket).
Существуют также два класса которые реализуют TicketType - это PDF_type и TXT_type.
Следовательно при создании обьекта класс TicketPrinter мы должны задать в конструкторе
тип класса, который будет определять формат билета.
Так же возможно динамически сменить формат билета с помощью метода
setTicketType(TicketType ticketType).
Вся реализация сохранения билета инкапсулирована в классах которые реализуют
интерфейс TicketType. При добавлении новых форматов отчётов, не нужно изменять
код TicketPrinter.

Idea:
Service that can save ticket in different type (in this case pdf, txt)
Implementation:
TicketSaver has field TicketType that must be defined in constructor,
but after we can switch it with method setTicketType();
TicketSaver's method saveTicket(...) delegates to TicketType.
If we will create new type for example DOC_type, TicketSaver's code
won't be changed.
