package builder;

public class Passenger {

    final private String firstName;
    final private String lastName;
    final private Integer age;
    final private Boolean isMale;
    final private Boolean isStudent;
    final private Long studentID;
    final private Long passportID;
    final private Long cartId;
    final private String email;

    private Passenger(String firstName, String lastName, Integer age, Boolean isMale, Boolean isStudent,
                      Long studentID, Long passportID, Long cartId, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.isMale = isMale;
        this.isStudent = isStudent;
        this.studentID = studentID;
        this.passportID = passportID;
        this.cartId = cartId;
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Integer getAge() {
        return age;
    }

    public Boolean getMale() {
        return isMale;
    }

    public Boolean getStudent() {
        return isStudent;
    }

    public Long getStudentID() {
        return studentID;
    }

    public Long getPassportID() {
        return passportID;
    }

    public Long getCartId() {
        return cartId;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return "Passenger{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", isMale=" + isMale +
                ", isStudent=" + isStudent +
                ", studentID=" + studentID +
                ", passportID=" + passportID +
                ", cartId=" + cartId +
                ", email='" + email + '\'' +
                '}';
    }

    public static class PassengerBuilder{

        private String firstName;
        private String lastName;
        private Integer age;
        private Boolean isMale;
        private Boolean isStudent;
        private Long studentID;
        private Long passportID;
        private Long cartID;
        private String email;

        public PassengerBuilder(){
        }

        public PassengerBuilder(String firstName, String lastName){
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public PassengerBuilder fisrtName(String firstName){
            this.firstName = firstName;
            return this;
        }

        public PassengerBuilder lastName(String lastName){
            this.lastName = lastName;
            return this;
        }

        public PassengerBuilder age(Integer age){
            this.age = age;
            return this;
        }

        public PassengerBuilder isMale(Boolean isMale){
            this.isMale = isMale;
            return this;
        }

        public PassengerBuilder isStudent(Boolean isStudent){
            this.isStudent = isStudent;
            return this;
        }

        public PassengerBuilder studentID(Long studentID){
            this.studentID = studentID;
            return this;
        }

        public PassengerBuilder passportID(Long passportID){
            this.passportID = passportID;
            return this;
        }

        public PassengerBuilder cartID(Long cartID){
            this.cartID = cartID;
            return this;
        }

        public PassengerBuilder email(String email){
            this.email = email;
            return this;
        }

        public Passenger build(){
            return new Passenger(firstName, lastName, age, isMale, isStudent, studentID, passportID, cartID, email);
        }

    }

}
