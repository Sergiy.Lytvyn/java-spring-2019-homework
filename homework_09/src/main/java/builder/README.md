Idea:
We have a website where the user can buy ticket to different types of transport.
Problem:
Every ticket create only for passenger.
Passenger has nine fields (first name, last name, age, passport ID, cart ID, phone number and others).
If we create a new instance with the keyword 'new', it will be badly for read.
For some travelers, we do not use some fields, for example:
if the passenger isn't a student, then the field studentID is redundant.
Solution:
In this case I create nested static class PassengerBuilder.
To create a passenger we use 'new Passenger.PassengerBuilder ()',
than we add other fields, at the and we need to use method build().
After that we will have new passenger with the fields that we identified.