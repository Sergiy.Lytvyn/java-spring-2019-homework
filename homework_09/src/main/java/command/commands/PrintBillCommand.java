package command.commands;

import command.priceList.Bill;
import java.util.List;

public class PrintBillCommand implements Command {

    private Bill bill;

    public PrintBillCommand(Bill bill) {
        this.bill = bill;
    }

    @Override
    public void execute() {
        List productList = bill.getProductList();
        productList.forEach(System.out::println);
        System.out.println(bill.getSum());
    }

    @Override
    public void undo() {
        System.out.println("STOP printing bill");
    }

}
