package command.commands;

import command.pojo.Product;
import command.priceList.Bill;

public class AddProductCommand implements Command{

    private Bill bill;
    private Product product;

    public AddProductCommand(Bill bill, Product product){
        this.bill = bill;
        this.product = product;
    }

    @Override
    public void execute() {
        bill.addProduct(product);
    }

    @Override
    public void undo() {
        bill.removeProduct(product);
    }
}
