package command;

import command.commands.Command;

public class CashMachine {

    private Command command;

    public CashMachine(Command command) {
        this.command = command;
    }

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }

    public void buttonWasPressed(){
        command.execute();
    }

    public void undoButtonWasPreased(){
        command.undo();
    }
}
