package command.priceList;

import command.pojo.Product;

import java.util.ArrayList;
import java.util.List;

public class Bill implements PriceList {

    private List<Product> productList;
    private Double sum;

    public Bill(){
        productList = new ArrayList<>();
        sum = 0.0;
    }

    @Override
    public void addProduct(Product product) {
        productList.add(product);
        sum = sum + product.getPrice();
    }

    @Override
    public void removeProduct(Product product) {
        productList.remove(product);
        sum = sum - product.getPrice();
    }

    @Override
    public Double getSum() {
        return sum;
    }

    public List<Product> getProductList() {
        return productList;
    }
}
