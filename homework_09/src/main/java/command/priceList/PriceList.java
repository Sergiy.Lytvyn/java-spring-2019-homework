package command.priceList;

import command.pojo.Product;

public interface PriceList {

    void addProduct(Product product);

    void removeProduct(Product product);

    Double getSum();
}
