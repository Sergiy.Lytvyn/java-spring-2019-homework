Idea:
Create simple cash machine like for supermarket.
Implementation:
I create pojo class - Product that has two fields: name and price,
create interface PriceList and Bill(implementation).
Package commands includes interface Command, class PrintBillCommand and
AddProductCommand(both implementation).
Cash machine has two button : button and undo.
Cash machine delegate to field command.
At result cash machine can be programed for different command.
Method setCommand(...) switches commands.


