package factoryMethod;

import builder.Passenger;

public class PlaneTicket extends Ticket {

    public PlaneTicket(Passenger passenger, String departure, String destination) {
        super(passenger, departure, destination);
    }

}
