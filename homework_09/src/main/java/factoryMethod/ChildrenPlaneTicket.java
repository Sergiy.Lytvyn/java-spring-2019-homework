package factoryMethod;

import builder.Passenger;

public class ChildrenPlaneTicket extends PlaneTicket {

    public ChildrenPlaneTicket(Passenger passenger, String departure, String destination) {
        super(passenger, departure, destination);
        this.setDiscountCoefficient(0.9);
    }

}
