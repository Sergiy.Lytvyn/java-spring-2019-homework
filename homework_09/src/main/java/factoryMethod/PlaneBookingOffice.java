package factoryMethod;

import builder.Passenger;

public class PlaneBookingOffice extends BookingOffice {

    public PlaneBookingOffice() {
    }

    @Override
    protected Ticket createNewTicket(Passenger passenger, String destination, String departure) {
        if( passenger.getAge() < 16 ){
            return new ChildrenPlaneTicket(passenger, destination, departure);
        } if ( passenger.getStudent() ){
            return new StudentPlaneTicket(passenger, destination ,departure);
        }
        else {
            return new PlaneTicket(passenger, destination, departure);
        }
    }

    @Override
    protected Double calculatePrice(Integer distance, Ticket ticket) {
        return distance * 1.2 * ticket.getDiscountCoefficient();
    }
}
