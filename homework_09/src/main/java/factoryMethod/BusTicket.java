package factoryMethod;

import builder.Passenger;

public class BusTicket extends Ticket {

    public BusTicket(Passenger passenger, String departure, String destination) {
        super(passenger, departure, destination);
    }

}
