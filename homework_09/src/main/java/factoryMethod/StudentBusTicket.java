package factoryMethod;

import builder.Passenger;

public class StudentBusTicket extends Ticket {

    public StudentBusTicket(Passenger passenger, String departure, String destination) {
        super(passenger, departure, destination);
        this.setDiscountCoefficient(0.6);
    }

}
