package factoryMethod;

import builder.Passenger;

public class StudentPlaneTicket extends Ticket {

    public StudentPlaneTicket(Passenger passenger, String departure, String destination) {
        super(passenger, departure, destination);
        this.setDiscountCoefficient(0.95);
    }

}
