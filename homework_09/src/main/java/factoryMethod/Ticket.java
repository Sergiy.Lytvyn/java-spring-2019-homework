package factoryMethod;

import builder.Passenger;

public abstract class Ticket {

    final private Passenger passenger;
    final private String destination;
    final private String departure;
    private Double discountCoefficient;
    private Double price;

    public Ticket(Passenger passenger, String departure, String destination){
        this.passenger = passenger;
        this.departure = departure;
        this.destination = destination;
        this.discountCoefficient = 1.0;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public String getDestination() {
        return destination;
    }

    public String getDeparture() {
        return departure;
    }

    public Double getPrice() {
        return price;
    }

    public Double getDiscountCoefficient() {
        return discountCoefficient;
    }

    void setPrice(Double price) {
        this.price = price;
    }

    void setDiscountCoefficient(Double discountCoefficient) {
        this.discountCoefficient = discountCoefficient;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "passenger=" + passenger +
                ", destination='" + destination + '\'' +
                ", departure='" + departure + '\'' +
                ", price=" + price +
                ", discountCoefficient=" + discountCoefficient +
                '}';
    }
}
