package factoryMethod;

import builder.Passenger;

public class ChildrenBusTicket extends BusTicket {

    public ChildrenBusTicket(Passenger passenger, String departure, String destination) {
        super(passenger, departure, destination);
        this.setDiscountCoefficient(0.7);
    }

}
