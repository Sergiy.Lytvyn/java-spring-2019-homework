package factoryMethod;

import builder.Passenger;

public class BusBookingOffice extends BookingOffice{

    public BusBookingOffice() {
    }

    @Override
    protected Ticket createNewTicket(Passenger passenger, String destination, String departure) {
        if(passenger.getAge() < 14){
            return new ChildrenBusTicket(passenger, destination, departure);
        } if (passenger.getStudent()){
            return new StudentBusTicket(passenger, destination, departure);
        } else {
            return new BusTicket(passenger, destination, departure);
        }
    }

    @Override
    protected Double calculatePrice(Integer distance, Ticket ticket) {
        return distance * 1.0 * ticket.getDiscountCoefficient();
    }


}
