package factoryMethod;

import builder.Passenger;

public abstract class BookingOffice {

    protected abstract Ticket createNewTicket(Passenger passenger, String destination, String departure);

    protected abstract Double calculatePrice(Integer distance, Ticket ticket);

    private Integer getDistance(String destination, String departure){
        //Here must be algorithm of find distance between departure
        // and destination. I return 300, because it doesn't compile
        return 300;
    }

    public Ticket getNewTicket(Passenger passenger, String destination, String departure){
        Ticket ticket = createNewTicket(passenger, destination, departure);
        Integer distance = getDistance(destination, departure);
        Double price = calculatePrice(distance, ticket);
        ticket.setPrice(price);
        return ticket;
    }

}
