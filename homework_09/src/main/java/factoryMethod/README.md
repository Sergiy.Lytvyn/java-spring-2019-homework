Идея:
Фабрика для создания билетов на разные виды транспорта.
Реализация:
В пакете entity находиться класс Passenger который являеться обьектом
для создания билета.
В иерархии билетов находиться абстрактный класс Ticket, который наследуют
классы BusTicket и PlaneTicket в которых определяеться алгоритм для
определения растояния и стоимости перевозки.
Также в иерархии билетов находиться два субкласса ChildrenBusTicket
и ChildrenPlaneTicket, которые переопределяют цену билета.
В абстрактном BookingOffice обьявлен абстрактный фабричный метод
createNewTicket(Passenger passenger, String destination, String departure),
также в классе определен клиентский метод getNewTicket(...),
который устанавливает цену для нового билета за входящими параметрами.
В классах BusBookingOffice и PlaneBookingOffice, которые наследуют
абстрактный класс BookingOffice определен алгоритм создания билета для
определенного вида транспорта и возрастной категории пассажира.

Примечание: в классах BusTicket и PlaneTicket метод getDistance()
должен содержать алгоритм по определению расстаяния перевозки.
Он не вынесен в абстрактный класс, потому что маршрут между
транспортными терминалами разных видов транспорта отличаеться.

Idea:
Factory for creating ticket for different kind of transport.
Implementation:
I create two hierarchies
1) Ticker hierarchy - POJO
2) Booking Office hierarchies - Factory that implements factory method
BookingOffice - abstract class that has two protected abstract methods
calculatePrice(...) and createNewTicket(...), and public method getNewTicket(...).
Algorithm of creating new ticket and set price for it is located in
method getNewTicket(...).