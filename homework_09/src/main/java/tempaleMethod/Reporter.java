package tempaleMethod;

public abstract class Reporter{

    public Reporter(){
    }

    final public void sendReport(){
        String info = getMediumInfo();
        createFile(info);
        makeStamp();
        sendReportOnEmail();
    }

    abstract String getMediumInfo();

    private void createFile(String info){
        System.out.println("Creating file and write " + info);
    }

    private void makeStamp(){
        System.out.println("Stamp it");
    }

    private void sendReportOnEmail(){
        System.out.println("Report was send");
    }

}
