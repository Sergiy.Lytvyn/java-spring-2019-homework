Idea:
Report mechanism for different user in sales site.(producer, consumer)
Implementation:
I create abstract class Reporter and two implementation:
ConsumerReporter and ProducerReporter.
Class Reporter has template method final public void sendReport()
that contain algorithm of sending message with report.
And Reporter has abstract method getMediumInfo() that must be override
in subclass.
Finally algorithm in sendReport() depends of override method in
subclass. Other steps of algorithm are same for all subclasses,
that why they located in Reporter class.