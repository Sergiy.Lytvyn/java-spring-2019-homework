package entity;

import java.util.Objects;

public class Student {

    private Integer ID;
    private String firstName;
    private String lastName;
    private Integer groupID;

    public Student() {
    }

    public Student(Integer ID, String firstName, String lastName, Integer groupID) {
        this.ID = ID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.groupID = groupID;
    }

    public Student(String firstName, String lastName, Integer groupID){
        this.firstName = firstName;
        this.lastName = lastName;
        this.groupID = groupID;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Integer getGroupID() {
        return groupID;
    }

    public Integer getID() {
        return ID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        Student student = (Student) o;
        return getID().equals(student.getID()) &&
                getFirstName().equals(student.getFirstName()) &&
                getLastName().equals(student.getLastName()) &&
                getGroupID().equals(student.getGroupID());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getID(), getFirstName(), getLastName(), getGroupID());
    }

    @Override
    public String toString() {
        return "Student{" +
                "ID=" + ID +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", groupID='" + groupID + '\'' +
                '}';
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public void setGroupID(Integer groupID) {
        this.groupID = groupID;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
