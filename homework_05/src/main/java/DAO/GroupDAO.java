package DAO;

import entity.Group;
import exceptions.CRUDException;
import util.ConnectionFactory;
import java.sql.*;
import java.util.*;

public class GroupDAO extends ConnectionFactory implements DAO<Group>  {

    public GroupDAO(){

    }

    private Group extractGroupFromResultSet(ResultSet rs) throws SQLException {
        Group group = new Group();
        group.setID(rs.getInt("ID"));
        group.setName(rs.getString("name"));
        return group;
    }

    @Override
    public Group getById(int id) {
        String query = "SELECT * FROM student_group WHERE ID = " + id;
        try (Connection connection = getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            if(resultSet.next()){
               return extractGroupFromResultSet(resultSet);
            }
        } catch (SQLException e){
            throw new CRUDException();
        }
        return null;
    }

    @Override
    public Set<Group> getAll() {
        String query = "SELECT * FROM student_group";
        Set<Group> groups = new HashSet<>();
        try(Connection connection = getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()){
               Group group = extractGroupFromResultSet(resultSet);
               groups.add(group);
            }
        } catch (SQLException e){
            throw new CRUDException();
        }
        return groups;
    }

    @Override
    public void insert(Group group) {
        final String query = "INSERT INTO student_group(name) VALUES(?)";
        try (Connection connection = getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, group.getName());
            preparedStatement.executeUpdate();
        } catch (SQLException e){
            throw new CRUDException();
        }
    }

    @Override
    public void insertAll(Collection<Group> collection) {
        final String query = "INSERT INTO student_group(name) VALUES(?)";
        try (Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            for (Group group : collection) {
                preparedStatement.setString(1, group.getName());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e){
            throw new CRUDException();
        }
    }

    @Override
    public void updateById(Group group) {
        final String query = "UPDATE student_group SET name = ? WHERE ID = ?";
        try(Connection connection = getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, group.getName());
            preparedStatement.setInt(2, group.getID());
            preparedStatement.executeUpdate();
        } catch (SQLException e){
            throw new CRUDException();
        }
    }

    @Override
    public void updateAllById(Collection<Group> collection) {
        final String query = "UPDATE student_group SET name = ? WHERE ID = ?";
        try (Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            for(Group group : collection) {
                preparedStatement.setString(1, group.getName());
                preparedStatement.setInt(2, group.getID());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e){
            throw new CRUDException();
        }
    }

    @Override
    public void deleteById(int id) {
        final String query = "DELETE FROM student_group WHERE ID = ?";
        try (Connection connection = getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e){
            throw new CRUDException();
        }
    }

}
