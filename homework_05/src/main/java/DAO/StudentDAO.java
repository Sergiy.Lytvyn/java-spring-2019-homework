package DAO;

import entity.Student;
import exceptions.CRUDException;
import util.ConnectionFactory;
import java.sql.*;
import java.util.*;

public class StudentDAO extends ConnectionFactory implements DAO<Student> {

    public StudentDAO(){

    }

    private Student extractStudentFromResultSet(ResultSet rs) throws SQLException {
        Student student = new Student();
        student.setID(rs.getInt("ID"));
        student.setFirstName(rs.getString("first_name"));
        student.setLastName(rs.getString("last_name"));
        student.setGroupID(rs.getInt("group_ID"));
        return student;
    }

    @Override
    public Student getById(int id) {
        String query = "SELECT * FROM students WHERE ID =" + id;
        try(Connection connection = getConnection()){
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            if (resultSet.next()) {
                return extractStudentFromResultSet(resultSet);
            }
        } catch (SQLException e){
            throw new CRUDException();
        }
        return null;
    }

    @Override
    public Set<Student> getAll() {
        Set<Student> students = new HashSet<>();
        String query = "SELECT * FROM students";
        try(Connection connection = getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()){
                Student student = extractStudentFromResultSet(resultSet);
                students.add(student);
            }
        } catch (SQLException e){
            throw new CRUDException();
        }
        return students;
    }

    @Override
    public void insert(Student student) {
        final String query = "INSERT INTO students(first_name, last_name, group_ID) VALUES(?, ?, ?)";
        try(Connection connection = getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, student.getFirstName());
            preparedStatement.setString(2, student.getLastName());
            preparedStatement.setInt(3, student.getGroupID());
            preparedStatement.executeUpdate();
        } catch (SQLException e){
            throw new CRUDException();
        }
    }

    @Override
    public void insertAll(Collection<Student> collection) {
        final String query = "INSERT INTO students(first_name, last_name, group_ID) VALUES(?, ?, ?)";
        try(Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            for (Student student : collection){
                preparedStatement.setString(1, student.getFirstName());
                preparedStatement.setString(2, student.getLastName());
                preparedStatement.setInt(3, student.getGroupID());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e){
            throw new CRUDException();
        }
    }

    @Override
    public void updateById(Student student) {
        final String query = "UPDATE students SET first_name = ?, last_name = ?, " +
                "group_ID = ? WHERE ID = ?";
        try(Connection connection = getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, student.getFirstName());
            preparedStatement.setString(2, student.getLastName());
            preparedStatement.setInt(3, student.getGroupID());
            preparedStatement.setInt(4, student.getID());
            preparedStatement.executeUpdate();
        } catch (SQLException e){
            throw new CRUDException();
        }
    }

    @Override
    public void updateAllById(Collection<Student> collection) {
        final String query = "UPDATE students SET first_name = ?, last_name = ?," +
                "group_ID = ? WHERE ID = ?";
        try (Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            for(Student student : collection) {
                preparedStatement.setString(1, student.getFirstName());
                preparedStatement.setString(2, student.getLastName());
                preparedStatement.setInt(3, student.getGroupID());
                preparedStatement.setInt(4, student.getID());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e){
            throw new CRUDException();
        }
    }

    @Override
    public void deleteById(int id) {
        final String query = "DELETE FROM students WHERE ID = ?";
        try (Connection connection = getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e){
            throw new CRUDException();
        }
    }

}
