package DAO;

import java.sql.SQLException;
import java.util.*;

public interface DAO<T> {
    T getById(int id) throws SQLException;

    Set<T> getAll() throws SQLException;

    void insert(T t) throws SQLException;

    void insertAll(Collection<T> collection) throws SQLException;

    void updateById(T group) throws SQLException;

    void updateAllById(Collection<T> collection) throws SQLException;

    void deleteById(int id) throws SQLException;
}
