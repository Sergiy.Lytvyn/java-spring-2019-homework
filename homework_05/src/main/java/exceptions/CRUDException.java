package exceptions;

public class CRUDException extends RuntimeException {

    public CRUDException (){
        super();
    }

    public CRUDException (String mess){
        super(mess);
    }

}
