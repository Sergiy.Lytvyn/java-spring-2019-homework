package util;

import java.io.*;
import java.sql.*;
import java.util.*;

public abstract class ConnectionFactory {

    private String user;
    private String password;
    private String url;
    private String driver;

    public ConnectionFactory(){

    }

    private void setProperties(){
        Properties property = new Properties();
        try (FileInputStream inputStream = new FileInputStream("src/main/resources/db/db.properties")){
            property.load(inputStream);
            user = property.getProperty("db.user");
            driver = property.getProperty("db.driver");
            url = property.getProperty("db.url");
            password = property.getProperty("db.password");
        } catch (IOException ex){
            throw new RuntimeException("Can't find properties file");
        }
    }

    public Connection getConnection() throws SQLException{
        setProperties();
        try {
            Class.forName(driver);
            return DriverManager.getConnection(url, user, password);
        } catch (ClassNotFoundException ex){
            throw new RuntimeException("Can't find JDBC driver");
        }
    }

}
