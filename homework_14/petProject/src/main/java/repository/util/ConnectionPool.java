package repository.util;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionPool {

    private static HikariConfig config =  new HikariConfig();
    private static HikariDataSource ds;

    public ConnectionPool() {}

    private static void setProperties(){
        Properties property = new Properties();
        try {
            InputStream inputStream = ConnectionPool.class.getClassLoader().getResourceAsStream("db/db.properties");
            property.load(inputStream);
            config.setDriverClassName(property.getProperty("dataSourceDriverName"));
            config.setJdbcUrl(property.getProperty("jdbcUrl"));
            config.setUsername(property.getProperty("dataSource.user"));
            config.setPassword(property.getProperty("dataSource.password"));
        } catch (IOException e) {
            throw new RuntimeException("Can not load properties");
        }
    }

    public static Connection getConnection() throws SQLException {
        if(ds == null){
            setProperties();
            ds = new HikariDataSource(config);
        }
        return ds.getConnection();
    }

}
