package repository.dao;

import repository.entity.Driver;
import repository.util.ConnectionPool;

import java.sql.*;
import java.util.Optional;

public class DriverDAO implements DAO<Driver> {

    private static DriverDAO driverDAO;

    public DriverDAO(){
    }

    public static DriverDAO getInstance(){
        if (driverDAO == null){
            synchronized (DriverDAO.class){
                if (driverDAO == null){
                    driverDAO = new DriverDAO();
                }
            }
        }
        return driverDAO;
    }

    private Driver getDriverFromRS(ResultSet resultSet) throws SQLException {
        Driver driver = new Driver();
        Integer id = resultSet.getInt("driver_ID");
        String email = resultSet.getString("driver_email");
        String password = resultSet.getString("driver_password");
        String name = resultSet.getString("driver_name");
        driver.setDriverID(id);
        driver.setEmail(email);
        driver.setPassword(password);
        driver.setName(name);
        return driver;
    }

    @Override
    public void create(Driver driver) throws SQLException {
        final String CREATE_QUERY = "INSERT INTO drivers(driver_email, driver_password, driver_name) VALUES (?,?,?) ";
        try (Connection connection = ConnectionPool.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(CREATE_QUERY);
            preparedStatement.setString(1, driver.getEmail());
            preparedStatement.setString(2, driver.getPassword());
            preparedStatement.setString(3, driver.getName());
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public void update(Driver driver) throws SQLException{

    }

    @Override
    public Optional<Driver> getByEmail(String email) throws SQLException{
        final String GET_BY_EMAIL = "SELECT * FROM drivers WHERE driver_email = '"+ email + "';";
        try (Connection connection = ConnectionPool.getConnection()){
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(GET_BY_EMAIL);
            if (resultSet.next()){
                return Optional.of(getDriverFromRS(resultSet));
            } else {
                return Optional.empty();
            }
        }
    }

    @Override
    public void deleteByEmail(String email) throws SQLException{
        final String DELETE_BY_EMAIL = "DELETE FROM drivers WHERE driver_email = '" + email + "';";
        try (Connection connection = ConnectionPool.getConnection()){
            Statement statement = connection.createStatement();
            statement.execute(DELETE_BY_EMAIL);
        }
    }
}
