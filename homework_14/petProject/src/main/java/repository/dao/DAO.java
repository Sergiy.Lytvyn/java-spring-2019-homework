package repository.dao;

import java.sql.SQLException;
import java.util.Optional;

public interface DAO<T> {

    void create(T t) throws SQLException;

    void update(T t) throws SQLException;

    Optional<T> getByEmail(String email) throws SQLException;

    void deleteByEmail(String email) throws SQLException;

}
