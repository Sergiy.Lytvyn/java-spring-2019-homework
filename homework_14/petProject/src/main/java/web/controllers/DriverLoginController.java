package web.controllers;

import service.services.DriverService;
import service.validation.ValidationException;
import web.dto.DriverSignInDTO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(
        name = "DriverController",
        urlPatterns = "/drivers"
)
public class DriverLoginController extends HttpServlet{

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        String name = req.getParameter("name");
        DriverSignInDTO driverSignInDTO = new DriverSignInDTO(name, password, email);
        DriverService driverService = DriverService.getInstance();
        try {
            driverService.create(driverSignInDTO);
            req.setAttribute("username", driverSignInDTO.getName());
            req.getRequestDispatcher("index.jsp").forward(req, resp);
        } catch (ValidationException | RuntimeException e) {
            req.setAttribute("mess", e.getMessage());
            req.getRequestDispatcher("error.jsp").forward(req, resp);
        } catch (SQLException e) {
            req.setAttribute("mess", "server error");
            req.getRequestDispatcher("error.jsp").forward(req, resp);
        }
    }

}
