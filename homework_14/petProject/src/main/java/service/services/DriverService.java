package service.services;

import repository.dao.DAO;
import repository.dao.DriverDAO;
import repository.entity.Driver;
import service.validation.ValidationException;
import service.validation.Validator;
import web.dto.DriverSignInDTO;

import java.sql.SQLException;
import java.util.Optional;

public class DriverService {

    private static DriverService ourInstance;

    public static DriverService getInstance() {
        if(ourInstance == null){
            synchronized (DriverService.class){
                if (ourInstance == null){
                    ourInstance = new DriverService();
                }
            }
        }
        return ourInstance;
    }

    private DriverService() { }

    public void create(DriverSignInDTO dto) throws SQLException, ValidationException {
        Validator validator = new Validator();
        validator.validateEmail(dto.getEmail());
        validator.validateStringLength(dto.getPassword(), 6, 16);
        validator.validate();
        DAO<Driver> driverDAO = DriverDAO.getInstance();
        Optional<Driver> driverFromDB = driverDAO.getByEmail(dto.getEmail());
        if(driverFromDB.isPresent()){
            throw new RuntimeException("This user already exist");
        } else {
           Driver driver = new Driver();
           driver.setName(dto.getName());
           driver.setEmail(dto.getEmail());
           driver.setPassword(dto.getPassword());
           driverDAO.create(driver);
        }
    }

}
