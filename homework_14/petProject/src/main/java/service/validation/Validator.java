package service.validation;

import java.util.ArrayList;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.List;
import java.util.stream.Collectors;

public class Validator {

    final static String EMAIL_REGEXP = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"" +
            "(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\"" +
            ")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}" +
            "(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:" +
            "(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

    private List<String> validationFail;

    public Validator(){
        validationFail = new ArrayList<>();
    }

    public void validateEmail(String email) {
        Pattern pattern = Pattern.compile(EMAIL_REGEXP);
        Matcher matcher = pattern.matcher(email);
        if (!matcher.matches()){
            validationFail.add("you input bad email");
        }
    }

    public void validateStringLength(String src, int min, int max) {
        int srcLength = src.length();
        if ((srcLength < min) || (srcLength > max)){
            validationFail.add("Wrong string length of field " + src + ". Length must be between "
                    + min + " and " + max + "but was " + srcLength);
        }
    }

    public void validateNotNull(Object o) {
        if(Objects.isNull(o)){
            validationFail.add(o + " is null");
        }
    }

    public void validetePositive(Long num){
        if (num < 0){
            validationFail.add(num + "is negative");
        }
    }

    public List<String> getValidationList(){
        return validationFail;
    }

    public void validate() throws ValidationException{
        if (!validationFail.isEmpty()){
            String res = validationFail.stream().collect(Collectors.joining(". "));
            throw new ValidationException(res);
        }
    }

}
