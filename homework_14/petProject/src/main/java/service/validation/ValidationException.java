package service.validation;

import java.util.List;
import java.util.stream.Collectors;

public class ValidationException extends Exception {

    public ValidationException() {
    }

    public ValidationException(String message) {
        super(message);
    }

}
