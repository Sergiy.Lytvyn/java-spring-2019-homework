<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri="/WEB-INF/tag/welcome.tld" prefix="custom" %>
<%@ page isELIgnored="false" %>
<html>
   <head>
      <title>Welcome to AutoBase 1.0</title>
   </head>
    <body>
    <c:choose>
    <c:when test="${username != null}">
    <custom:welcome name="${username}"/>
    </c:when>
       <c:otherwise>
           <custom:welcome name="anonymous"/>
           <form action="/pet-project/signIn">
           <input type="submit" value="Sign IN">
           </form>
       </c:otherwise>
    </c:choose>
    </body>
</html>



