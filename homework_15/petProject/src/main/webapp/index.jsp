<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri="/WEB-INF/tag/welcome.tld" prefix="custom" %>
<%@ page isELIgnored="false" %>
<html>
   <head>
      <title>Welcome to AutoBase 1.0</title>
       <meta charset="utf-8">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
       <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
   </head>
    <body>
    <c:set var="driver" value="${sessionScope.driver}"/>

    <c:choose>
    <c:when test="${driver != null}">
        <div class="jumbotron text-center">
            <h1>You have already login</h1>
        </div>
        <a href="/pet-project/driver/profile" class="btn btn-info" role="button">See my profile</a>
        <form action="/pet-project/logout" method="post">
            <button type="submit" class="btn btn-info">Logout</button>
        </form>
    </c:when>
       <c:otherwise>
           <div class="jumbotron text-center">
               <custom:welcome name="anonymous"/>
           </div>
           <div class="container">
               <a href="/pet-project/loginPage" class="btn btn-info" role="button">Login</a>
               <a href="/pet-project/registratePage" class="btn btn-info" role="button">Registrate</a>
           </div>
       </c:otherwise>
    </c:choose>
    </body>
</html>


