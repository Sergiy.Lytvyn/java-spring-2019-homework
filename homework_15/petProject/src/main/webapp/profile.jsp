<%@ page isELIgnored="false" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
    <title>Driver Profile</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

</head>
    <body>
    <c:set var="driver" value="${sessionScope.driver}"/>
    <h1>Welcome to your profile</h1>
    <h1>Name: <c:out value="${driver.name}"/></h1>
    <h1>Email: <c:out value="${driver.email}"/></h1>
    <form action="/pet-project/logout" method="post">
        <button type="submit" class="btn btn-default">Logout</button>
    </form>
    </body>
</html>