package repository.entity;

import java.util.Objects;

public class Driver {

    private Integer driverID;
    private String email;
    private String password;
    private String name;

    public Driver() {
    }

    public Driver(String email, String password, String name) {
        this.email = email;
        this.password = password;
        this.name = name;
    }

    public Integer getDriverID() {
        return driverID;
    }

    public void setDriverID(Integer driverID) {
        this.driverID = driverID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Driver)) return false;
        Driver driver = (Driver) o;
        return getDriverID().equals(driver.getDriverID()) &&
                getEmail().equals(driver.getEmail()) &&
                getPassword().equals(driver.getPassword()) &&
                getName().equals(driver.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDriverID(), getEmail(), getPassword(), getName());
    }
}
