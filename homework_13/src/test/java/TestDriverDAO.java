import entity.Driver;
import org.hamcrest.Matchers;
import org.junit.*;

import dao.DAO;
import dao.DriverDAO;
import java.sql.SQLException;
import java.util.Collection;

public class TestDriverDAO {

    private static DAO<Driver> driverDAO = DriverDAO.getInstance();

    private Driver getLastDriver(Collection<Driver> drivers){
        Driver driver = null;
        for (Driver d : drivers){
            driver = d;
        }
        return driver;
    }

    @AfterClass
    public static void deleteAllEntry() throws SQLException {
        Collection<Driver> drivers = driverDAO.getAll();
        for (Driver driver : drivers){
            driverDAO.deleteByID(driver.getID());
        }
    }

    @Before
    public void addSomeDrivers() throws SQLException {
        driverDAO.create(new Driver(1,"ooo"));
        driverDAO.create(new Driver(2, "kkk"));
    }

    @After
    public void deleteAddedDrivers() throws SQLException{
        Collection<Driver> drivers = driverDAO.getAll();
        int size = drivers.size();
        driverDAO.deleteByID(size);
        driverDAO.deleteByID((size - 1));
    }

    @Test
    public void testCrate() throws SQLException {
        //GIVEN
        Driver driver = new Driver(1,"Petr");
        Driver driverFromDB = null;
        Collection<Driver> driversCollection;
        //WHEN
        driverDAO.create(driver);
        driversCollection = driverDAO.getAll();
        for (Driver d : driversCollection){
            driverFromDB = d;
        }
        //THEN
        Assert.assertNotNull(driverFromDB);
        Assert.assertThat(driverFromDB.getName(), Matchers.is(driver.getName()));
    }

    @Test
    public void testGetAll() throws SQLException {
        //GIVEN
        Collection<Driver> drivers;
        Driver driver = new Driver(1, "Fedor");
        int amount;
        //WHEN
        driverDAO.create(driver);
        drivers = driverDAO.getAll();
        amount = drivers.size();
        driver.setID(amount);
        //THEN
        Assert.assertTrue(amount > 0);
    }

    @Test
    public void testGetByID() throws SQLException {
        //GIVEN
        Collection<Driver> drivers;
        Driver driverFromDB;
        Driver lastDriver;
        //WHEN
        drivers = driverDAO.getAll();
        lastDriver = getLastDriver(drivers);
        driverFromDB = driverDAO.getByID(lastDriver.getID());
        //THEN
        Assert.assertEquals(driverFromDB, lastDriver);
    }

    @Test
    public void testUpdateByID() throws SQLException {
        //GIVEN
        Driver lastDriver = getLastDriver(driverDAO.getAll());
        String nameBeforeUpdate = lastDriver.getName();
        Integer idBeforeUpdate = lastDriver.getID();
        Driver driverAfterUpdate;
        //WHEN
        lastDriver.setName("PPP");
        driverDAO.updateByID(lastDriver);
        driverAfterUpdate = driverDAO.getByID(lastDriver.getID());
        lastDriver.setName(nameBeforeUpdate);
        driverDAO.updateByID(lastDriver);
        //THEN
        Assert.assertNotEquals(nameBeforeUpdate, driverAfterUpdate.getName());
        Assert.assertEquals(idBeforeUpdate, driverAfterUpdate.getID());
    }

    @Test
    public void testDeleteByID() throws SQLException {
        //GIVEN
        Driver driver = new Driver(1, "Igor");
        Driver lastDriver;
        int sizeBefore;
        int sizeAfter;
        //WHEN
        driverDAO.create(driver);
        Collection<Driver> drivers = driverDAO.getAll();
        sizeBefore = drivers.size();
        lastDriver = getLastDriver(drivers);
        driverDAO.deleteByID(lastDriver.getID());
        sizeAfter = driverDAO.getAll().size();
        //THEN
        Assert.assertThat(1, Matchers.is(sizeBefore - sizeAfter));
    }

}
