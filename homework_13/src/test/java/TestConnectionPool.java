import org.junit.Assert;
import org.junit.Test;
import util.ConnectionPool;

import java.sql.Connection;
import java.sql.SQLException;

public class TestConnectionPool {

    @Test
    public void testGetConnection() throws SQLException {
        //GIVEN
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        //WHEN
        Connection connection = connectionPool.getConnection();
        //THEN
        Assert.assertNotNull(connection);
    }
}
