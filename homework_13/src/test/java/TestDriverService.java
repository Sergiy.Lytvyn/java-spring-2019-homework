import entity.Driver;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import services.DriverService;
import validators.ValidationException;

import java.sql.SQLException;

public class TestDriverService {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    private DriverService driverService = DriverService.getInstance();

    @Test
    public void testGetByIdWithIllegalId() throws SQLException {
       //GIVEN
        expectedException.expect(RuntimeException.class);
        //WHEN
        driverService.getByID(-1);
    }

    @Test
    public void testUpdateAllByIDWhenEntityNotExist() throws SQLException, ValidationException {
        //GIVEN
        expectedException.expect(RuntimeException.class);
        Driver firstDriver = new Driver(999999, "Serhii");
        Driver secondDriver = new Driver(1, "Vasil");
        //WHEN
        driverService.updateAllByID(firstDriver, secondDriver);
    }

    @Test
    public void testUpdateByID() throws SQLException, ValidationException {
        //GIVEN
        expectedException.expect(RuntimeException.class);
        Driver firstDriver = new Driver(999999, "Serhii");
        //WHEN
        driverService.updateByID(firstDriver);
    }

    @Test
    public void testDeleteByIdWithIllegalID() throws SQLException {
        //GIVEN
        expectedException.expect(RuntimeException.class);
        //WHEN
        driverService.deleteById(-1);
    }

    @Test
    public void testDeleteByIdWhenEntityNotExist() throws SQLException {
        //GIVEN
        expectedException.expect(RuntimeException.class);
        //WHEN
        driverService.deleteById(66666);
    }

}