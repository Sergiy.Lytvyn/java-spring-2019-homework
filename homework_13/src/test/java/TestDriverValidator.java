import entity.Driver;
import org.junit.*;
import org.junit.rules.ExpectedException;

import validators.DriverValidator;
import validators.ValidationException;
import validators.Validator;

public class TestDriverValidator {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    private Validator<Driver> validator = new DriverValidator();

    @Test
    public void testValidateWithIllegalID() throws ValidationException {
        //GIVEN
        expectedException.expect(ValidationException.class);
        Driver driver = new Driver(0, "Serhii");
        //WHEN
        validator.validate(driver);
    }

    @Test
    public void testValidateWithIllegalNameLength() throws ValidationException {
        //GIVEN
        expectedException.expect(ValidationException.class);
        Driver driver = new Driver(1, "S");
        //WHEN
        validator.validate(driver);
    }

    @Test
    public void testValidateWithNullID() throws ValidationException {
        //GIVEN
        expectedException.expect(ValidationException.class);
        Driver driver = new Driver(null, "Serhii");
        //WHEN
        validator.validate(driver);
    }

    @Test
    public void testValidateWithNullName() throws ValidationException {
        //GIVEN
        expectedException.expect(ValidationException.class);
        Driver driver = new Driver(1, null);
        validator.validate(driver);
    }

}
