package validators;

import entity.Driver;

import java.util.Objects;

public class DriverValidator implements Validator<Driver>{

    private static Validator<Driver> validator;

    public DriverValidator(){ }

    public static Validator<Driver> getValidator() {
        if (validator == null){
            synchronized (DriverValidator.class){
                if (validator == null){
                    validator = new DriverValidator();
                }
            }
        }
        return validator;
    }

    private boolean isValidID(Driver driver){
        return Objects.nonNull(driver.getID()) && (driver.getID() > 0);
    }

    private boolean isValidName(Driver driver){
        return Objects.nonNull(driver.getName()) && (driver.getName().length() > 2);
    }

    public void validate(Driver driver) throws ValidationException {
        if (Objects.isNull(driver)){
            throw new ValidationException("driver reference is null");
        } else if (!isValidName(driver)){
            throw new ValidationException("driver name isn't valid: " + driver.getName());
        } else if (!isValidID(driver)){
            throw new ValidationException("driver ID isn't valid: " + driver.getID());
        }
    }

}
