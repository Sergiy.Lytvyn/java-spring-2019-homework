package validators;

public class ValidationException extends Exception {

    ValidationException(){
        super();
    }

    ValidationException (String mess){
        super(mess);
    }

}
