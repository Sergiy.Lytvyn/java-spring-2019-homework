package controller;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface Controller<T> {

    @HEAD
    @Produces(MediaType.TEXT_HTML)
    Response head();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    Response getAll();

    @Path("/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    Response getByID(@PathParam("id") Integer id);

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    Response updateAllByID(T...objects);

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    Response updateByID(T object);

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    Response create(T object);

    @DELETE
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    Response deleteByID(@PathParam("id") Integer id);

}
