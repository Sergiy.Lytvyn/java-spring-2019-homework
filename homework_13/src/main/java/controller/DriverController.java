package controller;

import entity.Driver;
import services.DriverService;
import validators.ValidationException;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.sql.SQLException;
import java.util.Collection;

@Path("/drivers")
public class DriverController implements Controller<Driver>{

    @Override
    public Response head() {
        return Response.ok().build();
    }

    @Override
    public Response getAll() {
        DriverService driverService = DriverService.getInstance();
        Collection<Driver> drivers;
        try {
            drivers = driverService.getAll();
            if (drivers.isEmpty()){
                return Response.noContent().build();
            }
        } catch (SQLException ex){
            return Response.serverError().build();
        }
        return Response.ok().entity(drivers).build();
    }

    @Override
    public Response getByID(Integer id) {
        DriverService driverService = DriverService.getInstance();
        Driver driver;
        try {
            driver = driverService.getByID(id);
        } catch (SQLException e) {
            return Response.serverError().build();
        } catch (RuntimeException ex){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok().entity(driver).build();
    }

    @Override
    public Response updateAllByID(Driver...drivers) {
        DriverService driverService = DriverService.getInstance();
        try {
            driverService.updateAllByID(drivers);
        } catch (SQLException e) {
            return Response.serverError().build();
        } catch (ValidationException | RuntimeException e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok().build();
    }

    @Override
    public Response updateByID(Driver object) {
        DriverService driverService = DriverService.getInstance();
        try {
            driverService.updateByID(object);
        } catch (SQLException e) {
            return Response.serverError().build();
        } catch (ValidationException | RuntimeException e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok().build();
    }

    @Override
    public Response create(Driver object) {
        DriverService driverService = DriverService.getInstance();
        try {
            driverService.create(object);
        } catch (SQLException ex){
            return Response.serverError().build();
        } catch (ValidationException ex){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok().build();
    }

    @Override
    public Response deleteByID(Integer id) {
        DriverService driverService = DriverService.getInstance();
        try {
            driverService.deleteById(id);
        } catch (SQLException ex) {
            return Response.serverError().build();
        } catch (RuntimeException ex) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok().build();
    }

}
