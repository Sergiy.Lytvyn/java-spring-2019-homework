package entity;

import java.util.Objects;

public class Driver {

    private Integer ID;
    private String name;

    public Driver() {
    }

    public Driver(Integer ID, String name) {
        this.ID = ID;
        this.name = name;
    }

    public Integer getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Driver)) return false;
        Driver driver = (Driver) o;
        return getID().equals(driver.getID()) &&
                getName().equals(driver.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getID(), getName());
    }

    @Override
    public String toString() {
        return "Driver{" +
                "ID=" + ID +
                ", name='" + name + '\'' +
                '}';
    }
}
