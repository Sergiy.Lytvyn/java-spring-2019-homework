package dao;

import entity.Driver;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import util.ConnectionPool;

public class DriverDAO implements DAO<Driver>{

    private static DAO<Driver> driverDAO;

    private DriverDAO(){ }

    private Driver extractDriverFromResultSet(ResultSet rs) throws SQLException {
        Integer id = rs.getInt("driver_ID");
        String name = rs.getString("driver_name");
        return new Driver(id, name);
    }

    public static DAO<Driver> getInstance(){
        if (driverDAO == null){
            synchronized (DriverDAO.class){
                if (driverDAO == null){
                    driverDAO = new DriverDAO();
                }
            }
        }
        return driverDAO;
    }

    @Override
    public void create(Driver driver) throws SQLException {
        final String query = "INSERT INTO drivers (driver_name) VALUES (?)";
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)){
            preparedStatement.setString(1, driver.getName());
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public Collection<Driver> getAll() throws SQLException {
        final String query = "SELECT * FROM drivers";
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        ArrayList<Driver> drivers = new ArrayList<>();
        try (Connection connection = connectionPool.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(query)){
            while (resultSet.next()){
                drivers.add(extractDriverFromResultSet(resultSet));
            }
        }
        return drivers;
    }

    @Override
    public Driver getByID(Integer id) throws SQLException {
        final String query = "SELECT * FROM drivers WHERE driver_ID = " + id;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(query)) {
            if (resultSet.next()) {
                return extractDriverFromResultSet(resultSet);
            } else {
                throw new RuntimeException("element with id:" + id + " doesn't exist");
            }
        }
    }

    @Override
    public void updateByID(Driver driver) throws SQLException {
        final String query = "UPDATE drivers SET driver_name = ? WHERE driver_ID = ?";
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try(Connection connection = connectionPool.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, driver.getName());
            preparedStatement.setInt(2, driver.getID());
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public void updateAllByID(Driver...drivers) throws SQLException {
        final String query = "UPDATE drivers SET driver_name = ? WHERE driver_ID = ?";
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            for(Driver driver : drivers) {
                preparedStatement.setString(1, driver.getName());
                preparedStatement.setInt(2, driver.getID());
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
        }
    }

    @Override
    public void deleteByID(Integer id) throws SQLException{
        final String query = "DELETE FROM drivers WHERE driver_ID = " + id;
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        try (Connection connection = connectionPool.getConnection()){
            Statement statement = connection.createStatement();
            statement.execute(query);
        }
    }
}
