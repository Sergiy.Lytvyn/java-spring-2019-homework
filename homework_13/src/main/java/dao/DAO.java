package dao;

import java.sql.SQLException;
import java.util.Collection;

public interface DAO<T> {

    void create(T t) throws SQLException;

    Collection<T> getAll() throws SQLException;

    T getByID(Integer id) throws SQLException;

    void updateByID(T obj) throws SQLException;

    void updateAllByID(T...arr) throws SQLException;

    void deleteByID(Integer id) throws SQLException;

}
