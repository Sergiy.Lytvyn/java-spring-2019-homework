package services;

import dao.DAO;
import dao.DriverDAO;
import entity.Driver;
import validators.DriverValidator;
import validators.ValidationException;
import validators.Validator;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

public class DriverService {

    private static DriverService driverService;

    private DriverService(){ }

    private void checkID(Integer id){
        if (id < 1){
            throw new RuntimeException("try to use illegal id");
        }
    }

    public static DriverService getInstance(){
        if (driverService == null){
            synchronized (DriverService.class){
                if (driverService == null){
                    driverService = new DriverService();
                }
            }
        }
        return driverService;
    }

    public void create(Driver driver) throws SQLException, ValidationException {
        DriverValidator.getValidator().validate(driver);
        DriverDAO.getInstance().create(driver);
    }

    public Collection<Driver> getAll()throws SQLException{
        return DriverDAO.getInstance().getAll();
    }

    public Driver getByID(Integer id)throws SQLException {
       checkID(id);
        return DriverDAO.getInstance().getByID(id);
    }

    public void updateAllByID(Driver...drivers)throws SQLException, ValidationException{
        Validator<Driver> driverValidator = DriverValidator.getValidator();
        DAO<Driver> driverDAO = DriverDAO.getInstance();
        for (Driver driver : drivers) {
            driverValidator.validate(driver);
        }
        Collection<Integer> driversIdFromDAO = driverDAO.getAll().stream().map(Driver::getID)
                .collect(Collectors.toCollection(ArrayList::new));
        Collection<Integer> sourceDriversId = Arrays.stream(drivers).map(Driver::getID).collect(Collectors.toCollection(ArrayList::new));
        if (driversIdFromDAO.containsAll(sourceDriversId)){
            driverDAO.updateAllByID(drivers);
        } else {
            throw new RuntimeException("try to update entity that doesn't exist");
        }
    }

    public void updateByID(Driver driver) throws SQLException, ValidationException{
        DriverValidator.getValidator().validate(driver);
        Driver driverFromDB = getByID(driver.getID());
        if (driver.getID().equals(driverFromDB.getID())){
            DriverDAO.getInstance().updateByID(driver);
        } else {
            throw new RuntimeException("try to update entity that doesn't exist");
        }
    }

    public void deleteById(Integer id) throws SQLException {
        checkID(id);
        DAO<Driver> driverDAO = DriverDAO.getInstance();
        Driver driverFromDAO = driverDAO.getByID(id);
        if (Objects.nonNull(driverFromDAO)){
            driverDAO.deleteByID(id);
        } else {
            throw new RuntimeException("try to delete driver that doesn't exist in DB");
        }
    }

}
