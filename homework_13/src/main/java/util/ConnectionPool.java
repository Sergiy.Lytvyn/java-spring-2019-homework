package util;

import org.mariadb.jdbc.MariaDbPoolDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionPool {

    private static ConnectionPool instance;
    private final MariaDbPoolDataSource connectionPool;

    private ConnectionPool() throws SQLException {
        connectionPool = new MariaDbPoolDataSource();
        connectionPool.setUrl("jdbc:mariadb://localhost:3303/homework_13?");
        connectionPool.setUser("root");
        connectionPool.setPassword("jordan11111");
        connectionPool.setMaxPoolSize(10);
    }

    public static ConnectionPool getInstance() throws SQLException {
        if (instance == null){
            synchronized (ConnectionPool.class){
                if (instance == null){
                    instance = new ConnectionPool();
                }
            }
        }
        return instance;
    }

    public Connection getConnection() throws SQLException {
        return connectionPool.getConnection();
    }

}
