package StreamHomework;

import java.util.*;
import java.util.function.Function;
import java.util.stream.*;

public class StreamHomework {

    public static String getSeparatedStringOfIntegers(List<Integer> list) {
        return list.stream().filter(Objects::nonNull).map(x -> (x % 2) == 0 ? "e" + x : "o" + x)
                .collect(Collectors.joining(","));
    }

    public static Map<String, City> getLargestCityPerState(Collection<City> cities) {
       Map<String, Optional<City>> largestCity = cities.stream()
               .collect(Collectors.groupingBy(City::getState, Collectors.maxBy(Comparator.comparingLong(City::getPopulation))));
       return largestCity.values().stream()
               .map(Optional::get).collect(Collectors.toMap(City::getState, Function.identity()));
    }

    public static <T> Stream<T> zip(Stream<T> first, Stream<T> second) {
         Iterator<T> iteratorFirst = first.iterator();
        Iterator<T> iteratorSecond = second.iterator();
        Stream<T> res = Stream.empty();

        while (iteratorFirst.hasNext() && iteratorSecond.hasNext()){
            res = Stream.concat(res, Stream.of(iteratorFirst.next(), iteratorSecond.next()));
        }

        return res;
    }

}


