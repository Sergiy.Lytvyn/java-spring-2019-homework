package StreamHomework;

public class Book {

    private final String title;
    private final String author;
    private final Integer isbn;

    public Book(String title, String author, Integer isbn){
        if(isbn < 0){
            throw new IllegalArgumentException("incorrect isbn: " + isbn);
        }
        this.title = title;
        this.author = author;
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public int getIsbn() {
        return isbn;
    }

    public String getAuthor() {
        return author;
    }

}
