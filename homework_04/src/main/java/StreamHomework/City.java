package StreamHomework;

public class City {

    private final String state;
    private final long population;

    public City(String state, long population){
        if (population < 0){
            throw new IllegalArgumentException("incorrect population: " + population);
        }
        this.state = state;
        this.population = population;
    }

    public String getState() {
        return state;
    }

    public long getPopulation() {
        return population;
    }

}
