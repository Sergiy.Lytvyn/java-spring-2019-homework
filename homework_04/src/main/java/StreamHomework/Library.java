package StreamHomework;

import java.util.*;

public class Library {

    private List<Book> books;
    private Map<String, Integer> authorsWorks;

    public Library(){
        books = new ArrayList<>();
        authorsWorks = new HashMap<>();
    }

    public List<Book> getBooks() {
        return books;
    }

    public Map<String, Integer> getAuthorsWorks() {
        return authorsWorks;
    }

    public void add(Book b){
        String author = b.getAuthor();
        books.add(b);
        authorsWorks.compute(author, (key, oldValue) -> Objects.isNull(oldValue) ? 1 : ++oldValue);
    }

}

