package com.epam.rd.spring2019.homework_23.config;

import com.epam.rd.spring2019.homework_23.repository.config.AbstractSpringDataConfiguration;
import com.epam.rd.spring2019.homework_23.repository.config.RepositoryConfiguration;
import com.epam.rd.spring2019.homework_23.service.config.ServiceConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@Import({ServiceConfiguration.class, RepositoryConfiguration.class})
public class TestContextConfiguration extends AbstractSpringDataConfiguration {

    @Bean
    @Override
    public DataSource dataSource(){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.h2.Driver");
        dataSource.setUrl("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
        dataSource.setUsername("SA");
        dataSource.setPassword("");
        return dataSource;
    }

    @Override
    protected Properties jpaProperties() {
        Properties hibernateProperties = new Properties();
        hibernateProperties.setProperty("hibernate.hbm2ddl.auto", "create-drop");
        hibernateProperties.setProperty("hibernate.show_sql", "true");
        hibernateProperties.setProperty("hibernate.format_sql", "true");
        return hibernateProperties;
    }

}
