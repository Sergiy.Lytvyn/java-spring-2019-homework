package com.epam.rd.spring2019.homework_23.service.impl;

import com.epam.rd.spring2019.homework_23.config.TestContextConfiguration;
import com.epam.rd.spring2019.homework_23.repository.exception.DriverNotFoundException;
import com.epam.rd.spring2019.homework_23.service.api.DriverService;
import com.epam.rd.spring2019.homework_23.service.dto.DriverCreateDto;
import com.epam.rd.spring2019.homework_23.service.dto.DriverDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringJUnitConfig(TestContextConfiguration.class)
@Sql(scripts = {"/insert_drivers.sql","/insert_cars.sql"})
@Transactional
class DriverServiceImplTest {

    @Autowired
    private DriverService sut;

    @Test
    void getById() {
        //WHEN
        Optional<DriverDto> first = sut.getById("8ac64a65-3f38-4584-9907-bc1cd3aa6ecc");
        Optional<DriverDto> second = sut.getById("bf53fac8-6140-4376-a2b6-930f284bcb92");
        Optional<DriverDto> nonExist = sut.getById("c1ace717-fb0e-4964-800a-3639483a887e");
        //THAN
        assertTrue(first.isPresent());
        assertTrue(second.isPresent());
        assertFalse(nonExist.isPresent());
        assertEquals("Ted", second.get().getName());
        assertEquals("Michel", first.get().getName());
    }

    @Test
    void getAll() {
        //WHEN
        Collection<DriverDto> drivers = sut.getAll();
        //THAN
        assertEquals(3, drivers.size());
    }

    @Test
    void save() {
        //GIVEN
        DriverCreateDto newDriver = new DriverCreateDto("Driver");
        //WHEN
        sut.save(newDriver);
        //THAN
        assertEquals(4, sut.getAll().size());
        assertTrue(sut.getAll().stream()
                .anyMatch(driver -> driver.getName().equals(newDriver.getName())));
    }

    @Test
    void deleteById() {
        //GIVEN
        String id = "8ac64a65-3f38-4584-9907-bc1cd3aa6ecc";
        Optional<DriverDto> driver = sut.getById(id);
        //WHEN
        sut.deleteById(id);
        //THAN
        assertEquals(2, sut.getAll().size());
        assertTrue(driver.isPresent());
        assertFalse(sut.getById(id).isPresent());
        assertThrows(EmptyResultDataAccessException.class, ()->sut.deleteById("c1ace717-fb0e-4964-800a-3639483a887e"));
    }

    @Test
    void update() {
        //GIVEN
        DriverDto driver = sut.getAll().stream().findAny().orElseThrow(RuntimeException::new);
        DriverDto toUpdate = new DriverDto(driver.getId(), "New name");
        //WHEN
        sut.update(toUpdate);
        //WHEN
        assertEquals(3, sut.getAll().size());
        assertTrue(sut.getAll().contains(toUpdate));
    }
}