package com.epam.rd.spring2019.homework_23.service.api;

import com.epam.rd.spring2019.homework_23.service.dto.CarCreateDto;
import com.epam.rd.spring2019.homework_23.service.dto.CarDto;

import java.util.Collection;
import java.util.Optional;

public interface CarService {
    Optional<CarDto> getById(String id);
    Collection<CarDto> getAll();
    Collection<CarDto> getByBrand(String brand);
    Collection<CarDto> getAllByBrandAndModel(String brand, String model);
    CarDto update(CarDto updateDto);
    CarDto create(CarCreateDto dto);
    String delete(String id);
}
