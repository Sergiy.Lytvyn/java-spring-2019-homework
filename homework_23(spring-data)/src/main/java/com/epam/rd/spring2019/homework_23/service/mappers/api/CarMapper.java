package com.epam.rd.spring2019.homework_23.service.mappers.api;

import com.epam.rd.spring2019.homework_23.repository.domain.Car;
import com.epam.rd.spring2019.homework_23.service.dto.CarCreateDto;
import com.epam.rd.spring2019.homework_23.service.dto.CarDto;

import java.util.Collection;

public interface CarMapper{
    Car toCar(CarDto dto);
    Car toCar(CarCreateDto dto);
    CarDto toDto(Car car);
    Collection<CarDto> toDto(Collection<Car> carCollection);
}
