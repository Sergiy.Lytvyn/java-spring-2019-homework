package com.epam.rd.spring2019.homework_23.repository.api;

import com.epam.rd.spring2019.homework_23.repository.domain.Car;
import com.epam.rd.spring2019.homework_23.repository.domain.Driver;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CarDao extends CrudRepository<Car, UUID>, JpaSpecificationExecutor<Car> {
    @Query("select car from Car car where car.brand=:brand")
    Collection<Car> findByBrand(@Param("brand") String brand);
}
