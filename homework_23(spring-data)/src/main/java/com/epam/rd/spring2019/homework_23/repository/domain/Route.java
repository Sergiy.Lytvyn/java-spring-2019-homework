package com.epam.rd.spring2019.homework_23.repository.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Route {

    @Id
    @Type(type = "uuid-char")
    private UUID id = UUID.randomUUID();
    private String startPoint;
    private String finishPoint;
    @ManyToOne
    @JoinColumn(name = "fk_dispatcher_id", nullable = false)
    private Dispatcher dispatcher;
}
