package com.epam.rd.spring2019.homework_23.service.impl;

import com.epam.rd.spring2019.homework_23.repository.api.RouteDao;
import com.epam.rd.spring2019.homework_23.repository.domain.Route;
import com.epam.rd.spring2019.homework_23.repository.exception.RouteNotFoundException;
import com.epam.rd.spring2019.homework_23.service.api.RouteService;
import com.epam.rd.spring2019.homework_23.service.dto.RouteCreateDto;
import com.epam.rd.spring2019.homework_23.service.dto.RouteDto;
import com.epam.rd.spring2019.homework_23.service.mappers.api.RouteMapper;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class RouteServiceImpl implements RouteService {

    private final RouteDao routeDao;
    private final RouteMapper routeMapper;

    public RouteServiceImpl(RouteDao routeDao, RouteMapper routeMapper) {
        this.routeDao = routeDao;
        this.routeMapper = routeMapper;
    }

    private Route performUpdate(Route persist, Route updated){
        persist.setDispatcher(updated.getDispatcher());
        persist.setStartPoint(updated.getStartPoint());
        persist.setFinishPoint(updated.getFinishPoint());
        return persist;
    }

    @Override
    public RouteDto create(RouteCreateDto dto) {
        Route created = routeDao.save(routeMapper.toRoute(dto));
        return routeMapper.toDto(created);
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<RouteDto> getById(String id) {
        return routeDao.findById(UUID.fromString(id))
                .map(routeMapper::toDto);
    }

    @Transactional(readOnly = true)
    @Override
    public Collection<RouteDto> getAll() {
        List<Route> routes = Lists.newArrayList(routeDao.findAll());
        return routeMapper.toDto(routes);
    }

    @Transactional
    @Override
    public RouteDto update(RouteDto dto) {
        Route persist = routeDao.findById(UUID.fromString(dto.getId()))
                .orElseThrow(RouteNotFoundException::new);
        Route updated = routeMapper.toRoute(dto);
        return routeMapper.toDto(performUpdate(persist, updated));
    }

    @Override
    public String delete(String id) {
        routeDao.deleteById(UUID.fromString(id));
        return id;
    }
}
