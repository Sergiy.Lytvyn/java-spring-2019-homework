package com.epam.rd.spring2019.homework_23.service.impl;

import com.epam.rd.spring2019.homework_23.repository.api.DriverDao;
import com.epam.rd.spring2019.homework_23.repository.domain.Driver;
import com.epam.rd.spring2019.homework_23.repository.exception.DriverNotFoundException;
import com.epam.rd.spring2019.homework_23.service.api.DriverService;
import com.epam.rd.spring2019.homework_23.service.dto.DriverCreateDto;
import com.epam.rd.spring2019.homework_23.service.dto.DriverDto;
import com.epam.rd.spring2019.homework_23.service.mappers.api.DriverMapper;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class DriverServiceImpl implements DriverService {

    private final DriverDao driverDao;
    private final DriverMapper driverMapper;

    public DriverServiceImpl(DriverDao driverDao, DriverMapper driverMapper) {
        this.driverDao = driverDao;
        this.driverMapper = driverMapper;
    }

    private Driver performUpdate(Driver persist, DriverDto dto){
        persist.setName(dto.getName());
        return persist;
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<DriverDto> getById(String id) {
        Optional<Driver> driver = driverDao.findById(UUID.fromString(id));
        return driver.map(driverMapper::toDto);
    }

    @Transactional(readOnly = true)
    @Override
    public Collection<DriverDto> getAll() {
        List<Driver> drivers = Lists.newArrayList(driverDao.findAll());
        return drivers.stream()
                .map(driverMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public DriverDto save(DriverCreateDto dto) {
        Driver driver =  driverDao.save(driverMapper.toDriver(dto));
        return driverMapper.toDto(driver);
    }

    @Override
    public String deleteById(String id) {
        driverDao.deleteById(UUID.fromString(id));
        return id;
    }

    @Transactional
    @Override
    public DriverDto update(DriverDto dto) {
        Driver persist = driverDao.findById(UUID.fromString(dto.getId()))
                .orElseThrow(DriverNotFoundException::new);
        return driverMapper.toDto(performUpdate(persist, dto));
    }

}
