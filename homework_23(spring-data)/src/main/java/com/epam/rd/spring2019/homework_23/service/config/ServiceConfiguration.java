package com.epam.rd.spring2019.homework_23.service.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {
        "com.epam.rd.spring2019.homework_23.service.impl",
        "com.epam.rd.spring2019.homework_23.service.mappers.impl"
})
public class ServiceConfiguration {
}
