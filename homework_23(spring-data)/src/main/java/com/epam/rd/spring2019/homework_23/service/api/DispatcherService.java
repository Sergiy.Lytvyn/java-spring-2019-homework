package com.epam.rd.spring2019.homework_23.service.api;

import com.epam.rd.spring2019.homework_23.service.dto.DispatcherCreateDto;
import com.epam.rd.spring2019.homework_23.service.dto.DispatcherDto;

import java.util.Collection;
import java.util.Optional;

public interface DispatcherService {
    DispatcherDto create(DispatcherCreateDto dto);
    Optional<DispatcherDto> getById(String id);
    Collection<DispatcherDto> getAll();
    DispatcherDto update(DispatcherDto dto);
    String delete(String id);
}
