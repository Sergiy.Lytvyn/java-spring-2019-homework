package com.epam.rd.spring2019.homework_23.repository.domain;

import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Driver {

    @Id
    @Type(type = "uuid-char")
    private UUID id = UUID.randomUUID();
    private String name;

}
