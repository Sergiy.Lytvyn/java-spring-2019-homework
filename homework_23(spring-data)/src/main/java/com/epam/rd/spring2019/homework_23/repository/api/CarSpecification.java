package com.epam.rd.spring2019.homework_23.repository.api;

import com.epam.rd.spring2019.homework_23.repository.domain.Car;
import com.epam.rd.spring2019.homework_23.repository.domain.Route;
import lombok.experimental.UtilityClass;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Objects;

@UtilityClass
public class CarSpecification {
    public static Specification<Car> findByBrandAndModel(String brand, String model){
        return (root, query, criteriaBuilder) -> {
           Predicate brandPredicate = criteriaBuilder.like(root.get("brand"), brand + '%');
           if (Objects.isNull(model)){
               return brandPredicate;
           } else {
               Predicate modelPredicate = criteriaBuilder.like(root.get("model"), model + '%');
               return criteriaBuilder.and(brandPredicate, modelPredicate);
           }
       };
    }
}
