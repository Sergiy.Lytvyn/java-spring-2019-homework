package com.epam.rd.spring2019.homework_23.repository.config;

import com.epam.rd.spring2019.homework_23.repository.api.CarDao;
import com.epam.rd.spring2019.homework_23.repository.api.DispatcherDao;
import com.epam.rd.spring2019.homework_23.repository.api.DriverDao;
import com.epam.rd.spring2019.homework_23.repository.api.RouteDao;
import com.epam.rd.spring2019.homework_23.repository.domain.Dispatcher;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(basePackageClasses = {CarDao.class, DriverDao.class, RouteDao.class, DispatcherDao.class})
public class RepositoryConfiguration {

}
