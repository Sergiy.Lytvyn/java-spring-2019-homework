package com.epam.rd.spring2019.homework_23.service.api;

import com.epam.rd.spring2019.homework_23.service.dto.RouteCreateDto;
import com.epam.rd.spring2019.homework_23.service.dto.RouteDto;

import java.util.Collection;
import java.util.Optional;

public interface RouteService {
    RouteDto create(RouteCreateDto dto);
    Optional<RouteDto> getById(String id);
    Collection<RouteDto> getAll();
    RouteDto update(RouteDto dto);
    String delete(String id);
}
