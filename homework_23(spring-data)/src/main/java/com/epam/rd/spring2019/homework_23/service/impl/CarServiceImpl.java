package com.epam.rd.spring2019.homework_23.service.impl;

import com.epam.rd.spring2019.homework_23.repository.api.CarDao;
import com.epam.rd.spring2019.homework_23.repository.api.CarSpecification;
import com.epam.rd.spring2019.homework_23.repository.api.DriverDao;
import com.epam.rd.spring2019.homework_23.repository.domain.Car;
import com.epam.rd.spring2019.homework_23.repository.domain.Driver;
import com.epam.rd.spring2019.homework_23.repository.exception.CarNotFoundException;
import com.epam.rd.spring2019.homework_23.repository.exception.DriverNotFoundException;
import com.epam.rd.spring2019.homework_23.service.api.CarService;
import com.epam.rd.spring2019.homework_23.service.dto.CarCreateDto;
import com.epam.rd.spring2019.homework_23.service.dto.CarDto;
import com.epam.rd.spring2019.homework_23.service.mappers.api.CarMapper;
import com.google.common.collect.Lists;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class CarServiceImpl implements CarService {

    private final CarDao carDao;
    private final CarMapper carMapper;
    private final DriverDao driverDao;

    public CarServiceImpl(CarDao carDao, CarMapper carMapper, DriverDao driverDao) {
        this.carDao = carDao;
        this.carMapper = carMapper;
        this.driverDao = driverDao;
    }

    private Car performUpdate(Car persist, Car updated){
        persist.setModel(updated.getModel());
        persist.setBrand(updated.getBrand());
        persist.setDriver(updated.getDriver());
        return persist;
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CarDto> getById(String id) {
        Optional<Car> car = carDao.findById(UUID.fromString(id));
        return car.map(carMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<CarDto> getAll() {
        List<Car> cars = Lists.newArrayList(carDao.findAll());
        return carMapper.toDto(cars);
    }

    @Override
    public Collection<CarDto> getByBrand(String brand) {
        return carMapper.toDto(carDao.findByBrand(brand));
    }

    @Override
    public Collection<CarDto> getAllByBrandAndModel(String brand, String model) {
        Specification<Car> specification = CarSpecification.findByBrandAndModel(brand, model);
        return carMapper.toDto(carDao.findAll(specification));
    }

    @Override
    @Transactional
    public CarDto update(CarDto dto) {
        Car persist = carDao.findById(UUID.fromString(dto.getId()))
                .orElseThrow(CarNotFoundException::new);
        Car updated = performUpdate(persist, carMapper.toCar(dto));
        return carMapper.toDto(updated);
    }

    @Override
    public CarDto create(CarCreateDto dto) {
        Driver driver = driverDao.findById(UUID.fromString(dto.getDriverId()))
                .orElseThrow(DriverNotFoundException::new);
        Car created = carMapper.toCar(dto);
        created.setDriver(driver);
        return carMapper.toDto(carDao.save(created));
    }

    @Override
    public String delete(String id) {
        carDao.deleteById(UUID.fromString(id));
        return id;
    }
}
