package com.epam.rd.spring2019.homework_23.service.dto;

import lombok.Value;

@Value
public class CarCreateDto {
    private final String model;
    private final String brand;
    private final String driverId;
}
