package com.epam.rd.spring2019.homework_23.service.dto;

import lombok.Value;

@Value
public class DriverDto {
    private final String id;
    private final String name;
}
