package com.epam.rd.spring2019.homework_23.repository.api;

import com.epam.rd.spring2019.homework_23.repository.domain.Driver;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

public interface DriverDao extends CrudRepository<Driver, UUID> {

}
