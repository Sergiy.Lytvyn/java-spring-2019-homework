package com.epam.rd.spring2019.homework_23.service.mappers.impl;

import com.epam.rd.spring2019.homework_23.repository.domain.Dispatcher;
import com.epam.rd.spring2019.homework_23.service.dto.DispatcherCreateDto;
import com.epam.rd.spring2019.homework_23.service.dto.DispatcherDto;
import com.epam.rd.spring2019.homework_23.service.mappers.api.DispatcherMapper;
import com.epam.rd.spring2019.homework_23.service.mappers.api.RouteMapper;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class DispatcherMapperImpl implements DispatcherMapper {

    private final RouteMapper routeMapper;

    public DispatcherMapperImpl(RouteMapper routeMapper) {
        this.routeMapper = routeMapper;
    }

    @Override
    public Dispatcher toDispatcher(DispatcherDto dto) {
        Dispatcher dispatcher = new Dispatcher();
        dispatcher.setName(dto.getName());
        dispatcher.setId(UUID.fromString(dto.getId()));
        dispatcher.setRoutes(routeMapper.toRoute(dto.getRoutes()));
        return dispatcher;
    }

    @Override
    public Dispatcher toDispatcher(DispatcherCreateDto dto) {
        Dispatcher dispatcher = new Dispatcher();
        dispatcher.setName(dto.getName());
        return dispatcher;
    }

    @Override
    public DispatcherDto toDto(Dispatcher dispatcher) {
        return DispatcherDto.builder()
                .id(dispatcher.getId().toString())
                .name(dispatcher.getName())
                .routes(routeMapper.toDto(dispatcher.getRoutes()))
                .build();
    }
}
