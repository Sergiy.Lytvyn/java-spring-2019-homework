package com.epam.rd.spring2019.homework_23.service.dto;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class RouteDto {
    private final String id;
    private final String startPoint;
    private final String finishPoint;
    private final String dispatcherId;
}
