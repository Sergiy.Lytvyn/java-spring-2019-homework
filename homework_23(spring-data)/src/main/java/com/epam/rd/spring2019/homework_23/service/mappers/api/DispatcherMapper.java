package com.epam.rd.spring2019.homework_23.service.mappers.api;

import com.epam.rd.spring2019.homework_23.repository.domain.Dispatcher;
import com.epam.rd.spring2019.homework_23.service.dto.DispatcherCreateDto;
import com.epam.rd.spring2019.homework_23.service.dto.DispatcherDto;

public interface DispatcherMapper {
    Dispatcher toDispatcher(DispatcherDto dto);
    Dispatcher toDispatcher(DispatcherCreateDto dto);
    DispatcherDto toDto(Dispatcher dispatcher);
}
