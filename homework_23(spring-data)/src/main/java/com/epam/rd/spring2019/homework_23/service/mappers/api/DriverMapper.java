package com.epam.rd.spring2019.homework_23.service.mappers.api;

import com.epam.rd.spring2019.homework_23.repository.domain.Driver;
import com.epam.rd.spring2019.homework_23.service.dto.DriverCreateDto;
import com.epam.rd.spring2019.homework_23.service.dto.DriverDto;

public interface DriverMapper {
    Driver toDriver(DriverDto dto);
    Driver toDriver(DriverCreateDto dto);
    DriverDto toDto(Driver driver);
}
