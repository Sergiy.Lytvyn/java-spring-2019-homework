package com.epam.rd.spring2019.homework_23.service.mappers.impl;

import com.epam.rd.spring2019.homework_23.repository.domain.Driver;
import com.epam.rd.spring2019.homework_23.service.dto.DriverCreateDto;
import com.epam.rd.spring2019.homework_23.service.dto.DriverDto;
import com.epam.rd.spring2019.homework_23.service.mappers.api.DriverMapper;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class DriverMapperImpl implements DriverMapper {

    @Override
    public Driver toDriver(DriverDto dto) {
        return new Driver(UUID.fromString(dto.getId()), dto.getName());
    }

    @Override
    public Driver toDriver(DriverCreateDto dto) {
        return new Driver(UUID.randomUUID(), dto.getName());
    }

    @Override
    public DriverDto toDto(Driver driver) {
        return new DriverDto(
                driver.getId().toString(),
                driver.getName()
        );
    }

}
