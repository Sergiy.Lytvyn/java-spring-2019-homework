package com.epam.rd.spring2019.homework_23.service.api;

import com.epam.rd.spring2019.homework_23.service.dto.DriverCreateDto;
import com.epam.rd.spring2019.homework_23.service.dto.DriverDto;

import java.util.Collection;
import java.util.Optional;

public interface DriverService {
    Optional<DriverDto> getById(String id);
    Collection<DriverDto> getAll();
    DriverDto save(DriverCreateDto dto);
    String deleteById(String id);
    DriverDto update(DriverDto dto);
}
