package com.epam.rd.spring2019.homework_23.service.mappers.impl;

import com.epam.rd.spring2019.homework_23.repository.domain.Car;
import com.epam.rd.spring2019.homework_23.service.dto.CarCreateDto;
import com.epam.rd.spring2019.homework_23.service.dto.CarDto;
import com.epam.rd.spring2019.homework_23.service.mappers.api.CarMapper;
import com.epam.rd.spring2019.homework_23.service.mappers.api.DriverMapper;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class CarMapperImpl implements CarMapper {

    private final DriverMapper driverMapper;

    public CarMapperImpl(DriverMapper driverMapper) {
        this.driverMapper = driverMapper;
    }

    @Override
    public Car toCar(CarDto dto) {
        return new Car(UUID.fromString(dto.getId()),
                dto.getBrand(),
                dto.getModel(),
                driverMapper.toDriver(dto.getDriver())
        );
    }

    @Override
    public Car toCar(CarCreateDto dto) {
        Car car = new Car();
        car.setBrand(dto.getBrand());
        car.setModel(dto.getModel());
        return car;
    }

    @Override
    public CarDto toDto(Car car) {
        return CarDto.builder()
                .id(car.getId().toString())
                .brand(car.getBrand())
                .model(car.getModel())
                .driver(driverMapper.toDto(car.getDriver()))
                .build();
    }

    @Override
    public Collection<CarDto> toDto(Collection<Car> carCollection) {
        return carCollection.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }
}
