package com.epam.rd.spring2019.homework_22.service.impl;

import com.epam.rd.spring2019.homework_22.repository.api.CarDao;
import com.epam.rd.spring2019.homework_22.repository.api.DriverDao;
import com.epam.rd.spring2019.homework_22.repository.domain.Car;
import com.epam.rd.spring2019.homework_22.repository.domain.Driver;
import com.epam.rd.spring2019.homework_22.repository.exception.CarNotFoundException;
import com.epam.rd.spring2019.homework_22.repository.exception.DriverNotFoundException;
import com.epam.rd.spring2019.homework_22.service.api.CarService;
import com.epam.rd.spring2019.homework_22.service.dto.CarCreateDto;
import com.epam.rd.spring2019.homework_22.service.dto.CarDto;
import com.epam.rd.spring2019.homework_22.service.mappers.api.CarMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

@Service
public class CarServiceImpl implements CarService {

    private final CarDao carDao;
    private final CarMapper carMapper;
    private final DriverDao driverDao;

    public CarServiceImpl(CarDao carDao, CarMapper carMapper, DriverDao driverDao) {
        this.carDao = carDao;
        this.carMapper = carMapper;
        this.driverDao = driverDao;
    }

    private Car performUpdate(Car persist, Car updated){
        persist.setModel(updated.getModel());
        persist.setBrand(updated.getBrand());
        persist.setDriver(updated.getDriver());
        return persist;
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CarDto> getById(String id) {
        Optional<Car> car = carDao.getById(UUID.fromString(id));
        return car.map(carMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<CarDto> getAll() {
        return carMapper.toDto(carDao.getAll());
    }

    @Override
    @Transactional
    public CarDto update(CarDto dto) {
        Car persist = carDao.getById(UUID.fromString(dto.getId()))
                .orElseThrow(CarNotFoundException::new);
        Car updated = performUpdate(persist, carMapper.toCar(dto));
        return carMapper.toDto(updated);
    }

    @Override
    public CarDto create(CarCreateDto dto) {
        Driver driver = driverDao.getById(UUID.fromString(dto.getDriverId()))
                .orElseThrow(DriverNotFoundException::new);
        Car created = carMapper.toCar(dto);
        created.setDriver(driver);
        return carMapper.toDto(carDao.create(created));
    }

    @Override
    public String delete(String id) {
        return carDao.delete(UUID.fromString(id)).toString();
    }
}
