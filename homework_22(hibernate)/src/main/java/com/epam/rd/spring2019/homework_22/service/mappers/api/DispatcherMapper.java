package com.epam.rd.spring2019.homework_22.service.mappers.api;

import com.epam.rd.spring2019.homework_22.repository.domain.Dispatcher;
import com.epam.rd.spring2019.homework_22.service.dto.DispatcherCreateDto;
import com.epam.rd.spring2019.homework_22.service.dto.DispatcherDto;

public interface DispatcherMapper {
    Dispatcher toDispatcher(DispatcherDto dto);
    Dispatcher toDispatcher(DispatcherCreateDto dto);
    DispatcherDto toDto(Dispatcher dispatcher);
}
