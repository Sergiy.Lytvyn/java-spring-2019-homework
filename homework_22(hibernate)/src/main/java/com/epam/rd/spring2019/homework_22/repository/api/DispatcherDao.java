package com.epam.rd.spring2019.homework_22.repository.api;

import com.epam.rd.spring2019.homework_22.repository.domain.Dispatcher;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

public interface DispatcherDao {
    Optional<Dispatcher> getById(UUID id);
    Collection<Dispatcher> getAll();
    Dispatcher create(Dispatcher dispatcher);
    UUID delete(UUID id);
}
