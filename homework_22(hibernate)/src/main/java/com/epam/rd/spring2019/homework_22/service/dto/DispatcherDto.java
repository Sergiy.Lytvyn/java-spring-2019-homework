package com.epam.rd.spring2019.homework_22.service.dto;

import lombok.Builder;
import lombok.Value;

import java.util.Collection;

@Builder
@Value
public class DispatcherDto {
    private final String id;
    private final String name;
    private final Collection<RouteDto> routes;
}
