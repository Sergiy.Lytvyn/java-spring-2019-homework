package com.epam.rd.spring2019.homework_22.repository.api;

import com.epam.rd.spring2019.homework_22.repository.domain.Route;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

public interface RouteDao {
    Optional<Route> getById(UUID id);
    Collection<Route> getAll();
    Route create(Route route);
    UUID delete(UUID id);
}
