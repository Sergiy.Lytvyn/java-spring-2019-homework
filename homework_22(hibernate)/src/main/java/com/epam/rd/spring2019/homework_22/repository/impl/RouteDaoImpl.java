package com.epam.rd.spring2019.homework_22.repository.impl;

import com.epam.rd.spring2019.homework_22.repository.api.RouteDao;
import com.epam.rd.spring2019.homework_22.repository.domain.Route;
import com.epam.rd.spring2019.homework_22.repository.exception.RouteNotFoundException;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

@Repository
public class RouteDaoImpl implements RouteDao {

    private final SessionFactory sessionFactory;

    public RouteDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Optional<Route> getById(UUID id) {
        return Optional.ofNullable(sessionFactory.getCurrentSession().get(Route.class, id));
    }

    @Override
    public Collection<Route> getAll() {
        return sessionFactory.getCurrentSession().createQuery("FROM Route", Route.class).list();
    }

    @Transactional
    @Override
    public Route create(Route route) {
        sessionFactory.getCurrentSession().save(route);
        return route;
    }

    @Transactional
    @Override
    public UUID delete(UUID id) {
        Route route = getById(id).orElseThrow(RouteNotFoundException::new);
        sessionFactory.getCurrentSession().delete(route);
        return id;
    }
}
