package com.epam.rd.spring2019.homework_22.service.mappers.impl;

import com.epam.rd.spring2019.homework_22.repository.api.DispatcherDao;
import com.epam.rd.spring2019.homework_22.repository.domain.Dispatcher;
import com.epam.rd.spring2019.homework_22.repository.domain.Route;
import com.epam.rd.spring2019.homework_22.repository.exception.DispatcherNotFoundException;
import com.epam.rd.spring2019.homework_22.service.dto.RouteCreateDto;
import com.epam.rd.spring2019.homework_22.service.dto.RouteDto;
import com.epam.rd.spring2019.homework_22.service.mappers.api.RouteMapper;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class RouteMapperImpl implements RouteMapper {

    private final DispatcherDao dispatcherDao;

    public RouteMapperImpl(DispatcherDao dispatcherDao) {
        this.dispatcherDao = dispatcherDao;
    }

    @Override
    public Route toRoute(RouteDto dto) {
        Route route = new Route();
        route.setId(UUID.fromString(dto.getId()));
        route.setStartPoint(dto.getStartPoint());
        route.setFinishPoint(dto.getFinishPoint());
        Dispatcher dispatcher = dispatcherDao.getById(UUID.fromString(dto.getDispatcherId()))
                .orElseThrow(DispatcherNotFoundException::new);
        route.setDispatcher(dispatcher);
        return route;
    }

    @Override
    public Route toRoute(RouteCreateDto dto) {
        Route route = new Route();
        route.setStartPoint(dto.getStartPoint());
        route.setFinishPoint(dto.getFinishPoint());
        Dispatcher dispatcher = dispatcherDao.getById(UUID.fromString(dto.getDispatcherId()))
                .orElseThrow(DispatcherNotFoundException::new);
        route.setDispatcher(dispatcher);
        return route;
    }

    @Override
    public Collection<Route> toRoute(Collection<RouteDto> dtoCollection) {
        return dtoCollection.stream()
                .map(this::toRoute)
                .collect(Collectors.toList());
    }

    @Override
    public RouteDto toDto(Route route) {
        return RouteDto.builder()
                .id(route.getId().toString())
                .startPoint(route.getStartPoint())
                .finishPoint(route.getFinishPoint())
                .dispatcherId(route.getDispatcher().getId().toString())
                .build();
    }

    @Override
    public Collection<RouteDto> toDto(Collection<Route> routes) {
        return routes.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }
}
