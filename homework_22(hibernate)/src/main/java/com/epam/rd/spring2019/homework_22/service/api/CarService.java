package com.epam.rd.spring2019.homework_22.service.api;

import com.epam.rd.spring2019.homework_22.service.dto.CarCreateDto;
import com.epam.rd.spring2019.homework_22.service.dto.CarDto;

import java.util.Collection;
import java.util.Optional;

public interface CarService {
    Optional<CarDto> getById(String id);
    Collection<CarDto> getAll();
    CarDto update(CarDto updateDto);
    CarDto create(CarCreateDto dto);
    String delete(String id);
}
