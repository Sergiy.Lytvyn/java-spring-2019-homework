package com.epam.rd.spring2019.homework_22.repository.impl;

import com.epam.rd.spring2019.homework_22.repository.api.DriverDao;
import com.epam.rd.spring2019.homework_22.repository.domain.Driver;
import com.epam.rd.spring2019.homework_22.repository.exception.DriverNotFoundException;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

@Repository
public class DriverDaoImpl implements DriverDao {

    private final SessionFactory sessionFactory;

    public DriverDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Transactional
    @Override
    public Driver create(Driver driver) {
        sessionFactory.getCurrentSession().save(driver);
        return driver;
    }

    @Override
    public Optional<Driver> getById(UUID id) {
        return Optional.ofNullable(sessionFactory.getCurrentSession().get(Driver.class, id));
    }

    @Override
    public Collection<Driver> getAll() {
        return sessionFactory.getCurrentSession().createQuery("FROM Driver", Driver.class).list();
    }

    @Transactional
    @Override
    public UUID delete(UUID id) {
        Driver driver = getById(id).orElseThrow(DriverNotFoundException::new);
        sessionFactory.getCurrentSession().delete(driver);
        return id;
    }
}
