package com.epam.rd.spring2019.homework_22.service.impl;

import com.epam.rd.spring2019.homework_22.repository.api.RouteDao;
import com.epam.rd.spring2019.homework_22.repository.domain.Route;
import com.epam.rd.spring2019.homework_22.repository.exception.RouteNotFoundException;
import com.epam.rd.spring2019.homework_22.service.api.RouteService;
import com.epam.rd.spring2019.homework_22.service.dto.RouteCreateDto;
import com.epam.rd.spring2019.homework_22.service.dto.RouteDto;
import com.epam.rd.spring2019.homework_22.service.mappers.api.RouteMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

@Service
public class RouteServiceImpl implements RouteService {

    private final RouteDao routeDao;
    private final RouteMapper routeMapper;

    public RouteServiceImpl(RouteDao routeDao, RouteMapper routeMapper) {
        this.routeDao = routeDao;
        this.routeMapper = routeMapper;
    }

    private Route performUpdate(Route persist, Route updated){
        persist.setDispatcher(updated.getDispatcher());
        persist.setStartPoint(updated.getStartPoint());
        persist.setFinishPoint(updated.getFinishPoint());
        return persist;
    }

    @Override
    public RouteDto create(RouteCreateDto dto) {
        Route created = routeDao.create(routeMapper.toRoute(dto));
        return routeMapper.toDto(created);
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<RouteDto> getById(String id) {
        return routeDao.getById(UUID.fromString(id))
                .map(routeMapper::toDto);
    }

    @Transactional(readOnly = true)
    @Override
    public Collection<RouteDto> getAll() {
        return routeMapper.toDto(routeDao.getAll());
    }

    @Transactional
    @Override
    public RouteDto update(RouteDto dto) {
        Route persist = routeDao.getById(UUID.fromString(dto.getId()))
                .orElseThrow(RouteNotFoundException::new);
        Route updated = routeMapper.toRoute(dto);
        return routeMapper.toDto(performUpdate(persist, updated));
    }

    @Override
    public String delete(String id) {
        return routeDao.delete(UUID.fromString(id)).toString();
    }
}
