package com.epam.rd.spring2019.homework_22.service.dto;

import lombok.Value;

@Value
public class DriverDto {
    private final String id;
    private final String name;
}
