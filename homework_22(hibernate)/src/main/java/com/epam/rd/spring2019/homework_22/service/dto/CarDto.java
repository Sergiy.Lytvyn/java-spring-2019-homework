package com.epam.rd.spring2019.homework_22.service.dto;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class CarDto {
    private final String id;
    private final String brand;
    private final String model;
    private final DriverDto driver;
}
