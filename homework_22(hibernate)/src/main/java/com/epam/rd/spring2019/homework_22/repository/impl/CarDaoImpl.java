package com.epam.rd.spring2019.homework_22.repository.impl;

import com.epam.rd.spring2019.homework_22.repository.api.CarDao;
import com.epam.rd.spring2019.homework_22.repository.domain.Car;
import com.epam.rd.spring2019.homework_22.repository.exception.CarNotFoundException;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

@Repository
public class CarDaoImpl implements CarDao {

    private final SessionFactory sessionFactory;

    public CarDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @Transactional
    public Car create(Car car) {
        sessionFactory.getCurrentSession().save(car);
        return car;
    }

    @Override
    public Optional<Car> getById(UUID id) {
        return Optional.ofNullable(sessionFactory.getCurrentSession().get(Car.class, id));
    }

    @Override
    public Collection<Car> getAll() {
        return sessionFactory.getCurrentSession().createQuery("FROM Car", Car.class).list();
    }

    @Override
    @Transactional
    public UUID delete(UUID id) {
        Car car = getById(id).orElseThrow(CarNotFoundException::new);
        sessionFactory.getCurrentSession().delete(car);
        return id;
    }

}
