package com.epam.rd.spring2019.homework_22.service.dto;

import lombok.Value;

@Value
public class DriverCreateDto {
    private final String name;
}
