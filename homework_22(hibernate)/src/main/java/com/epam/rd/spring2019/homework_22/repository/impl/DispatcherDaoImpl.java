package com.epam.rd.spring2019.homework_22.repository.impl;

import com.epam.rd.spring2019.homework_22.repository.api.DispatcherDao;
import com.epam.rd.spring2019.homework_22.repository.domain.Dispatcher;
import com.epam.rd.spring2019.homework_22.repository.exception.DispatcherNotFoundException;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Repository
public class DispatcherDaoImpl implements DispatcherDao {

    private final SessionFactory sessionFactory;

    public DispatcherDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Optional<Dispatcher> getById(UUID id) {
        return Optional.ofNullable(sessionFactory.getCurrentSession().get(Dispatcher.class, id));
    }

    @Override
    public Collection<Dispatcher> getAll() {
        return sessionFactory.getCurrentSession().createQuery("FROM Dispatcher", Dispatcher.class).list();
    }

    @Transactional
    @Override
    public Dispatcher create(Dispatcher dispatcher) {
        sessionFactory.getCurrentSession().save(dispatcher);
        return dispatcher;
    }

    @Transactional
    @Override
    public UUID delete(UUID id) {
        Dispatcher dispatcher = getById(id).orElseThrow(DispatcherNotFoundException::new);
        sessionFactory.getCurrentSession().delete(dispatcher);
        return id;
    }
}
