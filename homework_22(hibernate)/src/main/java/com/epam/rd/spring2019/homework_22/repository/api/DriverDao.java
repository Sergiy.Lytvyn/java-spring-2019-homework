package com.epam.rd.spring2019.homework_22.repository.api;

import com.epam.rd.spring2019.homework_22.repository.domain.Driver;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

public interface DriverDao {
    Driver create(Driver driver);
    Optional<Driver> getById(UUID id);
    Collection<Driver> getAll();
    UUID delete(UUID id);
}
