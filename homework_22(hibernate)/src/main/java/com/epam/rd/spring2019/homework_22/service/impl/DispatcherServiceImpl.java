package com.epam.rd.spring2019.homework_22.service.impl;

import com.epam.rd.spring2019.homework_22.repository.api.DispatcherDao;
import com.epam.rd.spring2019.homework_22.repository.domain.Dispatcher;
import com.epam.rd.spring2019.homework_22.repository.domain.Route;
import com.epam.rd.spring2019.homework_22.repository.exception.DispatcherNotFoundException;
import com.epam.rd.spring2019.homework_22.service.api.DispatcherService;
import com.epam.rd.spring2019.homework_22.service.dto.DispatcherCreateDto;
import com.epam.rd.spring2019.homework_22.service.dto.DispatcherDto;
import com.epam.rd.spring2019.homework_22.service.mappers.api.DispatcherMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class DispatcherServiceImpl implements DispatcherService {

    private final DispatcherDao dispatcherDao;
    private final DispatcherMapper dispatcherMapper;

    public DispatcherServiceImpl(DispatcherDao dispatcherDao, DispatcherMapper dispatcherMapper) {
        this.dispatcherDao = dispatcherDao;
        this.dispatcherMapper = dispatcherMapper;
    }

    private Dispatcher performUpdate(Dispatcher persist, Dispatcher updated){
        persist.setName(updated.getName());
        updateDispatcherRoutes(persist.getRoutes(), updated.getRoutes());
        return persist;
    }

    private void updateDispatcherRoutes(Collection<Route> persist, Collection<Route> updated){
        Collection<Route> toRemove = persist.stream()
                .filter(route -> !updated.contains(route))
                .collect(Collectors.toList());
        Collection<Route> toAdd = updated.stream()
                .filter(route -> !persist.contains(route))
                .collect(Collectors.toList());
        persist.removeAll(toRemove);
        persist.addAll(toAdd);
    }

    @Override
    public DispatcherDto create(DispatcherCreateDto dto) {
        Dispatcher created =  dispatcherDao.create(dispatcherMapper.toDispatcher(dto));
        return dispatcherMapper.toDto(created);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DispatcherDto> getById(String id) {
        return dispatcherDao.getById(UUID.fromString(id))
                .map(dispatcherMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Collection<DispatcherDto> getAll() {
        return dispatcherDao.getAll().stream()
                .map(dispatcherMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public DispatcherDto update(DispatcherDto dto) {
        Dispatcher persist = dispatcherDao.getById(UUID.fromString(dto.getId())).orElseThrow(DispatcherNotFoundException::new);
        Dispatcher updated = dispatcherMapper.toDispatcher(dto);
        return dispatcherMapper.toDto(performUpdate(persist, updated));
    }

    @Override
    public String delete(String id) {
        return dispatcherDao.delete(UUID.fromString(id)).toString();
    }
}
