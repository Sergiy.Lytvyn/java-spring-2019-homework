package com.epam.rd.spring2019.homework_22.repository.api;

import com.epam.rd.spring2019.homework_22.repository.domain.Car;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

public interface CarDao {
    Car create(Car car);
    Optional<Car> getById(UUID id);
    Collection<Car> getAll();
    UUID delete(UUID id);
}
