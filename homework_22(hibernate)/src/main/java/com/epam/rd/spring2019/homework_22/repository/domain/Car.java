package com.epam.rd.spring2019.homework_22.repository.domain;

import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Car {

    @Id
    @Type(type = "uuid-char")
    private UUID id = UUID.randomUUID();
    private String brand;
    private String model;
    @ManyToOne
    @JoinColumn(name = "fk_driver_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Driver driver;

}
