package com.epam.rd.spring2019.homework_22.service.impl;

import com.epam.rd.spring2019.homework_22.repository.api.DriverDao;
import com.epam.rd.spring2019.homework_22.repository.domain.Driver;
import com.epam.rd.spring2019.homework_22.repository.exception.DriverNotFoundException;
import com.epam.rd.spring2019.homework_22.service.api.DriverService;
import com.epam.rd.spring2019.homework_22.service.dto.DriverCreateDto;
import com.epam.rd.spring2019.homework_22.service.dto.DriverDto;
import com.epam.rd.spring2019.homework_22.service.mappers.api.DriverMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class DriverServiceImpl implements DriverService {

    private final DriverDao driverDao;
    private final DriverMapper driverMapper;

    public DriverServiceImpl(DriverDao driverDao, DriverMapper driverMapper) {
        this.driverDao = driverDao;
        this.driverMapper = driverMapper;
    }

    private Driver performUpdate(Driver persist, DriverDto dto){
        persist.setName(dto.getName());
        return persist;
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<DriverDto> getById(String id) {
        Optional<Driver> driver = driverDao.getById(UUID.fromString(id));
        return driver.map(driverMapper::toDto);
    }

    @Transactional(readOnly = true)
    @Override
    public Collection<DriverDto> getAll() {
        return driverDao.getAll().stream()
                .map(driverMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public DriverDto save(DriverCreateDto dto) {
        Driver driver =  driverDao.create(driverMapper.toDriver(dto));
        return driverMapper.toDto(driver);
    }

    @Override
    public String deleteById(String id) {
        return driverDao.delete(UUID.fromString(id)).toString();
    }

    @Transactional
    @Override
    public DriverDto update(DriverDto dto) {
        Driver persist = driverDao.getById(UUID.fromString(dto.getId()))
                .orElseThrow(DriverNotFoundException::new);
        return driverMapper.toDto(performUpdate(persist, dto));
    }

}
