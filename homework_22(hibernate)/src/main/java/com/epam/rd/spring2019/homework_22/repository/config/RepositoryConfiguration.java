package com.epam.rd.spring2019.homework_22.repository.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {
        "com.epam.rd.spring2019.homework_22.repository.impl"
})
public class RepositoryConfiguration {
}
