package com.epam.rd.spring2019.homework_22.service.mappers.api;

import com.epam.rd.spring2019.homework_22.repository.domain.Route;
import com.epam.rd.spring2019.homework_22.service.dto.RouteCreateDto;
import com.epam.rd.spring2019.homework_22.service.dto.RouteDto;

import java.util.Collection;

public interface RouteMapper {
    Route toRoute(RouteDto dto);
    Route toRoute(RouteCreateDto dto);
    Collection<Route> toRoute(Collection<RouteDto> dtoCollection);
    RouteDto toDto(Route route);
    Collection<RouteDto> toDto(Collection<Route> routes);
}
