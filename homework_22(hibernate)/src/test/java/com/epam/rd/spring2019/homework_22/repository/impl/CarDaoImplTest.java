package com.epam.rd.spring2019.homework_22.repository.impl;

import com.epam.rd.spring2019.homework_22.config.TestContextConfiguration;
import com.epam.rd.spring2019.homework_22.repository.api.CarDao;
import com.epam.rd.spring2019.homework_22.repository.api.DriverDao;
import com.epam.rd.spring2019.homework_22.repository.domain.Car;
import com.epam.rd.spring2019.homework_22.repository.domain.Driver;
import com.epam.rd.spring2019.homework_22.repository.exception.CarNotFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringJUnitConfig(TestContextConfiguration.class)
@Sql(scripts = {"/insert_drivers.sql","/insert_cars.sql"})
@Transactional
class CarDaoImplTest {

    @Autowired
    private CarDao sut;
    @Autowired
    private DriverDao driverDao;

    @Test
    void save() {
        //GIVEN
        Driver driver = driverDao.getById(UUID.fromString("8ac64a65-3f38-4584-9907-bc1cd3aa6ecc"))
                .orElseThrow(RuntimeException::new);
        Car newCar = new Car(UUID.randomUUID(), "Brand", "Model", driver);
        //WHEN
        sut.create(newCar);
        //THAN
        assertEquals(4, sut.getAll().size());
        assertTrue(sut.getAll().contains(newCar));
    }

    @Test
    void getById() {
        //WHEN
        Optional<Car> mercedes = sut.getById(UUID.fromString("8b020699-8739-44fe-c523-fe777c4a5411"));
        Optional<Car> mazda = sut.getById(UUID.fromString("8b020699-8739-44fe-c523-fe777c4a5413"));
        Optional<Car> nonExist = sut.getById(UUID.fromString("2b020699-8739-44fe-c523-fe777c4a5413"));
        //THAN
        assertTrue(mercedes.isPresent());
        assertTrue(mazda.isPresent());
        assertFalse(nonExist.isPresent());
        assertEquals("Mercedes", mercedes.get().getBrand());
        assertEquals("Mazda", mazda.get().getBrand());
    }

    @Test
    void getAll() {
        //WHEN
        Collection<Car> cars = sut.getAll();
        Optional<Car> mazda = cars
                .stream()
                .filter(car ->"Mazda".equals(car.getBrand()))
                .findAny();
        //THAN
        assertEquals(3, cars.size());
        assertTrue(mazda.isPresent());
    }

    @Test
    void delete() {
        //GIVEN
        Collection<Car> cars = sut.getAll();
        //WHEN
        Car car = cars.stream().findAny().orElseThrow(RuntimeException::new);
        sut.delete(car.getId());
        //THEN
        assertEquals(2, sut.getAll().size());
        assertFalse(sut.getAll().contains(car));
        assertThrows(CarNotFoundException.class, () -> sut.delete(UUID.fromString("2b020699-8739-44fe-c523-fe777c4a5413")));
    }
}