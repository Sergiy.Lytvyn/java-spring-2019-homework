package com.epam.rd.spring2019.homework_22.repository.impl;

import com.epam.rd.spring2019.homework_22.config.TestContextConfiguration;
import com.epam.rd.spring2019.homework_22.repository.api.DriverDao;
import com.epam.rd.spring2019.homework_22.repository.domain.Driver;
import com.epam.rd.spring2019.homework_22.repository.exception.DriverNotFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringJUnitConfig(TestContextConfiguration.class)
@Sql(scripts = {"/insert_drivers.sql", "/insert_cars.sql"})
@Transactional
class DriverDaoImplTest {

    @Autowired
    private DriverDao sut;

    @Test
    void save() {
        //GIVEN
        Driver newDriver = new Driver(UUID.randomUUID(),"Driver");
        //WHEN
        sut.create(newDriver);
        //THAN
        assertTrue(sut.getById(newDriver.getId()).isPresent());
        assertEquals(4, sut.getAll().size());
        assertTrue(sut.getAll().contains(newDriver));
    }

    @Test
    void getById() {
        //WHEN
        Optional<Driver> first = sut.getById(UUID.fromString("8ac64a65-3f38-4584-9907-bc1cd3aa6ecc"));
        Optional<Driver> second = sut.getById(UUID.fromString("bf53fac8-6140-4376-a2b6-930f284bcb92"));
        Optional<Driver> nonExist = sut.getById(UUID.fromString("c1ace717-fb0e-4964-800a-3639483a887e"));
        //THAN
        assertTrue(first.isPresent());
        assertTrue(second.isPresent());
        assertFalse(nonExist.isPresent());
        assertEquals("Ted", second.get().getName());
        assertEquals("Michel", first.get().getName());
    }

    @Test
    void getAll() {
        //WHEN
        Collection<Driver> drivers = sut.getAll();
        //THAN
        assertEquals(3, drivers.size());
    }

    @Test
    void delete() {
        //GIVEN
        UUID id = UUID.fromString("8ac64a65-3f38-4584-9907-bc1cd3aa6ecc");
        Optional<Driver> driver = sut.getById(id);
        //WHEN
        sut.delete(id);
        //THAN
        assertEquals(2, sut.getAll().size());
        assertTrue(driver.isPresent());
        assertFalse(sut.getById(id).isPresent());
        assertThrows(DriverNotFoundException.class, ()->sut.delete(UUID.fromString("c1ace717-fb0e-4964-800a-3639483a887e")));
    }
}