package com.epam.rd.spring2019.homework_22.service.impl;

import com.epam.rd.spring2019.homework_22.config.TestContextConfiguration;
import com.epam.rd.spring2019.homework_22.repository.exception.CarNotFoundException;
import com.epam.rd.spring2019.homework_22.service.api.CarService;
import com.epam.rd.spring2019.homework_22.service.api.DriverService;
import com.epam.rd.spring2019.homework_22.service.dto.CarCreateDto;
import com.epam.rd.spring2019.homework_22.service.dto.CarDto;
import com.epam.rd.spring2019.homework_22.service.dto.DriverDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringJUnitConfig(TestContextConfiguration.class)
@Sql(scripts = {"/insert_drivers.sql","/insert_cars.sql"})
@Transactional
class CarServiceImplTest {

    @Autowired
    private CarService sut;
    @Autowired
    private DriverService driverService;

    @Test
    void getById() {
        //WHEN
        Optional<CarDto> mercedes = sut.getById("8b020699-8739-44fe-c523-fe777c4a5411");
        Optional<CarDto> mazda = sut.getById("8b020699-8739-44fe-c523-fe777c4a5413");
        Optional<CarDto> nonExist = sut.getById("2b020699-8739-44fe-c523-fe777c4a5413");
        //THAN
        assertTrue(mercedes.isPresent());
        assertTrue(mazda.isPresent());
        assertFalse(nonExist.isPresent());
        assertEquals("Mercedes", mercedes.get().getBrand());
        assertEquals("Mazda", mazda.get().getBrand());
    }

    @Test
    void getAll() {
        //WHEN
        Collection<CarDto> cars = sut.getAll();
        Optional<CarDto> mazda = cars
                .stream()
                .filter(car ->"Mazda".equals(car.getBrand()))
                .findAny();
        //THAN
        assertEquals(3, cars.size());
        assertTrue(mazda.isPresent());
    }

    @Test
    void update() {
        //GIVEN
        DriverDto driverDto = driverService.getById("8ac64a65-3f38-4584-9907-bc1cd3aa6ecc").orElseThrow(RuntimeException::new);
        CarDto toUpdate = CarDto.builder()
                .id("8b020699-8739-44fe-c523-fe777c4a5411")
                .brand("Brand")
                .model("Model")
                .driver(driverDto)
                .build();
        //WHEN
        sut.update(toUpdate);
        //THAN
        assertTrue(sut.getAll().contains(toUpdate));
        assertEquals(3, sut.getAll().size());
    }

    @Test
    void create() {
        //GIVEN
        CarCreateDto newCar = new CarCreateDto("Model", "Brand", "8ac64a65-3f38-4584-9907-bc1cd3aa6ecc");
        //WHEN
        sut.create(newCar);
        //THAN
        assertEquals(4, sut.getAll().size());
        assertTrue(sut.getAll().stream()
                .anyMatch(carDto -> carDto.getModel().equals(newCar.getModel()) && carDto.getBrand().equals(newCar.getBrand())));
    }

    @Test
    void delete() {
        //GIVEN
        Collection<CarDto> cars = sut.getAll();
        //WHEN
        CarDto car = cars.stream().findAny().orElseThrow(RuntimeException::new);
        sut.delete(car.getId());
        //THEN
        assertEquals(2, sut.getAll().size());
        assertFalse(sut.getAll().contains(car));
        assertThrows(CarNotFoundException.class, () -> sut.delete(UUID.randomUUID().toString()));
    }
}