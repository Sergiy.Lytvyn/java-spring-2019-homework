package com.epam.rd.spring2019.homework_22.service.impl;

import com.epam.rd.spring2019.homework_22.config.TestContextConfiguration;
import com.epam.rd.spring2019.homework_22.repository.api.RouteDao;
import com.epam.rd.spring2019.homework_22.repository.domain.Dispatcher;
import com.epam.rd.spring2019.homework_22.repository.domain.Route;
import com.epam.rd.spring2019.homework_22.repository.exception.DispatcherNotFoundException;
import com.epam.rd.spring2019.homework_22.repository.exception.RouteNotFoundException;
import com.epam.rd.spring2019.homework_22.service.api.RouteService;
import com.epam.rd.spring2019.homework_22.service.dto.RouteCreateDto;
import com.epam.rd.spring2019.homework_22.service.dto.RouteDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;


@SpringJUnitConfig(TestContextConfiguration.class)
@Sql(scripts = {"/insert_dispatchers.sql", "/insert_routes.sql"})
@Transactional
class RouteServiceImplTest {

    @Autowired
    private RouteService sut;

    @Test
    void create() {
        //GIVEN
        String dispatcherId = "254806df-e7e5-4122-bab7-9b491fc476e0";
        RouteCreateDto route = RouteCreateDto.builder()
                .startPoint("Start")
                .finishPoint("Finish")
                .dispatcherId(dispatcherId)
                .build();
        //WHEN
        sut.create(route);
        //THEN
        assertEquals(7, sut.getAll().size());
        assertTrue(sut.getAll().stream()
                .anyMatch(r -> r.getStartPoint().equals(route.getStartPoint())
                        && r.getFinishPoint().equals(route.getFinishPoint())
                        && r.getDispatcherId().equals(route.getDispatcherId()
                )));
    }

    @Test
    void getById() {
        //GIVEN
        String id = "14828661-d0a5-4103-b990-3ea496525a6f";
        //THEN
        Optional<RouteDto> route = sut.getById(id);
        Optional<RouteDto> empty = sut.getById(UUID.randomUUID().toString());
        //WHEN
        assertTrue(route.isPresent());
        assertFalse(empty.isPresent());
        assertEquals("Kiev", route.get().getStartPoint());
        assertEquals("Dnipro", route.get().getFinishPoint());
        assertEquals("254806df-e7e5-4122-bab7-9b491fc476e0",route.get().getDispatcherId());
    }

    @Test
    void getAll() {
        //WHEN
        Collection<RouteDto> routes = sut.getAll();
        //THAN
        assertEquals(6, routes.size());
        assertFalse(routes.stream().anyMatch(Objects::isNull));
    }

    @Test
    void update() {
        //GIVEN
        RouteDto existedRoute = sut.getById("14828661-d0a5-4103-b990-3ea496525a6f").orElseThrow(RouteNotFoundException::new);
        RouteDto toUpdate = RouteDto.builder()
                .id(existedRoute.getId())
                .startPoint("Start")
                .finishPoint("Finish")
                .dispatcherId("29835cf2-03a6-4a01-99b7-6b5b7b8e44c7")
                .build();
        //WHEN
        sut.update(toUpdate);
        //THAN
        assertEquals(6, sut.getAll().size());
        assertTrue(sut.getAll().contains(toUpdate));
        assertFalse(sut.getAll().contains(existedRoute));
        assertThrows(RouteNotFoundException.class, () -> sut.update(RouteDto.builder()
                .id(UUID.randomUUID().toString())
                .build())
        );
        assertThrows(DispatcherNotFoundException.class, () -> sut.update(RouteDto.builder()
                .id(existedRoute.getId())
                .dispatcherId(UUID.randomUUID().toString())
                .build())
        );

    }

    @Test
    void delete() {
        //GIVEN
        String id = "14828661-d0a5-4103-b990-3ea496525a6f";
        RouteDto route = sut.getById(id).orElseThrow(RuntimeException::new);
        //WHEN
        sut.delete(id);
        //THAN
        assertEquals(5, sut.getAll().size());
        assertFalse(sut.getAll().contains(route));
        assertThrows(RouteNotFoundException.class, () -> sut.delete(UUID.randomUUID().toString()));
    }
}