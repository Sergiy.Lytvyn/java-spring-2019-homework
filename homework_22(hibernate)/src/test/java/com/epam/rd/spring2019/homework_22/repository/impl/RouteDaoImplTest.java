package com.epam.rd.spring2019.homework_22.repository.impl;

import com.epam.rd.spring2019.homework_22.config.TestContextConfiguration;
import com.epam.rd.spring2019.homework_22.repository.api.DispatcherDao;
import com.epam.rd.spring2019.homework_22.repository.api.RouteDao;
import com.epam.rd.spring2019.homework_22.repository.domain.Dispatcher;
import com.epam.rd.spring2019.homework_22.repository.domain.Route;
import com.epam.rd.spring2019.homework_22.repository.exception.RouteNotFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringJUnitConfig(TestContextConfiguration.class)
@Sql(scripts = {"/insert_dispatchers.sql", "/insert_routes.sql"})
@Transactional
class RouteDaoImplTest {

    @Autowired
    private RouteDao sut;
    @Autowired
    private DispatcherDao dispatcherDao;

    @Test
    void getById() {
        //GIVEN
        UUID id = UUID.fromString("14828661-d0a5-4103-b990-3ea496525a6f");
        //THEN
        Optional<Route> route = sut.getById(id);
        Optional<Route> empty = sut.getById(UUID.randomUUID());
        //WHEN
        assertTrue(route.isPresent());
        assertFalse(empty.isPresent());
        assertEquals("Kiev", route.get().getStartPoint());
        assertEquals("Dnipro", route.get().getFinishPoint());
        assertNotNull(route.get().getDispatcher());
    }

    @Test
    void getAll() {
        //WHEN
        Collection<Route> routes = sut.getAll();
        //THAN
        assertEquals(6, routes.size());
        assertFalse(routes.stream().anyMatch(Objects::isNull));
    }

    @Test
    void create() {
        //GIVEN
        Dispatcher dispatcher = dispatcherDao.getAll()
                .stream()
                .findAny()
                .orElseThrow(RuntimeException::new);
        Route route = new Route();
        route.setDispatcher(dispatcher);
        route.setStartPoint("Barcelona");
        route.setFinishPoint("Madrid");
        //WHEN
        sut.create(route);
        //THEN
        assertTrue(sut.getAll().contains(route));
        assertEquals(7, sut.getAll().size());
        assertEquals(dispatcher, sut.getById(route.getId())
                .orElseThrow(RuntimeException::new)
                .getDispatcher());
    }

    @Test
    void delete() {
        //GIVEN
        UUID id = UUID.fromString("14828661-d0a5-4103-b990-3ea496525a6f");
        Route route = sut.getById(id).orElseThrow(RuntimeException::new);
        //WHEN
        sut.delete(id);
        //THAN
        assertEquals(5, sut.getAll().size());
        assertFalse(sut.getAll().contains(route));
        assertThrows(RouteNotFoundException.class, () -> sut.delete(UUID.randomUUID()));
    }
}