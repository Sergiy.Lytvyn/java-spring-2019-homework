package com.epam.rd.spring2019.homework_22.service.impl;

import com.epam.rd.spring2019.homework_22.config.TestContextConfiguration;
import com.epam.rd.spring2019.homework_22.repository.api.RouteDao;
import com.epam.rd.spring2019.homework_22.repository.domain.Dispatcher;
import com.epam.rd.spring2019.homework_22.repository.exception.DispatcherNotFoundException;
import com.epam.rd.spring2019.homework_22.service.api.DispatcherService;
import com.epam.rd.spring2019.homework_22.service.api.RouteService;
import com.epam.rd.spring2019.homework_22.service.dto.DispatcherCreateDto;
import com.epam.rd.spring2019.homework_22.service.dto.DispatcherDto;
import com.epam.rd.spring2019.homework_22.service.dto.RouteDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringJUnitConfig(TestContextConfiguration.class)
@Sql(scripts = {"/insert_dispatchers.sql", "/insert_routes.sql"})
@Transactional
class DispatcherServiceImplTest {

    @Autowired
    private DispatcherService sut;
    @Autowired
    private RouteService routeService;

    @Test
    void create() {
        //GIVEN
        DispatcherCreateDto dispatcher = new DispatcherCreateDto("David");
        //WHEN
        sut.create(dispatcher);
        //THAN
        assertEquals(4, sut.getAll().size());
        assertTrue(sut.getAll().stream()
                .anyMatch(d-> d.getName().equals(dispatcher.getName())));
    }

    @Test
    void getById() {
        //GIVEN
        String id = "254806df-e7e5-4122-bab7-9b491fc476e0";
        //WHEN
        Optional<DispatcherDto> dispatcher = sut.getById(id);
        Optional<DispatcherDto> empty = sut.getById(UUID.randomUUID().toString());
        //THAN
        assertTrue(dispatcher.isPresent());
        assertFalse(empty.isPresent());
        assertEquals("Dan", dispatcher.get().getName());
    }

    @Test
    void getAll() {
        //WHEN
        Collection<DispatcherDto> dispatchers = sut.getAll();
        //THAN
        assertEquals(3, dispatchers.size());
    }

    @Test
    void update() {
        //GIVEN
        DispatcherDto dispatcher = sut.getAll().stream().findFirst().orElseThrow(RuntimeException::new);
        RouteDto toRemove = dispatcher.getRoutes().stream().findAny().orElseThrow(RuntimeException::new);
        RouteDto toAdd = RouteDto.builder()
                .id(UUID.randomUUID().toString())
                .dispatcherId(dispatcher.getId())
                .startPoint("Start")
                .finishPoint("Finish")
                .build();
        dispatcher.getRoutes().remove(toRemove);
        dispatcher.getRoutes().add(toAdd);
        DispatcherDto toUpdate = DispatcherDto.builder()
                .id(dispatcher.getId())
                .name("Name")
                .routes(dispatcher.getRoutes())
                .build();
        //WHEN
        sut.update(toUpdate);
        DispatcherDto afterUpdate = sut.getById(dispatcher.getId()).orElseThrow(RuntimeException::new);
        //THAN
        assertEquals(6, routeService.getAll().size());
        assertEquals(3, sut.getAll().size());
        assertEquals(toUpdate.getName(), afterUpdate.getName());
        assertEquals(toUpdate.getRoutes(), afterUpdate.getRoutes());
        assertTrue(routeService.getAll().contains(toAdd));
        assertFalse(routeService.getAll().contains(toRemove));
        assertThrows(DispatcherNotFoundException.class,
                () -> sut.update(DispatcherDto.builder()
                        .id(UUID.randomUUID().toString())
                        .build())
        );
    }

    @Test
    void delete() {
        //GIVEN
        String id = "254806df-e7e5-4122-bab7-9b491fc476e0";
        DispatcherDto dispatcher = sut.getById(id).orElseThrow(RuntimeException::new);
        //WHEN
        sut.delete(id);
        //THAN
        assertEquals(2, sut.getAll().size());
        assertFalse(sut.getAll().contains(dispatcher));
        assertThrows(DispatcherNotFoundException.class, ()->sut.delete(UUID.randomUUID().toString()));
    }
}