package com.epam.rd.spring2019.homework_22.repository.impl;

import com.epam.rd.spring2019.homework_22.config.TestContextConfiguration;
import com.epam.rd.spring2019.homework_22.repository.api.DispatcherDao;
import com.epam.rd.spring2019.homework_22.repository.domain.Dispatcher;
import com.epam.rd.spring2019.homework_22.repository.exception.DispatcherNotFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringJUnitConfig(TestContextConfiguration.class)
@Sql(scripts = {"/insert_dispatchers.sql", "/insert_routes.sql"})
@Transactional
class DispatcherDaoImplTest {

    @Autowired
    private DispatcherDao sut;

    @Test
    void getById() {
        //GIVEN
        UUID id = UUID.fromString("254806df-e7e5-4122-bab7-9b491fc476e0");
        //WHEN
        Optional<Dispatcher> dispatcher = sut.getById(id);
        Optional<Dispatcher> empty = sut.getById(UUID.randomUUID());
        //THAN
        assertTrue(dispatcher.isPresent());
        assertFalse(empty.isPresent());
        assertEquals("Dan", dispatcher.get().getName());
    }

    @Test
    void getAll() {
        //WHEN
        Collection<Dispatcher> dispatchers = sut.getAll();
        //THAN
        assertEquals(3, dispatchers.size());
    }

    @Test
    void create() {
        //GIVEN
        Dispatcher dispatcher = new Dispatcher();
        dispatcher.setName("David");
        //WHEN
        sut.create(dispatcher);
        //THAN
        assertEquals(4, sut.getAll().size());
        assertTrue(sut.getAll().contains(dispatcher));
    }

    @Test
    void delete() {
        //GIVEN
        UUID id = UUID.fromString("254806df-e7e5-4122-bab7-9b491fc476e0");
        Dispatcher dispatcher = sut.getById(id).orElseThrow(RuntimeException::new);
        //WHEN
        sut.delete(id);
        //THAN
        assertEquals(2, sut.getAll().size());
        assertFalse(sut.getAll().contains(dispatcher));
        assertThrows(DispatcherNotFoundException.class, ()->sut.delete(UUID.randomUUID()));
    }
}