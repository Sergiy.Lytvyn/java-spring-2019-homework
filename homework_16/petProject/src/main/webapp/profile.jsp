<%@ page isELIgnored="false" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri="/WEB-INF/tag/language.tld" prefix="lan" %>
<html>
<head>
    <title><lan:print message="driver_profile"/></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

</head>
    <body>
    <jsp:include page="/footer"/>
    <c:set var="driver" value="${sessionScope.driver}"/>
    <h1><lan:print message="profile_welcome"/></h1>
    <h1><lan:print message="name"/>: <c:out value="${driver.name}"/></h1>
    <h1><lan:print message="email"/>: <c:out value="${driver.email}"/></h1>
    <form action="/pet-project/logout" method="post">
        <button type="submit" class="btn btn-default"><lan:print message="logout"/></button>
    </form>
    </body>
</html>