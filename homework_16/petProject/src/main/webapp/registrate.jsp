<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<%@ taglib uri="/WEB-INF/tag/language.tld" prefix="lan" %>
<html>
<head>
    <title><lan:print message="registrate"/></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<jsp:include page="/footer"/>
<div class="container">
    <div class="col-sm-4">
        <h1><lan:print message="registrate"/></h1>
        <form action="/pet-project/registrate" method="post">
            <div class="form-group">
                <label><lan:print message="email"/></label>
                <input type="email" class="form-control" name="email">
            </div>
            <div class="form-group">
                <label><lan:print message="password"/></label>
                <input type="password" class="form-control" name="pwd">
            </div>
            <div class="form-group">
                <label><lan:print message="name"/></label>
                <input type="text" class="form-control" name="name">
            </div>
            <button type="submit" class="btn btn-default"><lan:print message="registrate"/></button>
        </form>
    </div>
</div>
</body>
</html>
