<%@ page isELIgnored="false" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri="/WEB-INF/tag/language.tld" prefix="lan" %>
<html>
<body>
<jsp:include page="/footer"/>
<c:set var="mess" value="${sessionScope.mess}"/>
<h1> <lan:print message="error"/> : ${mess} </h1>
</body>
</html>