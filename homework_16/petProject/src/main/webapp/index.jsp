<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri="/WEB-INF/tag/language.tld" prefix="lan" %>
<%@ page isELIgnored="false" %>
<html>
   <head>
      <title><lan:print message="index_title"/></title>
       <meta charset="utf-8">
       <meta name="viewport" content="width=device-width, initial-scale=1">
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
       <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
   </head>
    <body>
    <jsp:include page="/footer"/>
    <c:set var="driver" value="${sessionScope.driver}"/>
    <c:choose>
    <c:when test="${driver != null}">
        <div class="jumbotron text-center">
            <h1><lan:print message="alredy_login"/></h1>
        </div>
        <a href="/pet-project/driver/profile" class="btn btn-info" role="button"><lan:print message="see_profile"/></a>
        <form action="/pet-project/logout" method="post">
            <button type="submit" class="btn btn-info"><lan:print message="logout"/></button>
        </form>
    </c:when>
       <c:otherwise>
           <div class="jumbotron text-center">
               <h1><lan:print message="welcome_guest"/></h1>
           <div class="container">
               <a href="/pet-project/loginPage" class="btn btn-info" role="button"><lan:print message="login"/></a>
               <a href="/pet-project/registratePage" class="btn btn-info" role="button"><lan:print message="registrate"/></a>
           </div>
       </c:otherwise>
    </c:choose>
    </body>
</html>


