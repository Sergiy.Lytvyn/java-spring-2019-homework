﻿<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<%@taglib prefix="lan" uri="/WEB-INF/tag/language.tld" %>
 <html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            color: black;
            text-align: center;
        }
    </style>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>

<body>
<div class="footer">
    <li class="btn btn-info"><a href="/pet-project/switchLanguage"><lan:print message="switch_language"/></a></li>
</div>
</body>
</html>
