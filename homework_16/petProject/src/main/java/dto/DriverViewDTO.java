package dto;

import java.io.Serializable;
import java.util.Objects;

public class DriverViewDTO implements Serializable {

    private static final long serialVersionUID = -1584900194159887831L;
    private String name;
    private String email;

    public DriverViewDTO() {
    }

    public DriverViewDTO(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DriverViewDTO that = (DriverViewDTO) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(email, that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, email);
    }
}
