package service.services;

import dto.DriverRegistrateDTO;
import dto.DriverViewDTO;
import exception.AppException;
import repository.dao.DriverDAO;
import repository.entity.Driver;
import dto.DriverLogInDTO;
import service.converter.DriverConverter;
import service.validation.Validator;

public class DriverServiceImpl implements DriverService{

    private static DriverServiceImpl ourInstance;
    private DriverDAO driverDAO;
    private Validator validator;
    private DriverConverter driverConverter;

    private DriverServiceImpl() {
        driverDAO = DriverDAO.getInstance();
        validator = new Validator();
        driverConverter = new DriverConverter();
    }

    public static DriverServiceImpl getInstance() {
        if(ourInstance == null){
            synchronized (DriverServiceImpl.class){
                if (ourInstance == null){
                    ourInstance = new DriverServiceImpl();
                }
            }
        }
        return ourInstance;
    }

    public DriverViewDTO create(DriverRegistrateDTO dto){
        validator.validateDriverRegistrationDTO(dto);
        try {
            driverDAO.getByEmail(dto.getEmail());
        } catch (AppException ex){
            Driver driver = driverConverter.convertDriverRegistrationToDriver(dto);
            driverDAO.create(driver);
            return driverConverter.convertDriverToDriverViewDTO(driver);
        }
        throw new AppException("Driver with email: " + dto.getEmail() + " already exist");
    }

    @Override
    public DriverViewDTO login(DriverLogInDTO driver) {
        validator.validateDriverLogInDTO(driver);
        Driver driverFromDB = driverDAO.getByEmail(driver.getEmail());
        if (driverFromDB.getPassword().equals(driver.getPassword())){
            return driverConverter.convertDriverToDriverViewDTO(driverFromDB);
        } else {
            throw new AppException("You input incorrect password");
        }
    }

}
