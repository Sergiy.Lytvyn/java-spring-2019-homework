package service.services;

import dto.DriverLogInDTO;
import dto.DriverRegistrateDTO;
import dto.DriverViewDTO;

public interface DriverService {

    DriverViewDTO create(DriverRegistrateDTO driver);

    DriverViewDTO login(DriverLogInDTO driver);

}
