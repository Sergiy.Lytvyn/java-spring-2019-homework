package service.validation;

import dto.DriverLogInDTO;
import dto.DriverRegistrateDTO;
import exception.ValidationException;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.List;
import java.util.stream.Collectors;

public class Validator {

    final static String EMAIL_REGEXP = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"" +
            "(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\"" +
            ")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}" +
            "(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:" +
            "(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)])";

    private List<String> validationFail;

    public Validator(){
        validationFail = new ArrayList<>();
    }

    public void validateEmail(String email) {
        Pattern pattern = Pattern.compile(EMAIL_REGEXP);
        Matcher matcher = pattern.matcher(email);
        if (!matcher.matches()){
            validationFail.add("You input bad email.");
        }
    }

    private void validateEmptyString(String string){
        if ((string == null) || (string.length() == 0)){
            validationFail.add("You input empty field.");
        }
    }

    public List<String> getValidationList(){
        return validationFail;
    }

    private void validate(){
        if (!validationFail.isEmpty()){
            String res = String.join(" ", validationFail);
            validationFail = new ArrayList<>();
            throw new ValidationException(res);
        }
    }

    public void validateDriverLogInDTO(DriverLogInDTO dto){
        validateEmail(dto.getEmail());
        validateEmptyString(dto.getPassword());
        validate();
    }

    public void validateDriverRegistrationDTO(DriverRegistrateDTO dto){
        validateEmail(dto.getEmail());
        validateEmptyString(dto.getPassword());
        validateEmptyString(dto.getName());
        validate();
    }

}
