package service.converter;

import dto.DriverRegistrateDTO;
import dto.DriverViewDTO;
import repository.entity.Driver;

public class DriverConverter {

    public DriverConverter() {
    }

    public Driver convertDriverRegistrationToDriver(DriverRegistrateDTO dto){
        Driver driver = new Driver();
        driver.setEmail(dto.getEmail());
        driver.setName(dto.getName());
        driver.setPassword(dto.getPassword());
        return driver;
    }

    public DriverViewDTO convertDriverToDriverViewDTO(Driver driver){
        DriverViewDTO dto = new DriverViewDTO();
        dto.setEmail(driver.getEmail());
        dto.setName(driver.getName());
        return dto;
    }
}
