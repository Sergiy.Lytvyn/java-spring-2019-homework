package tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Objects;

public class WelcomeTag extends TagSupport {

    private String name;

    @Override
    public int doStartTag() throws JspException {
        try {
            pageContext.getOut().write("Welcome To AutoBase 1.0, " + name);
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }

    @Override
    public void release() {
        super.release();
        this.name = null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
