package tag;

import tag.util.Utf8Control;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

public class LanguageTag extends TagSupport {

    private String message;
    private Utf8Control utf8Control = new Utf8Control();

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int doStartTag() throws JspException {
        HttpSession session = pageContext.getSession();
        if (session.getAttribute("LOCALE") == null) {
            session.setAttribute("LOCALE", new Locale("en"));
        }
        Locale locale = (Locale) session.getAttribute("LOCALE");
        if (message != null && !message.isEmpty()) {
            ResourceBundle messages = ResourceBundle.getBundle("i18n.messages", locale, utf8Control);
            String locMessage = messages.getString(message);
            try {
                pageContext.getOut().print(locMessage);
            } catch (IOException e) {
                throw new JspException(e.getMessage());
            }
        }
        return SKIP_BODY;
    }


}
