package repository.dao;

import exception.AppException;
import repository.entity.Driver;
import repository.util.ConnectionPool;

import java.sql.*;

public class DriverDAO implements DAO<Driver> {

    private static DriverDAO driverDAO;

    public DriverDAO(){
    }

    public static DriverDAO getInstance(){
        if (driverDAO == null){
            synchronized (DriverDAO.class){
                if (driverDAO == null){
                    driverDAO = new DriverDAO();
                }
            }
        }
        return driverDAO;
    }

    private Driver getDriverFromRS(ResultSet resultSet) throws SQLException {
        Driver driver = new Driver();
        Integer id = resultSet.getInt("driver_ID");
        String email = resultSet.getString("driver_email");
        String password = resultSet.getString("driver_password");
        String name = resultSet.getString("driver_name");
        driver.setDriverID(id);
        driver.setEmail(email);
        driver.setPassword(password);
        driver.setName(name);
        return driver;
    }

    @Override
    public Driver create(Driver driver) {
        final String CREATE_QUERY = "INSERT INTO drivers(driver_email, driver_password, driver_name) VALUES (?,?,?) ";
        try (Connection connection = ConnectionPool.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(CREATE_QUERY);
            preparedStatement.setString(1, driver.getEmail());
            preparedStatement.setString(2, driver.getPassword());
            preparedStatement.setString(3, driver.getName());
            preparedStatement.executeUpdate();
        } catch (SQLException ex){
            throw new AppException("can not create driver");
        }
        return driver;
    }


    @Override
    public Driver getByEmail(String email){
        final String GET_BY_EMAIL = "SELECT * FROM drivers WHERE driver_email = '"+ email + "';";
        try (Connection connection = ConnectionPool.getConnection()){
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(GET_BY_EMAIL);
            if (resultSet.next()) {
                return getDriverFromRS(resultSet);
            } else {
                throw new AppException("driver with email: " + email + " is not exist");
            }
        } catch (SQLException ex){
            throw new AppException("can not get driver by email");
        }
    }

}
