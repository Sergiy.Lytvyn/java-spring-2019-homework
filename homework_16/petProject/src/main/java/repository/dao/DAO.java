package repository.dao;

public interface DAO<T> {

    T create(T t);

    T getByEmail(String email);

}
