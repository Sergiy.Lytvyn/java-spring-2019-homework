package web.controllers;

import dto.DriverRegistrateDTO;
import dto.DriverViewDTO;
import service.services.DriverService;
import service.services.DriverServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(
        name = "Registrate",
        urlPatterns = "/registrate"
)
public class Registrate extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws  IOException {
        try {
            String name = req.getParameter("name");
            String password = req.getParameter("pwd");
            String email = req.getParameter("email");
            DriverRegistrateDTO registrateDTO = new DriverRegistrateDTO();
            registrateDTO.setEmail(email);
            registrateDTO.setPassword(password);
            registrateDTO.setName(name);
            DriverService driverService = DriverServiceImpl.getInstance();
            DriverViewDTO driver = driverService.create(registrateDTO);
            HttpSession session = req.getSession();
            session.setAttribute("driver", driver);
            resp.sendRedirect("/pet-project/driver/profile");
        } catch (RuntimeException ex){
            HttpSession session = req.getSession();
            session.setAttribute("mess", ex.getMessage());
            resp.sendRedirect("/pet-project/error");
        }
    }
}
