package web.controllers;

import dto.DriverLogInDTO;
import dto.DriverViewDTO;
import service.services.DriverService;
import service.services.DriverServiceImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(
        name = "Login",
        urlPatterns = "/login"
)
public class Login extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            String email = req.getParameter("email");
            String password = req.getParameter("pwd");
            DriverLogInDTO loginDTO = new DriverLogInDTO(email, password);
            DriverService driverService = DriverServiceImpl.getInstance();
            DriverViewDTO driver = driverService.login(loginDTO);
            HttpSession session = req.getSession();
            session.setAttribute("driver", driver);
            resp.sendRedirect("/pet-project/driver/profile");
        } catch (RuntimeException ex){
            HttpSession session = req.getSession();
            session.setAttribute("mess", ex.getMessage());
            resp.sendRedirect("/pet-project/error");
        }
    }

}
