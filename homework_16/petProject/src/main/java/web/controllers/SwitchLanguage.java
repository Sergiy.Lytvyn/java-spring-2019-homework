package web.controllers;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Locale;

@WebServlet(
        name = "SwitchLanguage",
        urlPatterns = "/switchLanguage"
)
public class SwitchLanguage extends HttpServlet {

    private static final Locale RuLocale = new Locale("ru");
    private static final Locale EnLocale = new Locale("en");

    private Locale getAnotherLocale(Locale locale){
        if (locale.equals(RuLocale)){
            return EnLocale;
        } else {
            return RuLocale;
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession();
        Locale locale = (Locale) session.getAttribute("LOCALE");
        session.setAttribute("LOCALE", getAnotherLocale(locale));
        String url = req.getHeader("referer");
        resp.sendRedirect(url);
    }





}
