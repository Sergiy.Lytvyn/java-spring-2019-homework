package compressor;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import org.apache.commons.io.FilenameUtils;

public class ZipCompressor implements Compressor{

    public ZipCompressor(){

    }

    private Path getTrimPathToFile(Path path, Path root){
        String filePath = path.toString();
        String rootPath = filePath.replace((root.toString() + File.separator), "");
        return Paths.get(rootPath);
    }

    private void addZipEntry(Path path, FileInputStream fileInputStream, ZipOutputStream zipOutputStream){
        try {
            ZipEntry zipEntry = new ZipEntry(path.toString());
            zipOutputStream.putNextEntry(zipEntry);
            byte[] buffer = new byte[fileInputStream.available()];
            fileInputStream.read(buffer);
            zipOutputStream.write(buffer);
            zipOutputStream.closeEntry();
        } catch (IOException ex){
            throw new UncheckedIOException("can't zip file: " + path, ex);
        }
    }

    private void checkZipFormat(Path file){
        String type = FilenameUtils.getExtension(file.toString());
        if ( !type.equals("zip") ) {
            throw new IllegalArgumentException("file must be only zip format");
        }
    }

    private void checkDirectoryForUnZip(Path directory){
        if(!Files.isDirectory(directory))
            throw new IllegalArgumentException("destination must be only directory");
    }

    private Path getNewFileNameIfCopyExists(Path destination){
        String baseName = destination.toString();
        String actualName = baseName;
        String fileName = FilenameUtils.getFullPath(baseName) + FilenameUtils.getBaseName(baseName);
        String extension = FilenameUtils.getExtension(baseName);
        int count = 0;
        while (Files.exists(Paths.get(actualName))){
            count++;
            actualName = fileName + "(" + count + ")." + extension;
        }
        return Paths.get(actualName);
    }

    private void zipFile(Path file, Path destination){
        checkZipFormat(destination);
        destination = getNewFileNameIfCopyExists(destination);
        try (FileInputStream fileInputStream = new FileInputStream(file.toFile());
             FileOutputStream fileOutputStream = new FileOutputStream(destination.toFile());
             ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream)){
            addZipEntry(file.getFileName(), fileInputStream, zipOutputStream);
        } catch (FileNotFoundException ex) {
            throw new UncheckedIOException("can't find file: " + file, ex);
        }
        catch (IOException ex){
            throw new UncheckedIOException("can't open or close file: " + file ,ex);
        }
    }

    private void zipDirectory(Path directory, Path destination) {
        checkZipFormat(destination);
        destination = getNewFileNameIfCopyExists(destination);
        try (FileOutputStream fileOutputStream = new FileOutputStream(destination.toFile());
             ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream)){
            Files.walk(directory).filter(Files::isRegularFile).forEach( path -> {
                try (FileInputStream fileInputStream = new FileInputStream(path.toFile())){
                    Path trimFilePath = getTrimPathToFile(path, directory);
                    addZipEntry(trimFilePath, fileInputStream, zipOutputStream);
                } catch (IOException ex){
                    throw new UncheckedIOException("can't read file: " + path, ex);
                }
            });
        } catch (FileNotFoundException ex) {
            throw new UncheckedIOException("can't find directory: " + directory, ex);
        }
        catch (IOException ex){
            throw new UncheckedIOException("can't open or close file: " + destination, ex);
        }
    }

    public void unCompress(Path zip, Path destination){
        checkZipFormat(zip);
        checkDirectoryForUnZip(destination);
        try (FileInputStream fileInputStream = new FileInputStream(zip.toFile());
             ZipInputStream zipInputStream = new ZipInputStream(fileInputStream)){
            byte [] buffer = new byte[2048];
            ZipEntry zipEntry;
            int length;
            while ((zipEntry = zipInputStream.getNextEntry()) != null){
                Path path = Paths.get(destination.toString(), zipEntry.getName());
                Files.createDirectories(path.getParent());
                try (FileOutputStream fileOutputStream = new FileOutputStream(path.toFile())){
                    while ((length = zipInputStream.read(buffer)) > 0) {
                        fileOutputStream.write(buffer, 0, length);
                    }
                } catch (IOException ex){
                    throw new UncheckedIOException("can't read/write file", ex);
                }
            }
        } catch (FileNotFoundException ex){
            throw new UncheckedIOException("can't find zip file: " + zip, ex);
        }
        catch (IOException ex) {
            throw new UncheckedIOException("can't unzip file: " + zip, ex);
        }
    }

    public void compress(Path file, Path destination){
        if (Files.isDirectory(file)) {
            zipDirectory(file, destination);
        } else {
            zipFile(file, destination);
        }
    }

}