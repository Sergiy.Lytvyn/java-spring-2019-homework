package compressor;

import java.nio.file.Path;

public interface Compressor {

    void compress(Path file, Path destination);

    void unCompress(Path file, Path destination);
}
