package TestCompressors;

import compressor.Compressor;
import compressor.ZipCompressor;
import org.junit.*;
import org.hamcrest.*;
import org.junit.rules.ExpectedException;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class TestZipCompressor {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private Compressor zipCompressor = new ZipCompressor();
    private String pathToSources = "src/test/resources/forZip";
    private String pathToResult = "src/test/resources/result";
    private ArrayList<String> listOfEntriesName = new ArrayList<>();

    private void addAllZipEntriesName(Path zipFile, List list){
        try(ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(zipFile.toFile()))){
            ZipEntry zipEntry;
            while ((zipEntry = zipInputStream.getNextEntry()) != null){
                list.add(zipEntry.getName());
            }
        } catch (IOException ex){
            throw new UncheckedIOException(ex);
        }
    }

    @After
    public void cleaningResult(){
        Path result = Paths.get(pathToResult);
        Path pathToGitignore = Paths.get(pathToResult, ".gitignore");
        try{
            Files.walk(result).filter(path -> (!path.equals(result) && (!path.equals(pathToGitignore))))
                    .sorted(Comparator.comparingInt(Path::getNameCount).reversed())
                    .forEach(path -> {
                try {
                    Files.deleteIfExists(path);
                } catch (IOException ex){
                    throw new UncheckedIOException(ex);
                }
            });
        } catch (IOException ex){
            throw new UncheckedIOException(ex);
        }
    }

    @Test
    public void testZipFile() {
        //GIVEN
        Path fileZip = Paths.get(pathToSources, "alice.txt");
        Path destination = Paths.get(pathToResult, "alice.zip");
        boolean markerFalse = Files.exists(destination);
        //WHEN
        zipCompressor.compress(fileZip, destination);
        addAllZipEntriesName(destination, listOfEntriesName);
        //THEN
        Assert.assertFalse(markerFalse);
        Assert.assertTrue(Files.exists(destination));
        Assert.assertTrue(Files.exists(fileZip));
        Assert.assertThat(listOfEntriesName, Matchers.hasItem(fileZip.getFileName().toString()));
        Assert.assertThat(listOfEntriesName, Matchers.hasSize(1));
    }

    @Test
    public void testZipFileWithIllegalDestination(){
        //GIVEN
        Path fileZip = Paths.get(pathToSources,"alice.txt");
        Path destination = Paths.get(pathToResult, "text.txt");
        expectedException.expect(IllegalArgumentException.class);
        //WHEN
        zipCompressor.compress(fileZip, destination);
    }

    @Test
    public void testZipDirectoryWithFilesAndSubdirectory(){
        //GIVEN
        Path dir = Paths.get(pathToSources);
        Path dest = Paths.get(pathToResult, "forZip.zip");
        boolean markerFalse = Files.exists(dest);
        //WHEN
        zipCompressor.compress(dir, dest);
        addAllZipEntriesName(dest, listOfEntriesName);
        //THEN
        Assert.assertFalse(markerFalse);
        Assert.assertTrue(Files.exists(dest));
        Assert.assertTrue(Files.exists(dir));
        Assert.assertThat(listOfEntriesName, Matchers.hasItem("d1"+ File.separator +"videoplayback.mp4"));
        Assert.assertThat(listOfEntriesName, Matchers.hasSize(2));
    }

    @Test
    public void testUnZipFile(){
        Path fileZip = Paths.get(pathToSources, "alice.txt");
        Path destination = Paths.get(pathToResult, "alice.zip");
        Path fileAfterUnZip = Paths.get(pathToResult, "alice.txt");
        //WHEN
        zipCompressor.compress(fileZip, destination);
        zipCompressor.unCompress(destination, Paths.get(pathToResult));
        //THEN
        Assert.assertTrue(Files.exists(fileAfterUnZip));
        Assert.assertTrue(Files.exists(destination));
        Assert.assertEquals(fileAfterUnZip.toFile().length(), fileZip.toFile().length());
    }

    @Test
    public void testUnZipDirectory(){
        Path fileZip = Paths.get(pathToSources);
        Path destination = Paths.get(pathToResult, "file.zip");
        Path simpleFileBefore = Paths.get(pathToSources, "d1", "videoplayback.mp4");
        Path simpleFileAfter = Paths.get(pathToResult, "d1", "videoplayback.mp4");

        int filesAfterUnZip = destination.getNameCount();
        //WHEN
        zipCompressor.compress(fileZip, destination);
        zipCompressor.unCompress(destination, Paths.get(pathToResult));
        //THEN
        Assert.assertThat(filesAfterUnZip, Matchers.is(5));
        Assert.assertEquals(simpleFileBefore.toFile().length(), simpleFileAfter.toFile().length());
    }

    @Test
    public void testUnZipFileWithIllegalSourceFormat(){
        //GIVEN
        Path source = Paths.get(pathToSources, "alice.txt");
        Path result = Paths.get(pathToResult);
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("file must be only zip format");
        //WHEN
        zipCompressor.unCompress(source, result);
    }

    @Test
    public void testUnZipFileWithIllegalDestination(){
        //GIVEN
        Path source = Paths.get(pathToSources, "alice.zip");
        Path result = Paths.get(pathToResult, "alice.txt");
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("destination must be only directory");
        //WHEN
        zipCompressor.unCompress(source, result);
    }

    @Test
    public void testUnZipFileWhichNotExist(){
        //GIVEN
        Path source = Paths.get(pathToSources, "alice.zip");
        Path result = Paths.get(pathToResult);
        expectedException.expect(UncheckedIOException.class);
        expectedException.expectMessage("can't find zip file: " + source);
        //WHEN
        zipCompressor.unCompress(source, result);
    }

    @Test
    public void testZipFileWhichFileNotExist(){
        //GIVEN
        expectedException.expect(UncheckedIOException.class);
        Path fileZip = Paths.get(pathToSources, "ssss.txt");
        Path dest = Paths.get(pathToResult, "s.zip");
        expectedException.expect(UncheckedIOException.class);
        expectedException.expectMessage("can't find file: " + fileZip);
        //WHEN
        zipCompressor.compress(fileZip, dest);
    }

    @Test
    public void testZipFileWhenFileWithSameNameIsExistsInDestination(){
        //GIVEN
        Path fileZip = Paths.get(pathToSources, "alice.txt");
        Path dest = Paths.get(pathToResult, "alice.zip");
        Path copyFile = Paths.get(pathToResult, "alice(1).zip");
        //WHEN
        zipCompressor.compress(fileZip, dest);
        zipCompressor.compress(fileZip, dest);
        //THEN
        Assert.assertTrue(Files.exists(dest));
        Assert.assertTrue(Files.exists(copyFile));
        Assert.assertEquals(dest.toFile().length(), copyFile.toFile().length());
        Assert.assertEquals(dest.getParent(), copyFile.getParent());


    }
}
