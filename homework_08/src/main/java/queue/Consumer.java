package queue;

public class Consumer {

    private MyQueue queue;

    public Consumer(MyQueue queue){
        this.queue = queue;
    }

    public String get(){
        return queue.get();
    }
}
