package queue;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class MyQueue {

    private Lock lock;
    private Condition condition;
    private String stuff;
    private boolean setValue;

    public MyQueue(Lock lock){
        this.lock = lock;
        this.condition = lock.newCondition();

    }

    public void put(String value){
        lock.lock();
        while (setValue){
            try{
                condition.await();
            } catch (InterruptedException ex){
                System.out.println("InterruptedException ");
            }
        }
        stuff = value;
        this.setValue = true;
        condition.signal();
        lock.unlock();
    }

    public String  get(){
        lock.lock();
        while (!setValue){
            try {
                condition.await();
            } catch (InterruptedException ex){
                System.out.println("InterruptedException");
            }
        }
        setValue = false;
        condition.signal();
        lock.unlock();
        return stuff;
    }

}
