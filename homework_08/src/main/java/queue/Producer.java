package queue;

public class Producer {

    private MyQueue queue;
    private String stuff = "stuff";
    private long count = 0;

    public Producer(MyQueue queue){
        this.queue = queue;
    }

    public void produce(){
        count++;
        queue.put(stuff + count);
    }
}
