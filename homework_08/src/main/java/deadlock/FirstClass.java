package deadlock;

public class FirstClass {

    private boolean markerForDeadLock = false;

    public FirstClass(){

    }

    public synchronized void lock(SecondClass secondClass){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex){
            System.out.println(Thread.currentThread() + "is break");
        }
        System.out.println("Try to do secondClass.doSmth()");
        secondClass.doSmth();
        markerForDeadLock = true;
    }

    public synchronized void doSmth(){
        System.out.println(Thread.currentThread() + " in " + this.getClass() + " method: doSmth");
    }

    public boolean isMarkerForDeadLock() {
        return markerForDeadLock;
    }
}