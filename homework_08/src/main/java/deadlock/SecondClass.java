package deadlock;

public class SecondClass {

    private boolean markerForDeadLock = false;

    public boolean isMarkerForDeadLock() {
        return markerForDeadLock;
    }

    public synchronized void lock(FirstClass firstClass){
        try{
            Thread.sleep(1000);
        } catch (InterruptedException ex){
            System.out.println(Thread.currentThread() + "is break");
        }
        System.out.println("Try to do thirdClass.doSmth()");
        firstClass.doSmth();
        markerForDeadLock = true;
    }

    public synchronized void doSmth(){
        System.out.println(Thread.currentThread() + " in " + this.getClass() + " method: doSmth");
    }
}
