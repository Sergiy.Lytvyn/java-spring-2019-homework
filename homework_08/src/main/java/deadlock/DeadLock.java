package deadlock;

public class DeadLock {

    private FirstClass firstClass;
    private SecondClass secondClass;
    private Thread firstThread;
    private Thread secondThread;

    public DeadLock(){
        firstClass = new FirstClass();
        secondClass = new SecondClass();
    }

    public void lockIt() {
        firstThread = new Thread(()-> firstClass.lock(secondClass));
        secondThread = new Thread(()-> secondClass.lock(firstClass));
        firstThread.start();
        secondThread.start();
    }

    public Thread getFirstThread() {
        return firstThread;
    }

    public FirstClass getFirstClass() {
        return firstClass;
    }

    public SecondClass getSecondClass() {
        return secondClass;
    }

    public Thread getSecondThread() {
        return secondThread;
    }

}
