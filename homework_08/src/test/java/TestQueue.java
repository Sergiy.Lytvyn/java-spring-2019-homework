import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;
import queue.Consumer;
import queue.MyQueue;
import queue.Producer;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

public class TestQueue {

    private ReentrantLock lock = new ReentrantLock();
    private MyQueue queue = new MyQueue(lock);
    private Consumer consumer = new Consumer(queue);
    private Producer producer = new Producer(queue);

    @Test
    public void testQueueWithLockWhenConsumerSleeps(){
        ArrayList<String> producerStuff = new ArrayList<>();
        ArrayList<String> consumerStuff = new ArrayList<>();
        //WHEN
        Thread producerThread = new Thread(()->{
            for (int i = 0; i < 10; i++){
                producer.produce();
                producerStuff.add("stuff" + (i + 1));
            }
        });

        Thread consumerThread = new Thread(()->{
            for (int i = 0; i < 10; i++){
                String stuff = consumer.get();
                consumerStuff.add(stuff);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    throw new RuntimeException(Thread.currentThread().getName() + " is interrupted");
                }
            }
        });
        producerThread.start();
        consumerThread.start();
        try {
            producerThread.join();
            consumerThread.join();
        } catch (InterruptedException e) {
            throw new RuntimeException("Producer or Consumer are interrupted");
        }
        //THEN
        Assert.assertThat(consumerStuff, Matchers.hasSize(10));
        Assert.assertThat(producerStuff, Matchers.hasSize(10));
        Assert.assertThat(producerStuff, Matchers.equalTo(consumerStuff));
    }

    @Test
    public void testQueueWithLockWhenProducerSleeps(){
        ArrayList<String> producerStuff = new ArrayList<>();
        ArrayList<String> consumerStuff = new ArrayList<>();
        //WHEN
        Thread producerThread = new Thread(()->{
            for (int i = 0; i < 10; i++){
                producer.produce();
                producerStuff.add("stuff" + (i + 1));
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    throw new RuntimeException(Thread.currentThread().getName() + " is interrupted");
                }
            }
        });

        Thread consumerThread = new Thread(()->{
            for (int i = 0; i < 10; i++){
                String stuff = consumer.get();
                consumerStuff.add(stuff);
            }
        });
        producerThread.start();
        consumerThread.start();
        try {
            producerThread.join();
            consumerThread.join();
        } catch (InterruptedException e) {
            throw new RuntimeException("Producer or Consumer are interrupted");
        }
        //THEN
        Assert.assertThat(consumerStuff, Matchers.hasSize(10));
        Assert.assertThat(producerStuff, Matchers.hasSize(10));
        Assert.assertThat(consumerStuff, Matchers.equalTo(producerStuff));
    }
}
