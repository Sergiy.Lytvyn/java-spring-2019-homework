
import deadlock.DeadLock;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Test;

public class TestDeadLock {

    private DeadLock deadLock = new DeadLock();

    @Test
    public void testDeadLock(){
        //GIVEN
        Thread.State firstThreadState;
        Thread.State secondThreadState;
        //WHEN
        deadLock.lockIt();
        try {
            Thread.currentThread().sleep(1500);
        } catch (InterruptedException ex){
            System.out.println("Interrupted handle");
        }
        firstThreadState = deadLock.getFirstThread().getState();
        secondThreadState = deadLock.getSecondThread().getState();

        //THEN
        Assert.assertThat(firstThreadState, Matchers.is(Thread.State.BLOCKED));
        Assert.assertThat(secondThreadState, Matchers.is(Thread.State.BLOCKED));
        Assert.assertFalse(deadLock.getFirstClass().isMarkerForDeadLock());
        Assert.assertFalse(deadLock.getSecondClass().isMarkerForDeadLock());


    }

}
