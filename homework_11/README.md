In this homework was implemented two interfaces: Map and OrderedMap.
First implementation is hash table.
Second implementation is black-red tree, for iterator was create custom stack.
For demonstration was created PhoneBook app that use SortedMap.
For build use : mvn clean install
For run use : java -jar /target/PhoneBook-jar-with-dependencies.jar
App support next fields:

-add FirstName_LastName:PhoneNumber.
Example: -add Serhii_Lytvyn:380684387054
Description: name must be separated with '_' and can has more that one word.
PhoneNumber must has 12 numbers.
Result: if person exists in PhoneBook than phone number will be changed, for another
way person will be added to PhoneBook.

-getPhone FirstName_LastName
Example: -getPhone Serhii_Lytvyn
Description: please, see previous description
Result: if this person exists in PhoneBook than phone number will be printed in cmd, for another
way 'null' will be printed.

-getAll true
Description: this field works only with this parameter
Result: all entries will be printed in cmd in lexicography order by name, for another way
nothing happens.

P.S.
I know that serialization is better way then FileWriter(FileReader),
but I couldn't used serialization, because i must worked
with com.epm.lab.collection.Map.Entry<K,V> class
that doesn't implemented Serialization interface.
Also Map.Entry was final that why I couldn't extend it.
But in file phoneBook.txt always located human readable information.
And finally, sorry for long homework description.