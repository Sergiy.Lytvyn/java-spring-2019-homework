package com.epam.rd.spring2019.homework_11.customHashMap;

import com.epam.rd.spring2019.homework_11.hashMap.CustomHashMap;
import com.epm.lab.collections.Map;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class CustomHashMapTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    private CustomHashMap<String, Integer> map = new CustomHashMap<>();

    private void putSomeEntry(){
        map.put("Dnipro", 500_000);
        map.put("Kiev", 1_200_00);
        map.put("London", 3_000_000);
        map.put("NY", 5_000_000);
    }

    @Test
    public void testConstructorWithIllegalCapacity(){
        //GIVEN
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("illegal capacity: " + (-1));
        //WHEN
        CustomHashMap map = new CustomHashMap(-1);
    }

    @Test
    public void testGet() {
        //WHEN
        putSomeEntry();
        Integer resultDnipro = map.get("Dnipro");
        Integer resultLondon = map.get("London");
        //THEN
        Assert.assertThat(resultLondon, Matchers.is(3_000_000));
        Assert.assertThat(resultDnipro, Matchers.is(500_000));
    }

    @Test
    public void testPut() {
        //WHEN
        putSomeEntry();
        //THEN
        new Map.Entry<>("Dnipro", 500_000);
        new Map.Entry<>("Kiev", 1_200_00);
        new Map.Entry<>("London", 3_000_000);
        new Map.Entry<>("NY", 5_000_000);
        Assert.assertThat(4, Matchers.is(map.size()));
        Assert.assertThat(map, Matchers.containsInAnyOrder(new Map.Entry<>("Dnipro", 500_000), new Map.Entry<>("Kiev", 1_200_00),
            new Map.Entry<>("London", 3_000_000), new Map.Entry<>("NY", 5_000_000)));
    }

    @Test
    public void testPutWhenKeyExist(){
        //WHEN
        putSomeEntry();
        map.put("London", 5000);
        //THEN
        Assert.assertThat(4, Matchers.is(map.size()));
        Assert.assertThat(5000,Matchers.is(map.get("London")));
    }


    @Test
    public void iterator() {
        //GIVEN
        Map.Entry<String, Integer>[] arr = new Map.Entry[4];
        Map.Entry<String, Integer>[] res = new Map.Entry[4];
        Iterator iterator;
        //WHEN
        arr[0] = new Map.Entry<>("Dnipro", 500_000);
        arr[1] = new Map.Entry<>("Kiev", 1_200_00);
        arr[2] = new Map.Entry<>("London", 3_000_000);
        arr[3] = new Map.Entry<>("NY", 5_000_000);
        putSomeEntry();
        iterator = map.iterator();
        for (int i = 0; i < res.length; i++){
            if (iterator.hasNext()){
                res[i] = (Map.Entry<String, Integer>) iterator.next();
            }
        }
        //THEN
        Assert.assertThat(res, Matchers.arrayContainingInAnyOrder(arr));
        Assert.assertFalse(iterator.hasNext());
    }

    @Test
    public void testIteratorWithNoSuchElement(){
        //GIVEN
        expectedException.expect(NoSuchElementException.class);
        Iterator iterator;
        //WHEN
        putSomeEntry();
        iterator = map.iterator();
        for (int i = 0; i < 5; i++){
            iterator.next();
        }
    }
}