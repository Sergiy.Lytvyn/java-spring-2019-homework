package com.epam.rd.spring2019.homework_11.sortedMap;

import com.epm.lab.collections.Map;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.NoSuchElementException;


public class SortedMapTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    private Map<String, Integer> map = new SortedMap<>();

    private void putSomeEntry(){

        map.put("Dnipro", 800_000);
        map.put("Kiev", 1_200_00);
        map.put("London", 3_000_000);
        map.put("NY", 5_000_000);
        map.put("Toronto", 4_000_000);
    }

    @Test
    public void testPut(){
        //WHEN
        putSomeEntry();
        map.put("NY", 3_000_000);
        //THEN
        Assert.assertThat(map.size(), Matchers.is(5));
        Assert.assertThat(map, Matchers.hasItem(new Map.Entry<>("Toronto", 4_000_000)));
        Assert.assertThat(map, Matchers.hasItem(new Map.Entry<>("NY", 3_000_000)));
    }

    @Test
    public void testPutWhenEntryExists(){
        //WHEN
        putSomeEntry();
        map.put("London", 5000);
        //THEN
        Assert.assertThat(5, Matchers.is(map.size()));
        Assert.assertThat(5000,Matchers.is(map.get("London")));
    }

    @Test
    public void testGet(){
        //GIVEN
        Integer sizeBefore;
        //WHEN
        putSomeEntry();
        sizeBefore = map.size();
        //THEN
        Assert.assertThat(4_000_000, Matchers.is(map.get("Toronto")));
        Assert.assertThat(5_000_000, Matchers.is(map.get("NY")));
        Assert.assertNull(map.get("Kharkov"));
        Assert.assertThat(sizeBefore, Matchers.equalTo(map.size()));
    }

    @Test
    public void testIterator(){
        //GIVEN
        ArrayList<Map.Entry<String, Integer>> entriesFromMap = new ArrayList<>();
        ArrayList<Map.Entry<String, Integer>> sourceEntries = new ArrayList<>();
        putSomeEntry();
        Iterator<Map.Entry<String, Integer>> iterator = map.iterator();
        //WHEN
        sourceEntries.add(new Map.Entry<>("Dnipro", 800_000));
        sourceEntries.add(new Map.Entry<>("Kiev", 1_200_00));
        sourceEntries.add(new Map.Entry<>("London", 3_000_000));
        sourceEntries.add(new Map.Entry<>("NY", 5_000_000));
        sourceEntries.add(new Map.Entry<>("Toronto", 4_000_000));
        while (iterator.hasNext()){
            entriesFromMap.add(iterator.next());
        }
        //THEN
        Assert.assertThat(entriesFromMap, Matchers.equalTo(sourceEntries));
        Assert.assertThat(map.size(), Matchers.is(5));
    }

    @Test
    public void testIteratorWithNoSuchException(){
        //GIVEN
        expectedException.expect(NoSuchElementException.class);
        Iterator iterator;
        //WHEN
        putSomeEntry();
        iterator = map.iterator();
        while (iterator.hasNext()){
            iterator.next();
        }
        iterator.next();
    }

    @Ignore
    @Test
    public void testForFail(){
        map = null;
        map.size();
    }
}