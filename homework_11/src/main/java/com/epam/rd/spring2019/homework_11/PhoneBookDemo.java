package com.epam.rd.spring2019.homework_11;

import com.epam.rd.spring2019.homework_11.cmdParser.CommandLineParser;
import com.epam.rd.spring2019.homework_11.sortedMap.SortedMap;
import com.epm.lab.collections.Map;

import java.io.FileWriter;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneBookDemo {

    private final static Path pathToPhoneBook = Paths.get("phoneBook.txt");
    private final static Pattern pattern = Pattern.compile("(\\w+):(\\d{12})");

    private static SortedMap<String, Long> getPhoneBook(){
        SortedMap<String, Long> phoneBook = new SortedMap<>();
        try {
            Files.lines(pathToPhoneBook).map(pattern::matcher).
                    filter(Matcher::matches).
                    forEach(x -> phoneBook.put(x.group(1), Long.valueOf(x.group(2))));
            return phoneBook;
        } catch (IOException e) {
            return phoneBook;
        }
    }

    private static void savePhoneBook(SortedMap<String, Long> phoneBook){
        try (FileWriter fileWriter = new FileWriter(pathToPhoneBook.toFile())){
            for(Map.Entry<String, Long> entry : phoneBook){
                String phoneBookEntry = entry.key + ":" + entry.value;
                fileWriter.write(phoneBookEntry + System.lineSeparator());
            }
        } catch (IOException ex){
            throw new UncheckedIOException(ex.getMessage(), ex);
        }
    }

    public static void main(String[] args) {
        System.out.println("PhoneBook v1.0 on");
        try {
            CommandLineParser commandLineParser = new CommandLineParser(args);
            Map.Entry<String, Long> newEntry = commandLineParser.getEntry();
            Boolean getAll = commandLineParser.getMarkerAll();
            String name = commandLineParser.getName();
            SortedMap<String, Long> phoneBook = getPhoneBook();
            if (newEntry != null) {
                phoneBook.put(newEntry.key, newEntry.value);
                System.out.println("entry was added");
            }
            if ((getAll != null) && (getAll)) {
                phoneBook.forEach(x-> System.out.println(x.key + ":" + x.value));
            }
            if (name != null) {
                System.out.println(phoneBook.get(name));
            }
            savePhoneBook(phoneBook);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
        System.out.println("PhoneBook v1.0 off");
    }

}
