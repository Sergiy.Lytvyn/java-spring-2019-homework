package com.epam.rd.spring2019.homework_11.sortedMap;


import com.epam.rd.spring2019.homework_11.sortedMap.stack.Stack;
import com.epam.rd.spring2019.homework_11.sortedMap.stack.StackImpl;
import com.epm.lab.collections.OrderedMap;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class SortedMap<K extends Comparable<K>, V> implements  OrderedMap<K,V> {

    private Node<K,V> root;
    private int size;
    private boolean addWhenExists;

    public SortedMap(){
    }

    private boolean isRed(Node node){ return (node != null) && (node.color == NodeColor.RED); }

    private Node<K,V> putNode(Node<K,V> node, Entry<K,V> entry){
        if (node == null){
            return new Node<>(entry, NodeColor.RED);
        }
        int compare = entry.key.compareTo(node.entry.key);
        if (compare < 0){
            node.left = putNode(node.left, entry);
        } else if (compare > 0){
            node.right = putNode(node.right, entry);
        } else {
            node.entry.value = entry.value;
            addWhenExists = true;
        }
        if ((isRed(node.right)) && (!isRed(node.left))){
            node = rotateLeft(node);
        }
        if ((isRed(node.left)) && (isRed(node.left.left))){
            node = rotateRight(node);
        }
        if ((isRed(node.left)) && (isRed(node.right))){
            flipColor(node);
        }
        return node;
    }

    private Node<K,V> rotateRight(Node<K,V> node){
        Node<K,V> leftNode = node.left;
        node.left = leftNode.right;
        leftNode.right = node;
        leftNode.color = leftNode.left.color;
        leftNode.right.color = NodeColor.RED;
        return leftNode;
    }

    private Node<K,V> rotateLeft(Node<K,V> node){
        Node<K,V> rightNode = node.right;
        node.right = rightNode.left;
        rightNode.left = node;
        rightNode.color = rightNode.left.color;
        rightNode.left.color = NodeColor.RED;
        return rightNode;
    }

    private void flipColor(Node<K,V> node){
        node.color = node.color.getOtherColor(node.color);
        node.left.color = node.left.color.getOtherColor(node.left.color);
        node.right.color = node.right.color.getOtherColor(node.right.color);
    }

    private Node<K,V> getNode(Node<K,V> node, K key){
        if (node == null){
            return null;
        }
        K nodeKey = node.entry.key;
        int compare = key.compareTo(nodeKey);
        if (compare < 0){
            node = getNode(node.left, key);
        }
        if (compare > 0){
            node = getNode(node.right, key);
        }
        return node;
    }

    @Override
    public V get(K k) {
        Node<K,V> node = getNode(root, k);
        if (node == null){
            return null;
        } else {
        return node.entry.value;
        }
    }

    @Override
    public void put(K k, V v) {
        Entry<K,V> entry = new Entry<>(k, v);
        root = putNode(root, entry);
        root.color = NodeColor.BLACK;
        if (addWhenExists){
            addWhenExists = false;
            return;
        }
        size++;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<Entry<K, V>> iterator() {
        return new SortedMapIterator(root);
    }

    private static class Node<K,V>{

        private Entry<K,V> entry;
        private NodeColor color;
        private Node<K,V> left;
        private Node<K,V> right;

        Node(Entry<K, V> entry, NodeColor color) {
            this.entry = entry;
            this.color = color;
        }

    }

    private enum NodeColor{

        RED(), BLACK();

        NodeColor getOtherColor(NodeColor color){
            return color == RED ? BLACK : RED;
        }

    }

    private class SortedMapIterator implements Iterator<Entry<K,V>>{

        private Stack<Node<K,V>> stack;

        SortedMapIterator(Node<K,V> root) {
            stack = new StackImpl<>();
            pushLeftChild(root);
        }

        private void pushLeftChild(Node<K,V> node){
            while (node != null){
                stack.push(node);
                node= node.left;
            }
        }

        @Override
        public boolean hasNext() {
            return !stack.isEmpty();
        }

        @Override
        public Entry<K, V> next() {
            if (!hasNext()){
                throw new NoSuchElementException();
            }
            Node<K,V> node = stack.pop();
            pushLeftChild(node.right);
            return node.entry;
        }
    }

}