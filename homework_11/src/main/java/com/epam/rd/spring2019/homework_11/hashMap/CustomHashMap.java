package com.epam.rd.spring2019.homework_11.hashMap;

import com.epm.lab.collections.Map;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

public class CustomHashMap<K,V> implements Map<K,V>{

    private Node<K,V>[] table;
    private int size;

    public CustomHashMap(int capacity){
        if (capacity <= 0){
            throw new IllegalArgumentException("illegal capacity: " + capacity);
        }
        table = new Node[capacity];
    }

    public CustomHashMap(){
        this(16);
    }

    private int getNodePosition(Node node){
        int hash = node.hash;
        return hash < 0 ? (-1) * hash % table.length : hash % table.length;
    }

    private Node<K, V> getNode(K key) {
        int hash = Objects.hashCode(key);
        for (Node<K, V> node : table) {
            for (Node<K, V> result = node; result != null; result = result.next) {
                if (result.hash == hash) {
                    return result;
                }
            }
        }
        return null;
    }

    @Override
    public V get(K key) {
        Node<K,V> node = getNode(key);
       return node == null ? null : node.entry.value;
    }

    @Override
    public void put(K key, V value) {
        Entry<K, V> newEntry = new Entry<>(key, value);
        Node<K,V> newNode = new Node<>(newEntry, null);
        Node<K,V> node = getNode(key);
        int position = getNodePosition(newNode);
        if (table[position] == null){
            table[position] = newNode;
            size++;
        } else if (node != null){
            node.entry = newEntry;
        } else {
            node = table[position];
            while (node.next != null){
                node = node.next;
            }
            node.next = newNode;
            size++;
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<Entry<K, V>> iterator() {
        return new MapIterator();
    }

    private static class Node<K, V> {

        final int hash;
        private Entry<K,V> entry;
        private Node<K,V> next;

        Node(Entry<K, V> entry, Node<K, V> next) {
            this.entry = entry;
            this.next = next;
            hash = Objects.hashCode(entry.key);
        }
    }

    private class MapIterator implements Iterator<Entry<K, V>>{

        private Entry<K,V>[] entries;
        private int count;

        MapIterator() {
            entries = new Entry[(size + 1)];
            setEntries();
        }

        private void setEntries(){
            for(Node<K,V> node : table){
                for(Node<K,V> nextNode = node; nextNode != null; nextNode = nextNode.next){
                    entries[count++] = nextNode.entry;
                }
            }
            count = 0;
        }

        @Override
        public boolean hasNext() {
            return entries[count] != null;
        }

        @Override
        public Entry<K, V> next() {
            Entry<K, V> entry = entries[count++];
            if (entry != null){
                return entry;
            } else {
                throw new NoSuchElementException();
            }
        }
    }
}
