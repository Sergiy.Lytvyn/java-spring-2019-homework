package com.epam.rd.spring2019.homework_11.sortedMap.stack;

public class StackImpl<V> implements Stack<V>{

    private Node<V> top;

    public StackImpl() {
    }

    @Override
    public void push(V value) {
        top = new Node<>(value, top);
    }

    @Override
    public V pop() {
        V oldValue = top.value;
        top = top.next;
        return oldValue;
    }

    @Override
    public boolean isEmpty() {
        return top == null;
    }

    private static class Node<V>{

        private V value;
        private Node next;

        private Node(V value, Node next) {
            this.value = value;
            this.next = next;
        }
    }

}
