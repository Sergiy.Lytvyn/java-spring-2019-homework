package com.epam.rd.spring2019.homework_11.cmdParser;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import com.epm.lab.collections.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandLineParser {

    @Option(name = "-add", usage = "add entry name:phone number")
    private String entry;

    @Option(name = "-getAll", usage = "get all sorted entry")
    private String markerGetAll;

    @Option(name = "-getPhone", usage = "get phone number for this name")
    private String name;

    public CommandLineParser(String...args) {
        CmdLineParser cmdLineParser = new CmdLineParser(this);
        try {
            cmdLineParser.parseArgument(args);
        } catch (CmdLineException e) {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    public Map.Entry<String, Long> getEntry() {
        if (entry != null) {
            Pattern pattern = Pattern.compile("(\\w+):(\\d{12})");
            Matcher matcher = pattern.matcher(entry);
            if (matcher.matches()) {
                String name = matcher.group(1);
                Long phoneNumber = Long.valueOf(matcher.group(2));
                return new Map.Entry<>(name, phoneNumber);
            } else {
                throw new RuntimeException("Incorrect input -add");
            }
        } else {
            return null;
        }

    }

    public Boolean getMarkerAll() {
        return Boolean.valueOf(markerGetAll);
    }

    public String getName() {
        if (name != null) {
            Pattern pattern = Pattern.compile("\\w+");
            Matcher matcher = pattern.matcher(name);
            if (matcher.matches()) {
                return matcher.group();
            } else {
                throw new RuntimeException("Incorrect input -getPhone");
            }
        } else {
            return null;
        }
    }

}
