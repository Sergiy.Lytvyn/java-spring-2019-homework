package com.epam.rd.spring2019.homework_11.sortedMap.stack;

public interface Stack<V>{

    void push(V value);

    V pop();

    boolean isEmpty();

}
