import logParser.LogParser;
import logParser.LogParserImpl;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;

public class TestLogParser {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    private ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    private LogParser logParser = new LogParserImpl();

    private PrintStream setConsoleCaptureAndGetOldStream(){
        PrintStream printStream = new PrintStream(byteArrayOutputStream);
        PrintStream old = System.out;
        System.setOut(printStream);
        return old;
    }

    private String getConsolePrint(PrintStream prevStream){
        System.out.flush();
        System.setOut(prevStream);
        return byteArrayOutputStream.toString().trim();
    }

    private String getExcept(Path path){
        String except;
        try (FileReader fileReader = new FileReader(path.toFile());
             BufferedReader bufferedReader = new BufferedReader(fileReader)){
            except = bufferedReader.lines().collect(Collectors.joining(System.lineSeparator()));
        } catch (IOException e) {
            throw new RuntimeException();
        }
        return except;
    }

    @Test
    public void testSetLogs(){
        //GIVEN
        Path source = Paths.get("src/test/resources/logs.txt");
        Path ex = Paths.get("src/test/resources/topFiveLogs.txt");
        PrintStream ps = setConsoleCaptureAndGetOldStream();
        String result;
        String except = getExcept(ex);
        //WHEN
        logParser.setLogs(source);
        logParser.printResult(5);
        result = getConsolePrint(ps);
        //THEN
        Assert.assertEquals(result, except);
    }

    @Test
    public void testSetLogsWithSourceThatNoExist(){
        //GIVEN
        Path source = Paths.get("src/test/resource/logs");
        expectedException.expect(UncheckedIOException.class);
        expectedException.expectMessage("can't load logs from file " + source);
        //WHEN
        logParser.setLogs(source);
    }

    @Test
    public void testSerializeAndDeserialize(){
        //GIVEN
        Path source = Paths.get("src/test/resources/logs.txt");
        Path saveFile = Paths.get("src/test/resources/save.dat");
        Path excepted = Paths.get("src/test/resources/topFiveLogs.txt");
        PrintStream ps = setConsoleCaptureAndGetOldStream();
        String except = getExcept(excepted);
        String result;
        //WHEN
        logParser.setLogs(source);
        logParser.serialize(saveFile);
        logParser.deserialize(saveFile);
        logParser.printResult(5);
        result = getConsolePrint(ps);
        //THEN
        Assert.assertEquals(result, except);
    }

    @Test
    public void testDeserializeWithFileThatNotExist(){
        //GIVEN
        Path source = Paths.get("src/test/resource/log.txt");
        expectedException.expect(UncheckedIOException.class);
        expectedException.expectMessage("can't deserialize logs from file " + source);
        //WHEN
        logParser.deserialize(source);
    }

}
