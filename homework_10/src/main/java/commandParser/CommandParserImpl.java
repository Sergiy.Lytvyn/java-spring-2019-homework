package commandParser;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public class CommandParserImpl implements CommandParser {

    @Option(name = "-load", usage = "set path to file with data")
    private String pathToSavedFile;

    @Option(name = "-file", usage = "set file with logs")
    private String pathToFile;

    @Option(name="-save", usage = "set path to save")
    private String pathToSaveLocation;

    @Option(name="-view", usage = "set amount of logs")
    private Integer amount;

    public CommandParserImpl(String...args){
        CmdLineParser cmdLineParser = new CmdLineParser(this);
        try {
            cmdLineParser.parseArgument(args);
        } catch (CmdLineException e) {
            throw new RuntimeException(e.getLocalizedMessage());
        }
    }

    private Path validatePath(String path){
        if (Objects.nonNull(path)){
            return Paths.get(path);
        }
        return null;
    }

    public Path getPathToSavedFile() {
        return validatePath(pathToSavedFile);
    }

    public Path getPathToFile() { return validatePath(pathToFile); }

    public Path getPathToSaveLocation() { return validatePath(pathToSaveLocation); }

    public Integer getAmount() {
        if (Objects.nonNull(amount)){
            if (amount < 1){
                throw new IllegalArgumentException("Amount can't be 0 or less");
            }
            return amount;
        }
       return null;
    }

}
