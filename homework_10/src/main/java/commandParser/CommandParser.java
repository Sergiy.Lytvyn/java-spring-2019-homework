package commandParser;

import java.nio.file.Path;

public interface CommandParser {

    Path getPathToSavedFile();

    Path getPathToFile();

    Path getPathToSaveLocation();

    Integer getAmount();

}
