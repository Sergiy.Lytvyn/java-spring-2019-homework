package logParser;

import java.nio.file.Path;

public interface LogParser {

    void serialize(Path toFile);

    void deserialize(Path toData);

    void setLogs(Path toLogs);

    void printResult(int amount);

}
