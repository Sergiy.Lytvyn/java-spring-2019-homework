package logParser;

import pojo.Log;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LogParserImpl implements LogParser, Serializable {

    final static private Pattern pattern = Pattern.compile("(\\d{4}-\\d{2}-\\d{2}\\s\\d{2}:\\d{2}:\\d{2}.\\d{3})" +
            "\\s+\\w+\\s+Module=(\\w+)" +
            "\\s+Operation:\\s+(\\w+\\s*\\w*)" +
            "\\s+Execution time:\\s+(\\d+)\\s+\\w+");

    private Map <String, List<Log>> mapWithLogs;

    public LogParserImpl(){
    }

    private static Log getNewLogIfMatch(String line){
        Matcher matcher = pattern.matcher(line);
        if (matcher.matches()){
            String date = matcher.group(1);
            String module = matcher.group(2);
            String operation = matcher.group(3).trim();
            Long execution = Long.parseLong(matcher.group(4));
            return new Log(module, operation, execution ,date);
        } else {
            throw new RuntimeException("This log didn't match: " + line);
        }
    }

    private Map<String, List<Log>> getMapWithLogs(Stream<Log> stream){
        return stream.sorted(Comparator.comparingLong(Log::getExecuting).reversed())
                .collect(Collectors.groupingBy(Log::getModuleName, Collectors.toList()));
    }

    @Override
    public void serialize(Path toFile){
        try (FileOutputStream fileOutputStream = new FileOutputStream(toFile.toFile());
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
            objectOutputStream.writeObject(mapWithLogs);
        } catch (IOException ex){
            throw new UncheckedIOException("can't serialize logs into file " + toFile, ex);
        }
    }

    @Override
    public void deserialize(Path toData){
        Map<String, List<Log>> map;
        try (FileInputStream filterInputStream = new FileInputStream(toData.toFile());
             ObjectInputStream objectInputStream = new ObjectInputStream(filterInputStream)) {
            map = (Map<String, List<Log>>) objectInputStream.readObject();
            Stream<Log> streamOfLogs = map.values().stream().flatMap(List::stream);
            mapWithLogs = getMapWithLogs(streamOfLogs);
        } catch (ClassNotFoundException ex) {
            throw new RuntimeException("System can't find class that try to load");
        } catch (IOException ex){
            throw new UncheckedIOException("can't deserialize logs from file " + toData, ex);
        }
    }

    @Override
    public void setLogs(Path toLogs){
        Stream<Log> streamOfLogs;
        try {
            streamOfLogs = Files.lines(toLogs).map(LogParserImpl::getNewLogIfMatch);
            mapWithLogs = getMapWithLogs(streamOfLogs);
        } catch (IOException ex) {
            throw new UncheckedIOException("can't load logs from file " + toLogs, ex);
        }
    }

    @Override
    public void printResult(int amount) {
        Set<String> modules = mapWithLogs.keySet();
        for (String module : modules){
            System.out.println(module + " Module:");
            mapWithLogs.get(module).stream().limit(amount).forEach(System.out::println);
        }
    }
}
