import commandParser.CommandParserImpl;
import logParser.LogParser;
import logParser.LogParserImpl;
import commandParser.CommandParser;

import java.nio.file.Path;
import java.util.Objects;

public class DemoLogParser {

    public static void main(String[] args) {
        LogParser logParser = new LogParserImpl();
        try {
            CommandParser setting = new CommandParserImpl(args);
            Path load = setting.getPathToSavedFile();
            Path fileLog = setting.getPathToFile();
            Path save = setting.getPathToSaveLocation();
            Integer amount = setting.getAmount();
            if (Objects.nonNull(load)) {
                logParser.deserialize(load);
            }
            if (Objects.nonNull(fileLog)) {
                logParser.setLogs(fileLog);
            }
            if (Objects.nonNull(save)) {
                logParser.serialize(save);
            }
            if (Objects.nonNull(amount)) {
                logParser.printResult(amount);
            }
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

}
