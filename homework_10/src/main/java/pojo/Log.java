package pojo;

import java.io.Serializable;

public class Log implements Serializable {

    private String moduleName;
    private String operationName;
    private Long executing;
    private String date;

    public Log() {
    }

    public Log(String moduleName, String operationName, Long executing, String date) {
        this.moduleName = moduleName;
        this.operationName = operationName;
        this.executing = executing;
        this.date = date;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getOperationName() {
        return operationName;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }

    public Long getExecuting() {
        return executing;
    }

    public void setExecuting(Long executing) {
        this.executing = executing;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return operationName + " " + executing + " ms " + "finished at " + date;
    }

}
