package com.epam.rd.spring2019.util;

public interface CustomList<E> extends CustomCollection<E> {

    public E get(int index);

    public void set(int index, E e);

    public void add(int index, E e);

    public E remove(int index);

    public int indexOf(E e);

}
