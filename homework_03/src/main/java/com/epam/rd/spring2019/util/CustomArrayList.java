package com.epam.rd.spring2019.util;

public class CustomArrayList<E> implements CustomList<E> {

    private static final int DEFAULT_CAPACITY = 10;
    private int size;
    private E[] list;
    private E[] newList;

    public CustomArrayList() {
        list = (E[]) new Object[DEFAULT_CAPACITY];
    }

    public CustomArrayList(int capacity) {
        if (capacity <= 0) {
            throw new IllegalArgumentException();
        } else {
            list = (E[]) new Object[capacity];
        }
    }

    private void checkSize(int size) {
        if (size >= list.length) {
            newList = (E[]) new Object[(int) Math.round((double)list.length * 3 / 2)];
            System.arraycopy(list, 0, newList, 0, list.length);
            list = newList;
        }
    }

    private void checkIndex(int index){
        if (index < 0 || index >= size()){
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public E get(int index) {
        checkIndex(index);
        return list[index];
    }

    @Override
    public void set(int index, E e) {
        checkIndex(index);
        list[index] = e;
    }

    @Override
    public void add(int index, E e) {
        checkIndex(index);
        checkSize(size);
        if (index == size){
            add(e);
        }
        System.arraycopy(list, index, list , index + 1, size - index );
        list[index] = e;
        size++;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean add(E e) {
        checkSize(size);
        list[size++] = e;
        return true;
    }

    @Override
    public boolean remove(E e){
        int i = indexOf(e);
        if (i != -1){
            remove(i);
            return true;
        }
        return false;
    }

    @Override
    public E remove(int index) {
        E oldElement;
        checkIndex(index);
        System.arraycopy(list, (index + 1), list, index, (size - 1) - index);
        oldElement = list[--size];
        list[size] = null;
        return oldElement;
    }

    @Override
    public int indexOf(E e) {
        if(e == null) {
            for (int i = 0; i < size; i++){
                if (list[i] == null){
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (e.equals(list[i])) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public boolean contains(E e) {
        return indexOf(e) >= 0;
    }

    @Override
    public boolean isEmpty(){
        return size == 0;
    }

}
