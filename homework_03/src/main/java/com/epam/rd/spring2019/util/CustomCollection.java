package com.epam.rd.spring2019.util;

public interface CustomCollection<E> {

    public int size();

    public boolean add(E e);

    public boolean remove(E c);

    public boolean contains(E e);

    public boolean isEmpty();

}

