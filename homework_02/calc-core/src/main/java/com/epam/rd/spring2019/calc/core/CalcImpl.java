package com.epam.rd.spring2019.calc.core;

import com.epam.ed.spring2019.calc.interfaces.Calc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CalcImpl implements Calc {

    public static final Logger logger = LoggerFactory.getLogger(CalcImpl.class);


    public double addition(double a, double b) {
        logger.info("addition");
        return a + b;
    }

    public double subtraction(double a, double b) {
        logger.info("subtraction");
        return a - b;
    }

    public double multiplication(double a, double b) {
        logger.info("multiplication");
        return a * b;
    }

    public double division(double a, double b) {
        logger.info("division");
        return a / b;
    }

    public String calculate(double a, double b, char operator) {

        StringBuilder mess = new StringBuilder();
        double res;

        switch (operator) {
            case '+': {
                res = addition(a, b);
                break;
            }
            case '-': {
                res = subtraction(a, b);
                break;
            }
            case 'x': {
                res = multiplication(a, b);
                break;
            }
            case '/': {
                if (b==0){
                    throw new ArithmeticException();
                }
                res = division(a, b);
                break;
            }
            default: {
                logger.warn("Operator is incorrect");
                return mess.append("Operator is incorrect").toString();
            }

        }
        return mess.append("number1=" + a + " number2=" + b
                + " operator=" + operator + " result= " + res).toString();
    }
}
