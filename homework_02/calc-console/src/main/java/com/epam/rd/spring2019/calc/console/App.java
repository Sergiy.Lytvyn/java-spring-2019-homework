package com.epam.rd.spring2019.calc.console;

import com.epam.rd.spring2019.calc.core.CalcImpl;
import java.util.InputMismatchException;

public class App {
    public static void main(String[] args) {

        CalcImpl.logger.info("Calculator v0.1 on");

        CalcImpl calculator = new CalcImpl();
        double a;
        double b;
        char operator;

        if(args.length==3) {
            try {
                a = Double.valueOf(args[0]);
                b = Double.valueOf(args[1]);
                operator = args[2].charAt(0);
                System.out.println(calculator.calculate(a, b, operator));
            } catch (ArithmeticException exc) {
                CalcImpl.logger.warn("User divides on 0");
                System.out.println("You can't divide on 0");
            } catch (InputMismatchException exc) {
                CalcImpl.logger.error("User has inputed not number");
                System.out.println("You have inputed not number");
            }finally {
                CalcImpl.logger.info("Calculator off");
            }
        } else {
            CalcImpl.logger.warn("incorrect input");
            System.out.println("incorrect input");
            CalcImpl.logger.info("Calculator off");
        }
    }
}
