package com.DiFramework.graph;

import com.DiFramework.pojo.BeanDefinition;

import java.util.List;

public interface GraphSortedStrategy {

    List<BeanDefinition> sort(List<BeanDefinition> beanDefinitionList);

}
