package com.DiFramework.graph;

import com.DiFramework.exception.GraphException;
import com.DiFramework.pojo.BeanDefinition;
import com.DiFramework.pojo.BeanParameter;

import java.util.*;
import java.util.stream.Collectors;

public class RecursiveSortStrategy implements GraphSortedStrategy {

    private List<BeanDefinition> visitedDependencies = new ArrayList<>();

    private void analiseDependencies(BeanDefinition beanDefinition, List<BeanDefinition> source, List<BeanDefinition> result) {
        List<String> dependenciesId = beanDefinition.getBeanFields().values().stream().filter(BeanParameter::isRef).map(BeanParameter::getValue).collect(Collectors.toList());
        List<BeanDefinition> dependencies = source.stream().filter(bean -> dependenciesId.contains(bean.getId())).collect(Collectors.toList());
        if (dependencies.size() != dependenciesId.size()) {
            throw new GraphException("missing dependency in bean " + beanDefinition.getId());
        }
        if (visitedDependencies.contains(beanDefinition)){
            throw new GraphException("cycling dependencies: " + beanDefinition.getId());
        } else {
            visitedDependencies.add(beanDefinition);
        }
        for (BeanDefinition bean : dependencies) {
            analiseDependencies(bean, source, result);
        }
        if (!result.contains(beanDefinition)) {
            result.add(beanDefinition);
        }
        visitedDependencies.remove(beanDefinition);
    }

    @Override
    public List<BeanDefinition> sort(List<BeanDefinition> beanDefinitionList) {
        List<BeanDefinition> result = new ArrayList<>(beanDefinitionList.size());
        for (BeanDefinition bean : beanDefinitionList){
            analiseDependencies(bean, beanDefinitionList, result);
        }
        visitedDependencies.clear();
        return result;
    }
}
