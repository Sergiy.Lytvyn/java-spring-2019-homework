package com.DiFramework.loaders;

import com.DiFramework.pojo.BeanDefinition;
import com.DiFramework.pojo.BeanParameter;
import org.ini4j.Profile;
import org.ini4j.Wini;

import java.io.*;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class IniLoader implements Loader {

    private final Wini wini;

    public IniLoader(String fileName){
        try {
            wini = new Wini(ClassLoader.getSystemResourceAsStream(fileName));
        } catch (FileNotFoundException ex) {
            throw new UncheckedIOException("can not find ini file: " + fileName, ex);
        } catch (IOException ex) {
            throw new UncheckedIOException("can not read ini file", ex);
        }
    }

    private BeanParameter getParameter(String field){
        Pattern pattern = Pattern.compile("(val|ref):(.*)");
        Matcher matcher = pattern.matcher(field);
        if (matcher.find()){
            String type = matcher.group(1);
            String value = matcher.group(2);
            return new BeanParameter(type.toUpperCase(), value);
        } else {
            throw new RuntimeException("You input illegal class field: " + field);
        }
    }

    private BeanDefinition convertSectionToBeanDefinition(Profile.Section section) {
        String sectionName = section.getName();
        String className = sectionName.substring(0, sectionName.indexOf(':'));
        String beanId = sectionName.substring(sectionName.indexOf(':') + 1);
        try {
            Class clazz = Class.forName(className);
            Map<String, BeanParameter> parameterMap = section.entrySet().stream()
                    .collect(Collectors.toMap(Map.Entry::getKey, entry -> getParameter(entry.getValue())));
            return new BeanDefinition(beanId, clazz, parameterMap);
        } catch (ClassNotFoundException ex){
            throw new RuntimeException("can not find class: " + className);
        }
    }

    public List<BeanDefinition> loadBeanDefinition() {
        return wini.values().stream()
                .map(this::convertSectionToBeanDefinition)
                .collect(Collectors.toList());
    }

}
