package com.DiFramework.loaders;

import com.DiFramework.pojo.BeanDefinition;

import java.util.List;

public interface Loader {

    List<BeanDefinition> loadBeanDefinition();

}
