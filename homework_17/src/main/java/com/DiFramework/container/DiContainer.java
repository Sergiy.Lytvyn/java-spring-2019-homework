package com.DiFramework.container;

import com.DiFramework.graph.GraphSortedStrategy;
import com.DiFramework.graph.RecursiveSortStrategy;
import com.DiFramework.loaders.IniLoader;
import com.DiFramework.loaders.Loader;
import com.DiFramework.pojo.BeanDefinition;
import com.DiFramework.pojo.BeanParameter;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;
import java.util.*;

public class DiContainer {

    private Loader beanLoader;
    private GraphSortedStrategy sortedStrategy;
    private Map<String, Object> beanScope;
    private List<BeanDefinition> beanDefinitions;

    public DiContainer(String pathToSetting){
        beanLoader = new IniLoader(pathToSetting);
        sortedStrategy = new RecursiveSortStrategy();
        beanScope = new HashMap<>();
        beanDefinitions = sortedStrategy.sort(beanLoader.loadBeanDefinition());
        createBeans();
    }

    private Object parseParam(String paramType, String value) {
        switch (paramType) {
            case "int":
                return Integer.parseInt(value);
            case "java.lang.String":
                return value;
            case "float":
                return Float.parseFloat(value);
            case "long":
                return Long.parseLong(value);
            case "double":
                return Double.parseDouble(value);
            case "short":
                return Short.parseShort(value);
            default:
                throw new IllegalArgumentException();
        }
    }

    private Constructor getConstructor(Class clazz){
        Constructor[] constructors = clazz.getConstructors();
        if (constructors.length != 1){
            throw new IllegalStateException("Bean class:" + clazz.getName() + " has more or less than one public constructor");
        }
        return constructors[0];
    }

    private Object[] getParams(Constructor constructor, BeanDefinition beanDefinition){
        Parameter[] parameters = constructor.getParameters();
        if(parameters.length != beanDefinition.getBeanFields().values().size()){
            System.out.println(parameters.length);
            System.out.println(beanDefinition.getBeanFields().values().size());
            throw new IllegalStateException("You input illegal count of bean parameters. Check " + beanDefinition.getId() + " bean");
        }
        ArrayList<Object> params = new ArrayList<>(parameters.length);
        Map<String, BeanParameter> beanFields = beanDefinition.getBeanFields();
        for (Parameter parameter : parameters){
            BeanParameter beanParameter = beanFields.get(parameter.getName());
            if (beanParameter == null){
                throw new IllegalStateException("missing field: " + parameter.getName());
            }
            if (beanParameter.isVal()){
                Object par = parseParam(parameter.getType().getTypeName(), beanParameter.getValue());
                params.add(par);
            } else {
                Object par = beanScope.get(beanParameter.getValue());
                params.add(par);
            }
        }
        return params.toArray();
    }

    private void createBeans(){
        for (BeanDefinition beanDefinition : beanDefinitions){
            Class clazz = beanDefinition.getBeanClass();
            Constructor constructor = getConstructor(clazz);
            Object[] params = getParams(constructor, beanDefinition);
            try {
                Object instance = constructor.newInstance(params);
                beanScope.put(beanDefinition.getId(), instance);
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                throw new RuntimeException(e.getMessage());
            }
        }
    }

    public Object getBean(String id){
        Object bean = beanScope.get(id);
        if(bean == null){
            throw new NoSuchElementException("DI container doesn't have bean with this id: " + id);
        }
        return bean;
    }

    public Object getBean(Class<?> clazz){
        Optional<BeanDefinition> beanId = beanDefinitions.stream().filter(bean -> bean.getBeanClass().equals(clazz)).findFirst();
        if (beanId.isPresent()){
            return getBean(beanId.get().getId());
        } else {
            throw new NoSuchElementException("DI container doesn't have bean with this class: " + clazz.getName());
        }
    }

}
