package com.DiFramework.pojo;

import java.util.Map;
import java.util.Objects;

public class BeanDefinition {

    private final String id;
    private final Class beanClass;
    private final Map<String, BeanParameter> beanFields;

    public BeanDefinition(String id, Class beanClass, Map<String, BeanParameter> beanFields) {
        this.id = id;
        this.beanClass = beanClass;
        this.beanFields = beanFields;
    }

    public String getId() {
        return id;
    }

    public Class getBeanClass() {
        return beanClass;
    }

    public Map<String, BeanParameter> getBeanFields() {
        return beanFields;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BeanDefinition that = (BeanDefinition) o;
        return id.equals(that.id) &&
                beanClass.equals(that.beanClass) &&
                beanFields.equals(that.beanFields);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, beanClass, beanFields);
    }

    @Override
    public String toString() {
        return "BeanDefinition{" +
                "id='" + id + '\'' +
                ", beanClass=" + beanClass.getName() +
                ", beanFields=" + beanFields +
                '}';
    }
}
