package com.DiFramework.pojo;

import java.util.Objects;

public class BeanParameter {

    private final ParameterType type;
    private final String value;

    public BeanParameter(String type, String value) {
        this.type = ParameterType.valueOf(type);
        this.value = value;
    }

    public ParameterType getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public boolean isRef(){
        return type == ParameterType.REF;
    }

    public boolean isVal(){
        return type == ParameterType.VAL;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BeanParameter that = (BeanParameter) o;
        return type == that.type &&
                Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, value);
    }

    @Override
    public String toString() {
        return "Parameter{" +
                "type=" + type +
                ", value='" + value + '\'' +
                '}';
    }
}

enum ParameterType{
    REF(), VAL()
}
