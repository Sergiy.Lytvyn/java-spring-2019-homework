package com.DiFramework.graph;

import com.DiFramework.exception.GraphException;
import com.DiFramework.loaders.IniLoader;
import com.DiFramework.loaders.Loader;
import com.DiFramework.pojo.BeanDefinition;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static org.junit.Assert.*;

public class RecursiveSortStrategyTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void sort() {
        //GIVEN
        GraphSortedStrategy graphSortedStrategy = new RecursiveSortStrategy();
        Loader iniLoader = new IniLoader("setting.ini");
        //WHEN
        List<BeanDefinition> beanDefinitions = graphSortedStrategy.sort(iniLoader.loadBeanDefinition());
        //THAN
        Assert.assertThat(beanDefinitions.size(), CoreMatchers.is(5));
        Assert.assertThat(beanDefinitions.get(0).getId(), CoreMatchers.is("x"));
        Assert.assertThat(beanDefinitions.get(1).getId(), CoreMatchers.is("f1"));
        Assert.assertThat(beanDefinitions.get(2).getId(), CoreMatchers.is("f2"));
        Assert.assertThat(beanDefinitions.get(3).getId(), CoreMatchers.is("f3"));
        Assert.assertThat(beanDefinitions.get(4).getId(), CoreMatchers.is("y"));
    }

    @Test
    public void sortWithCyclingDependencies(){
        //GIVEN
        expectedException.expect(GraphException.class);
        expectedException.expectMessage("cycling dependencies: f3");
        GraphSortedStrategy graphSortedStrategy = new RecursiveSortStrategy();
        Loader iniLoader = new IniLoader("settingWithCyclingDependencies.ini");
        //WHEN
        List<BeanDefinition> beanDefinitions = graphSortedStrategy.sort(iniLoader.loadBeanDefinition());
    }

    @Test
    public void sortWithMissingDependency(){
        //GIVEN
        expectedException.expect(GraphException.class);
        expectedException.expectMessage("missing dependency in bean f3");
        GraphSortedStrategy graphSortedStrategy = new RecursiveSortStrategy();
        Loader iniLoader = new IniLoader("settingWithMissingDependency.ini");
        //WHEN
        List<BeanDefinition> beanDefinitions = graphSortedStrategy.sort(iniLoader.loadBeanDefinition());
    }


}