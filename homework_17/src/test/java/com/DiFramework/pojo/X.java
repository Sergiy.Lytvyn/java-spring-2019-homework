package com.DiFramework.pojo;

import java.util.Objects;

public class X {

    private final int id;
    private final String name;


    public X(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        X x = (X) o;
        return id == x.id &&
                Objects.equals(name, x.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
