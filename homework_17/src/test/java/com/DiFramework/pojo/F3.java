package com.DiFramework.pojo;

import java.util.Objects;

public class F3 {

    private final int id;
    private final F2 f2;
    private final X x;

    public F3(int id, F2 f2, X x) {
        this.id = id;
        this.f2 = f2;
        this.x = x;
    }

    public int getId() {
        return id;
    }

    public F2 getF2() {
        return f2;
    }

    public X getX() {
        return x;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        F3 f3 = (F3) o;
        return id == f3.id &&
                Objects.equals(f2, f3.f2) &&
                Objects.equals(x, f3.x);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, f2, x);
    }
}
