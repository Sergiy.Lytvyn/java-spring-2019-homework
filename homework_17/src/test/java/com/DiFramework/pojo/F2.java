package com.DiFramework.pojo;

import java.util.Objects;

public class F2 {

    private final long id;
    private final F1 f1;

    public F2(long id, F1 f1) {
        this.id = id;
        this.f1 = f1;
    }

    public long getId() {
        return id;
    }

    public F1 getF1() {
        return f1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        F2 f2 = (F2) o;
        return id == f2.id &&
                Objects.equals(f1, f2.f1);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, f1);
    }
}
