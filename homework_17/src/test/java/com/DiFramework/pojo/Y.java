package com.DiFramework.pojo;

import java.util.Objects;

public class Y {

    private final F1 f1;
    private final  F2 f2;
    private final F3 f3;

    public Y(F1 f1, F2 f2, F3 f3) {
        this.f1 = f1;
        this.f2 = f2;
        this.f3 = f3;
    }

    public F1 getF1() {
        return f1;
    }

    public F2 getF2() {
        return f2;
    }

    public F3 getF3() {
        return f3;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Y y = (Y) o;
        return Objects.equals(f1, y.f1) &&
                Objects.equals(f2, y.f2) &&
                Objects.equals(f3, y.f3);
    }

    @Override
    public int hashCode() {
        return Objects.hash(f1, f2, f3);
    }
}
