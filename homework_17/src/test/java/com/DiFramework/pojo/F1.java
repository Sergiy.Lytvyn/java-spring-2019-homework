package com.DiFramework.pojo;

import java.util.Objects;

public class F1 {

    private final long id;
    private final X x;

    public F1(long id, X x) {
        this.id = id;
        this.x = x;
    }

    public long getId() {
        return id;
    }

    public X getX() {
        return x;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        F1 f1 = (F1) o;
        return id == f1.id &&
                Objects.equals(x, f1.x);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, x);
    }
}
