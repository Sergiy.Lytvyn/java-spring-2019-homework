package com.DiFramework.container;

import com.DiFramework.pojo.*;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import static org.junit.Assert.*;

public class DiContainerTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void getBeanWithClass() {
        //GIVEN
        DiContainer container = new DiContainer("setting.ini");
        //WHEN
        Y y = (Y) container.getBean(Y.class);
        X x = (X) container.getBean(X.class);
        F1 f1 = (F1) container.getBean(F1.class);
        F2 f2 = (F2) container.getBean(F2.class);
        F3 f3 = (F3) container.getBean(F3.class);
        Y secondY = (Y) container.getBean(Y.class);
        //THAN
        Assert.assertEquals(y.getF1(), f1);
        Assert.assertEquals(y.getF2(), f2);
        Assert.assertEquals(y.getF3(), f3);
        Assert.assertEquals(f3.getX(), x);
        Assert.assertEquals(f3.getId(), 1);
        Assert.assertEquals(f3.getF2(), f2);
        Assert.assertEquals(f2.getF1(), f1);
        Assert.assertEquals(f2.getId(), 1);
        Assert.assertEquals(f1.getId(), 1);
        Assert.assertEquals(f1.getX(), x);
        Assert.assertEquals(y, secondY);
    }

    @Test
    public void getBeanWithBeanId() {
        //GIVEN
        DiContainer container = new DiContainer("setting.ini");
        //WHEN
        Y y = (Y) container.getBean("y");
        X x = (X) container.getBean("x");
        F1 f1 = (F1) container.getBean("f1");
        F2 f2 = (F2) container.getBean("f2");
        F3 f3 = (F3) container.getBean("f3");
        Y secondY = (Y) container.getBean("y");
        //THAN
        Assert.assertEquals(y.getF1(), f1);
        Assert.assertEquals(y.getF2(), f2);
        Assert.assertEquals(y.getF3(), f3);
        Assert.assertEquals(f3.getX(), x);
        Assert.assertEquals(f3.getId(), 1);
        Assert.assertEquals(f3.getF2(), f2);
        Assert.assertEquals(f2.getF1(), f1);
        Assert.assertEquals(f2.getId(), 1);
        Assert.assertEquals(f1.getId(), 1);
        Assert.assertEquals(f1.getX(), x);
        Assert.assertEquals(y, secondY);
    }

    @Test
    public void testGetBeanWithNonExistingBeanId(){
        //GIVEN
        expectedException.expect(NoSuchElementException.class);
        expectedException.expectMessage("DI container doesn't have bean with this id: J");
        DiContainer container = new DiContainer("setting.ini");
        //WHEN
        container.getBean("J");
    }

    @Test
    public void testGetBeanWithNonExistingBeanClass(){
        //GIVEN
        expectedException.expect(NoSuchElementException.class);
        expectedException.expectMessage("DI container doesn't have bean with this class: java.util.ArrayList");
        DiContainer container = new DiContainer("setting.ini");
        //WHEN
        container.getBean(ArrayList.class);
    }

    @Test
    public void testGetBeanWithBadParameter(){
        //GIVEN
        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage("missing field: f2");
        //WHEN
        DiContainer container = new DiContainer("settingWithBadBeanParameter.ini");
    }

    @Test
    public void testGetBeanWithIllegalCountOfParameters(){
        //GIVEN
        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage("You input illegal count of bean parameters. Check f3 bean");
        //WHEN
        DiContainer container = new DiContainer("settingWithIllegalCountOfParameters.ini");
    }

    @Test
    public void testGetBeanWithToPublicConstructor(){
        //GIVEN
        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage("Bean class:com.DiFramework.pojo.TwoPublicConstructor has more or less than one public constructor");
        //WHEN
        DiContainer container = new DiContainer("settingOfBeanWithTwoConstructor.ini");
    }
}