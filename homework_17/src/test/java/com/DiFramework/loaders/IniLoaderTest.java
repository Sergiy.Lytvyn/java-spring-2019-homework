package com.DiFramework.loaders;

import com.DiFramework.pojo.BeanDefinition;
import org.hamcrest.*;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

public class IniLoaderTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testLoadBeanDefinition(){
        //GIVEN
        Loader iniLoader = new IniLoader("setting.ini");
        //WHEN
        List<BeanDefinition> beanDefinitions = iniLoader.loadBeanDefinition();
        //THAN
        Assert.assertThat(beanDefinitions.size(), CoreMatchers.is(5));
    }

    @Test
    public void testLoadBeanDefinitionWithNonExitingClass(){
        //GIVEN
        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage("can not find class: com.DiFramework.pojo.K");
        //WHEN
        Loader iniLoader = new IniLoader("settingWithNonExitingClass.ini");
        List<BeanDefinition> beanDefinitions = iniLoader.loadBeanDefinition();
    }

    @Test
    public void testLoadBeanDefinitionWithIllegalField(){
        //GIVEN
        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage("You input illegal class field: value:Name for x");
        //WHEN
        Loader iniLoader = new IniLoader("settingWithIllegalField.ini");
        List<BeanDefinition> beanDefinitions = iniLoader.loadBeanDefinition();
    }


}