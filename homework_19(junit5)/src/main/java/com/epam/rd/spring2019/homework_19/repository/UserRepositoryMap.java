package com.epam.rd.spring2019.homework_19.repository;

import com.epam.rd.spring2019.homework_19.repository.domain.User;
import com.epam.rd.spring2019.homework_19.exeption.RepositoryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Map;

@Repository
public class UserRepositoryMap implements UserRepository {

    @Autowired
    private Map<String, User> userMap;

    @Override
    public User create(User user) {
        if (userMap.containsKey(user.getId())){
            throw new RepositoryException("created user already exist");
        }
        userMap.put(user.getId(), user);
        return user;
    }

    @Override
    public User updateById(String id, User user) {
        if (userMap.containsKey(id)) {
            userMap.put(id, user);
        } else {
            throw new RepositoryException("You try to update user by non exist id");
        }
        return user;
    }

    @Override
    public String deleteById(String id) {
        if (userMap.remove(id) == null){
            throw new RepositoryException("You try to delete user by non exist id");
        }
        return id;
    }

    @Override
    public Collection<User> getAll() {
        return userMap.values();
    }
}
