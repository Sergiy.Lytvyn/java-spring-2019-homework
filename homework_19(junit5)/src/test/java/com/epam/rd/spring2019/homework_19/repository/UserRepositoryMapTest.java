package com.epam.rd.spring2019.homework_19.repository;

import com.epam.rd.spring2019.homework_19.repository.domain.User;
import com.epam.rd.spring2019.homework_19.exeption.RepositoryException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.springframework.test.util.ReflectionTestUtils;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.*;

class UserRepositoryMapTest {

    private UserRepository sut = new UserRepositoryMap();
    private Map map = mock(Map.class);
    private final User user = User.builder()
            .id("John Wall")
            .email("email")
            .firstName("John")
            .lastName("Wall")
            .registrationDate(LocalDateTime.now())
            .build();

    @BeforeEach
    void init(){
        ReflectionTestUtils.setField(sut, "userMap", map);
    }

    @Test
    void create() {
        //GIVEN
        given(map.containsKey(eq(user.getId()))).willReturn(false);
        given(map.put(eq(user.getId()), eq(user))).willReturn(user);
        //WHEN
        User result = sut.create(user);
        //THEN
        assertEquals(user, result);
    }

    @Test
    void createWithException(){
        //GIVEN
        given(map.containsKey(eq(user.getId()))).willReturn(true);
        //THAN
        assertThrows(RepositoryException.class, () -> sut.create(user));
    }

    @Test
    void updateById() {
        //GIVEN
        given(map.containsKey(eq(user.getId()))).willReturn(true);
        given(map.put(eq(user.getId()), eq(user))).willReturn(user);
        //WHEN
        User result = sut.updateById(user.getId(), user);
        //THAN
        assertEquals(user, result);
    }

    @Test
    void updateByIdWithException(){
        //GIVEN
        given(map.containsKey(eq(user.getId()))).willReturn(false);
        //THAN
        assertThrows(RepositoryException.class, ()->sut.updateById(user.getId(), user));
    }

    @Test
    void deleteById() {
        //GIVEN
        given(map.remove(eq(user.getId()))).willReturn(user.getId());
        //WHEN
        String id = sut.deleteById(user.getId());
        //THAN
        assertEquals(user.getId(), id);
    }

    @Test
    void deleteByIdWithException(){
        //GIVEN
        given(map.remove(eq(user.getId()))).willReturn(null);
        //THAN
        assertThrows(RepositoryException.class, ()->sut.deleteById(user.getId()));
    }

    @Test
    void getAll() {
        //GIVEN
        User secondUser = User.builder()
                .id("James Harden")
                .email("test@email")
                .firstName("James")
                .lastName("Harden")
                .registrationDate(LocalDateTime.now())
                .build();
        given(map.values()).willReturn(Arrays.asList(user, secondUser));
        //WHEN
        Collection<User> result = sut.getAll();
        //THAN
        assertEquals(2, result.size());
        assertTrue(result.contains(user));
        assertTrue(result.contains(secondUser));
    }
}