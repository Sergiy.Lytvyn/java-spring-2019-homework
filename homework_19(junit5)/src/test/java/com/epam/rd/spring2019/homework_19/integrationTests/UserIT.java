package com.epam.rd.spring2019.homework_19.integrationTests;

import com.epam.rd.spring2019.homework_19.integrationTests.config.TestConfig;
import com.epam.rd.spring2019.homework_19.controllers.UserController;
import com.epam.rd.spring2019.homework_19.controllers.dto.UserDTO;
import com.epam.rd.spring2019.homework_19.repository.UserRepository;
import com.epam.rd.spring2019.homework_19.repository.domain.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringJUnitWebConfig(TestConfig.class)
class UserIT {

    @Autowired
    private UserController controller;

    @Autowired
    private UserRepository repository;

    @Autowired
    private ObjectMapper objectMapper;

    private MockMvc mockMvc;

    private LocalDateTime dateTime = LocalDateTime.now();
    private UserDTO dto = UserDTO.builder()
            .id(null)
            .email("it@test.email")
            .firstName("Chris")
            .lastName("Pall")
            .registrationDate(dateTime.toString())
            .build();

    private void compareUserWithDTO(User user, UserDTO dto){
        assertEquals(user.getId(), dto.getId());
        assertEquals(user.getRegistrationDate().toString(), dto.getRegistrationDate());
        assertEquals(user.getEmail(), dto.getEmail());
        assertEquals(user.getFirstName(), dto.getFirstName());
        assertEquals(user.getLastName(), dto.getLastName());

    }

    @BeforeEach
    void init(){
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @DirtiesContext
    @Test
    void create() throws Exception {
        //GIVEN
        dto.setId("Chris Paul");
        //THAN
        mockMvc.perform(MockMvcRequestBuilders.post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(jsonPath("$.id", Matchers.is("Chris Paul")))
                .andExpect(jsonPath("$.email", Matchers.is(dto.getEmail())))
                .andExpect(jsonPath("$.firstName", Matchers.is(dto.getFirstName())))
                .andExpect(jsonPath("$.lastName", Matchers.is(dto.getLastName())))
                .andExpect(jsonPath("$.registrationDate", Matchers.is(dto.getRegistrationDate())));

        User result = repository.getAll().stream()
                .filter(us -> us.getId().equals(dto.getId()))
                .findAny()
                .orElseThrow(RuntimeException::new);

        compareUserWithDTO(result, dto);
    }

    @Test
    void createWhenUserExist() throws Exception {
        //GIVEN
        dto.setId("John Wall");
        //THAN
        mockMvc.perform(MockMvcRequestBuilders.post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.is("created user already exist")));

        assertEquals(2, repository.getAll().size());
    }

    @DirtiesContext
    @Test
    void update() throws Exception {
        //GIVEN
        dto.setId("John Wall");
        //THAN
        mockMvc.perform(MockMvcRequestBuilders.put("/user/" + dto.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.id", Matchers.is("John Wall")))
                .andExpect(jsonPath("$.email", Matchers.is(dto.getEmail())))
                .andExpect(jsonPath("$.firstName", Matchers.is(dto.getFirstName())))
                .andExpect(jsonPath("$.lastName", Matchers.is(dto.getLastName())))
                .andExpect(jsonPath("$.registrationDate", Matchers.is(dto.getRegistrationDate())));

        User result = repository.getAll().stream()
                .filter(us -> us.getId().equals("John Wall"))
                .findAny()
                .orElseThrow(RuntimeException::new);

        compareUserWithDTO(result, dto);

    }

    @Test
    void updateWhenUserNonExist() throws Exception {
        //GIVEN
        dto.setId("Test_id");
        //THAN
        mockMvc.perform(MockMvcRequestBuilders.put("/user/" + dto.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.is("You try to update user by non exist id")));

        Collection<User> users = repository.getAll();
        long count = users.stream().filter(us -> us.getId().equals("Test_id")).count();

        assertEquals(0, count);
        assertEquals(2, users.size());
    }

    @DirtiesContext
    @Test
    void delete() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/user/James Harden"))
                .andExpect(MockMvcResultMatchers.status().isOk());

        Collection<User> users = repository.getAll();
        User user = users.stream().findAny().orElseThrow(RuntimeException::new);

        assertEquals(1, users.size());
        assertEquals("John Wall", user.getId());
        assertEquals("John", user.getFirstName());
        assertEquals("Wall", user.getLastName());
    }

    @Test
    void deleteWhenIdNonExist() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/user/10")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.is("You try to delete user by non exist id")));

        Collection<User> users = repository.getAll();

        assertEquals(2, users.size());
    }

    @Test
    void getAll() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", Matchers.hasSize(2)))
                .andExpect(jsonPath("$[0].id", Matchers.is("John Wall")))
                .andExpect(jsonPath("$[0].firstName", Matchers.is("John")))
                .andExpect(jsonPath("$[0].lastName", Matchers.is("Wall")))
                .andExpect(jsonPath("$[1].id", Matchers.is("James Harden")))
                .andExpect(jsonPath("$[1].firstName", Matchers.is("James")))
                .andExpect(jsonPath("$[1].lastName", Matchers.is("Harden")));

        Collection<User> users = repository.getAll();

        assertEquals(2, users.size());
    }

}
