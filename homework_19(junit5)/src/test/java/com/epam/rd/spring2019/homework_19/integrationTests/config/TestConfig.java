package com.epam.rd.spring2019.homework_19.integrationTests.config;

import com.epam.rd.spring2019.homework_19.controllers.UserController;
import com.epam.rd.spring2019.homework_19.repository.UserRepositoryMap;
import com.epam.rd.spring2019.homework_19.repository.domain.User;
import com.epam.rd.spring2019.homework_19.services.UserServiceImpl;
import com.epam.rd.spring2019.homework_19.services.mapper.UserMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Configuration
@ComponentScan(basePackageClasses = {UserServiceImpl.class, UserController.class, UserMapper.class, UserRepositoryMap.class})
public class TestConfig {

    @Bean
    public Map<String, User> map(){
        Map<String, User> map = new HashMap<>();
        User first = User.builder()
                .id("John Wall")
                .email("email")
                .firstName("John")
                .lastName("Wall")
                .registrationDate(LocalDateTime.now())
                .build();
        User second = User.builder()
                .id("James Harden")
                .email("test@email")
                .firstName("James")
                .lastName("Harden")
                .registrationDate(LocalDateTime.now().plusDays(2))
                .build();
        map.put(first.getId(), first);
        map.put(second.getId(), second);
        return map;
    }

    @Bean
    public ObjectMapper objectMapper(){
        return new ObjectMapper();
    }

}
