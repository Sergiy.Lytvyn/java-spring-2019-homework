package com.epam.rd.spring2019.homework_19.repository;

import com.epam.rd.spring2019.homework_19.repository.domain.User;
import com.epam.rd.spring2019.homework_19.exeption.RepositoryException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.*;

public class UserRepositoryMapTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    private UserRepository sut = new UserRepositoryMap();
    private Map map = mock(Map.class);
    private final User user = User.builder()
            .id("John Wall")
            .email("email")
            .firstName("John")
            .lastName("Wall")
            .registrationDate(LocalDateTime.now())
            .build();

    @Before
    public void init(){
        ReflectionTestUtils.setField(sut, "userMap", map);
    }

    @Test
    public void create() {
        //GIVEN
        given(map.containsKey(eq(user.getId()))).willReturn(false);
        given(map.put(eq(user.getId()), eq(user))).willReturn(user);
        //WHEN
        User result = sut.create(user);
        //THEN
        assertEquals(user, result);
    }

    @Test
    public void createWithException(){
        //GIVEN
        expectedException.expect(RepositoryException.class);
        given(map.containsKey(eq(user.getId()))).willReturn(true);
        //WHEN
        sut.create(user);
    }

    @Test
    public void updateById() {
        //GIVEN
        given(map.containsKey(eq(user.getId()))).willReturn(true);
        given(map.put(eq(user.getId()), eq(user))).willReturn(user);
        //WHEN
        User result = sut.updateById(user.getId(), user);
        //THAN
        assertEquals(user, result);
    }

    @Test
    public void updateByIdWithException(){
        //GIVEN
        expectedException.expect(RepositoryException.class);
        given(map.containsKey(eq(user.getId()))).willReturn(false);
        //WHEN
        sut.updateById(user.getId(), user);
    }

    @Test
    public void deleteById() {
        //GIVEN
        given(map.remove(eq(user.getId()))).willReturn(user.getId());
        //WHEN
        String id = sut.deleteById(user.getId());
        //THAN
        assertEquals(user.getId(), id);
    }

    @Test
    public void deleteByIdWithException(){
        //GIVEN
        expectedException.expect(RepositoryException.class);
        given(map.remove(eq(user.getId()))).willReturn(null);
        //WHEN
        sut.deleteById(user.getId());
    }

    @Test
    public void getAll() {
        //GIVEN
        User secondUser = User.builder()
                .id("James Harden")
                .email("test@email")
                .firstName("James")
                .lastName("Harden")
                .registrationDate(LocalDateTime.now())
                .build();
        given(map.values()).willReturn(Arrays.asList(user, secondUser));
        //WHEN
        Collection<User> result = sut.getAll();
        //THAN
        assertEquals(2, result.size());
        assertTrue(result.contains(user));
        assertTrue(result.contains(secondUser));
    }
}