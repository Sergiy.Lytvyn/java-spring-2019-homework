package com.epam.rd.spring2019.homework_19.services;

import com.epam.rd.spring2019.homework_19.repository.domain.User;
import com.epam.rd.spring2019.homework_19.controllers.dto.UserDTO;
import com.epam.rd.spring2019.homework_19.services.mapper.UserMapper;
import com.epam.rd.spring2019.homework_19.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.*;

public class UserServiceImplTest {

    private UserRepository repository = Mockito.mock(UserRepository.class);
    private UserMapper mapper = Mockito.mock(UserMapper.class);
    private UserService sut = new UserServiceImpl();

    private final LocalDateTime dateTime = LocalDateTime.now();
    private final User user = User.builder()
            .id("John Wall")
            .email("email")
            .firstName("John")
            .lastName("Wall")
            .registrationDate(dateTime)
            .build();

    private final UserDTO dto = UserDTO.builder()
            .id("John Wall")
            .email("email")
            .firstName("John")
            .lastName("Wall")
            .registrationDate(dateTime.toString())
            .build();

    @Before
    public void init(){
     ReflectionTestUtils.setField(sut, "repository", repository);
     ReflectionTestUtils.setField(sut, "mapper", mapper);
     given(mapper.toUserDTO(Mockito.eq(user))).willReturn(dto);
     given(mapper.toUser(Mockito.eq(dto))).willReturn(user);
    }

    @Test
    public void create() {
     //GIVEN
     given(repository.create(Mockito.eq(user))).willReturn(user);
     //WHEN
     UserDTO resultDTO = sut.create(dto);
     //THEN
     assertEquals(dto.getId(), resultDTO.getId());
     assertEquals(dto.getEmail(), resultDTO.getEmail());
     assertEquals(dto.getFirstName(), resultDTO.getFirstName());
     assertEquals(dto.getLastName(), resultDTO.getLastName());
     assertEquals(dto.getRegistrationDate(), resultDTO.getRegistrationDate());
     Mockito.verify(repository, Mockito.times(1)).create(user);
    }

    @Test
    public void updateById() {
        //GIVEN
        given(repository.updateById(Mockito.eq("John Wall"), Mockito.eq(user))).willReturn(user);
        //WHEN
        UserDTO resultDTO = sut.updateById("John Wall", dto);
        //THEN
        assertEquals(dto.getId(), resultDTO.getId());
        assertEquals(dto.getEmail(), resultDTO.getEmail());
        assertEquals(dto.getFirstName(), resultDTO.getFirstName());
        assertEquals(dto.getLastName(), resultDTO.getLastName());
        assertEquals(dto.getRegistrationDate(), resultDTO.getRegistrationDate());
        Mockito.verify(repository, Mockito.times(1)).updateById("John Wall", user);
    }

    @Test
    public void deleteById() {
        //GIVEN
        given(repository.deleteById(Mockito.eq("John Wall"))).willReturn("John Wall");
        //WHEN
        String id = sut.deleteById("John Wall");
        //THEN
        assertEquals("John Wall", id);
        Mockito.verify(repository, Mockito.times(1)).deleteById("John Wall");
    }

    @Test
    public void getAll() {
        //GIVEN
        User secondUser = User.builder()
                .id("James Harden")
                .email("test@email")
                .firstName("James")
                .lastName("Harden")
                .registrationDate(dateTime)
                .build();
        UserDTO secondDTO = UserDTO.builder()
                .id("James Harden")
                .email("test@email")
                .firstName("James")
                .lastName("Harden")
                .registrationDate(dateTime.toString())
                .build();

        given(repository.getAll()).willReturn(Arrays.asList(user, secondUser));
        given(mapper.toUserDTO(secondUser)).willReturn(secondDTO);
        //WHEN
        Collection<UserDTO> users = sut.getAll();
        //THEN
        assertTrue(users.contains(dto));
        assertTrue(users.contains(secondDTO));
        assertEquals(2, users.size());
        Mockito.verify(repository, Mockito.times(1)).getAll();
    }
}