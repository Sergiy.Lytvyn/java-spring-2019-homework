package com.epam.rd.spring2019.homework_19.exeption;

public class RepositoryException extends RuntimeException {

    public RepositoryException(String message) {
        super(message);
    }

}
