package com.epam.rd.spring2019.homework_19.config;

import com.epam.rd.spring2019.homework_19.repository.domain.User;
import com.epam.rd.spring2019.homework_19.services.mapper.UserMapper;
import com.epam.rd.spring2019.homework_19.repository.UserRepositoryMap;
import com.epam.rd.spring2019.homework_19.services.UserServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
@ComponentScan(basePackageClasses = {UserRepositoryMap.class, UserServiceImpl.class, UserMapper.class})
public class RootConfig {

    @Bean
    Map<String, User> map(){
        return new HashMap<>();
    }

}
