package com.epam.rd.spring2019.homework_19.controllers.dto;

import lombok.*;

import java.io.Serializable;

@Builder
@EqualsAndHashCode
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO implements Serializable {

    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private String registrationDate;

}
