package com.epam.rd.spring2019.homework_19.services.mapper;

import com.epam.rd.spring2019.homework_19.repository.domain.User;
import com.epam.rd.spring2019.homework_19.controllers.dto.UserDTO;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class UserMapper {

    public User toUser(UserDTO dto){
        return User.builder()
                .id(dto.getId())
                .email(dto.getEmail())
                .firstName(dto.getFirstName())
                .lastName(dto.getLastName())
                .registrationDate(LocalDateTime.parse(dto.getRegistrationDate()))
                .build();
    }

    public UserDTO toUserDTO(User user){
        return UserDTO.builder()
                .id(user.getId())
                .email(user.getEmail())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .registrationDate(user.getRegistrationDate().toString())
                .build();
    }

}
