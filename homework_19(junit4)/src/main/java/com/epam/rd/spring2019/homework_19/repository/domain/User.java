package com.epam.rd.spring2019.homework_19.repository.domain;

import lombok.*;

import java.time.LocalDateTime;

@Builder
@EqualsAndHashCode
@Setter
@Getter
public class User {

    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private LocalDateTime registrationDate;

}


