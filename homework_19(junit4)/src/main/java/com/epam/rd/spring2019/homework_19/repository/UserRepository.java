package com.epam.rd.spring2019.homework_19.repository;

import com.epam.rd.spring2019.homework_19.repository.domain.User;

import java.util.Collection;

public interface UserRepository {

    User create(User user);

    User updateById(String id, User user);

    String deleteById(String id);

    Collection<User> getAll();

}
