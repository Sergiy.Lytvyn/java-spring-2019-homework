package com.epam.rd.spring2019.homework_19.services;

import com.epam.rd.spring2019.homework_19.repository.domain.User;
import com.epam.rd.spring2019.homework_19.controllers.dto.UserDTO;
import com.epam.rd.spring2019.homework_19.services.mapper.UserMapper;
import com.epam.rd.spring2019.homework_19.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repository;

    @Autowired
    private UserMapper mapper;

    @Override
    public UserDTO create(UserDTO dto) {
        User user = repository.create(mapper.toUser(dto));
        return mapper.toUserDTO(user);
    }

    @Override
    public UserDTO updateById(String id, UserDTO dto) {
        User user = repository.updateById(id, mapper.toUser(dto));
        return mapper.toUserDTO(user);
    }

    @Override
    public String deleteById(String id) {
        return repository.deleteById(id);
    }

    @Override
    public Collection<UserDTO> getAll() {
        return repository.getAll().stream().map(mapper::toUserDTO).collect(Collectors.toList());
    }
}
