package com.epam.spring2019.autoBase.web.controllers;

import com.epam.spring2019.autoBase.repository.domain.UserRole;
import com.epam.spring2019.autoBase.services.api.UserService;
import com.epam.spring2019.autoBase.web.dto.user.UserCreateDTO;
import com.epam.spring2019.autoBase.web.dto.user.UserDTO;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Secured({"ROLE_USER","ROLE_ADMIN", "ROLE_SUPER_ADMIN"})
    @GetMapping("profile")
    public String profile(Principal principal, Model model){
        UserDTO userDTO = userService.getByEmail(principal.getName());
        model.addAttribute("user", userDTO);
        if (UserRole.isAdmin(userDTO.getRole())){
            return "admin/profile";
        } else if (UserRole.isSuperAdmin(userDTO.getRole())){
            return "superAdmin/profile";
        } else {
            return "user/profile";
        }
    }

    @Secured("ROLE_SUPER_ADMIN")
    @GetMapping("all")
    public String all(Model model){
        model.addAttribute("users", userService.getAll());
        return "user/all";
    }

    @Secured("ROLE_SUPER_ADMIN")
    @PostMapping
    public String create(UserCreateDTO dto, Model model){
        userService.create(dto);
        return all(model);
    }

}
