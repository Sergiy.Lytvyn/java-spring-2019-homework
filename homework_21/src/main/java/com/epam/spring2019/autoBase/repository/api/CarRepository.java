package com.epam.spring2019.autoBase.repository.api;

import com.epam.spring2019.autoBase.repository.domain.Car;

import java.util.Collection;
import java.util.UUID;

public interface CarRepository {

    Collection<Car> getAll();

    Car updateById(UUID id, Car updated);

    Car create(Car car);

    UUID deleteById(UUID id);
}
