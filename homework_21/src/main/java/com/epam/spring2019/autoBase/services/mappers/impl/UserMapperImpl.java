package com.epam.spring2019.autoBase.services.mappers.impl;

import com.epam.spring2019.autoBase.repository.domain.User;
import com.epam.spring2019.autoBase.repository.domain.UserRole;
import com.epam.spring2019.autoBase.services.mappers.api.UserMapper;
import com.epam.spring2019.autoBase.web.dto.user.UserCreateDTO;
import com.epam.spring2019.autoBase.web.dto.user.UserDTO;
import lombok.NoArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class UserMapperImpl implements UserMapper {

    private final PasswordEncoder passwordEncoder;

    public UserMapperImpl(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDTO toDTO(User user){
        return UserDTO.builder()
                .id(user.getId().toString())
                .name(user.getName())
                .email(user.getEmail())
                .role(user.getRole().name())
                .build();
    }

    @Override
    public UserDetails toUserDetails(User user){
        return org.springframework.security.core.userdetails.User.builder()
                .username(user.getEmail())
                .password(user.getPassword())
                .roles(user.getRole().name())
                .build();
    }

    @Override
    public User toUser(UserCreateDTO dto) {
        return User.builder()
                .id(UUID.randomUUID())
                .password(passwordEncoder.encode(dto.getPassword()))
                .role(UserRole.valueOf(dto.getRole()))
                .name(dto.getName())
                .email(dto.getEmail())
                .build();
    }


}
