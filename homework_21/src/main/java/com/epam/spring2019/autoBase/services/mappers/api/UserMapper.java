package com.epam.spring2019.autoBase.services.mappers.api;

import com.epam.spring2019.autoBase.repository.domain.User;
import com.epam.spring2019.autoBase.web.dto.user.UserCreateDTO;
import com.epam.spring2019.autoBase.web.dto.user.UserDTO;
import org.springframework.security.core.userdetails.UserDetails;

public interface UserMapper {
    UserDTO toDTO(User user);
    UserDetails toUserDetails(User user);
    User toUser(UserCreateDTO dto);
}
