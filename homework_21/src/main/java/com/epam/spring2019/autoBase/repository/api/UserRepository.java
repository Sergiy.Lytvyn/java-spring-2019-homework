package com.epam.spring2019.autoBase.repository.api;

import com.epam.spring2019.autoBase.repository.domain.User;

import java.util.Collection;

public interface UserRepository {

    User getByEmail(String email);

    Collection<User> getAll();

    User create(User user);

}
