package com.epam.spring2019.autoBase.services.api;

import com.epam.spring2019.autoBase.web.dto.user.UserCreateDTO;
import com.epam.spring2019.autoBase.web.dto.user.UserDTO;

import java.util.Collection;

public interface UserService {

    UserDTO getByEmail(String email);

    Collection<UserDTO> getAll();

    UserDTO create(UserCreateDTO dto);
}
