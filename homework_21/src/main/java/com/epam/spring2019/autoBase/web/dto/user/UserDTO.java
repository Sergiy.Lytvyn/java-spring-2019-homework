package com.epam.spring2019.autoBase.web.dto.user;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Builder
@Getter
@RequiredArgsConstructor
@EqualsAndHashCode
public class UserDTO {

    private final String id;
    private final String name;
    private final String email;
    private final String role;

}
