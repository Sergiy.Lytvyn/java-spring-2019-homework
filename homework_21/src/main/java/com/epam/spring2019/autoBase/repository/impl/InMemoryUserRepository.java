package com.epam.spring2019.autoBase.repository.impl;

import com.epam.spring2019.autoBase.repository.api.UserRepository;
import com.epam.spring2019.autoBase.repository.domain.User;
import com.epam.spring2019.autoBase.repository.exception.UserNotFoundException;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.UUID;

@Repository
public class InMemoryUserRepository implements UserRepository {

    private final Map<UUID, User> userMap;

    public InMemoryUserRepository(Map<UUID, User> userMap) {
        this.userMap = userMap;
    }

    @Override
    public User getByEmail(String email) {
        return userMap.values()
                .stream()
                .filter(user -> user.getEmail().equals(email))
                .findFirst()
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public Collection<User> getAll() {
        return userMap.values();
    }

    @Override
    public User create(User user) {
        userMap.put(user.getId(), user);
        return user;
    }

}
