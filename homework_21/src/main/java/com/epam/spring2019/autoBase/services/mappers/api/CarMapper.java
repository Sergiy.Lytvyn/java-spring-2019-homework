package com.epam.spring2019.autoBase.services.mappers.api;

import com.epam.spring2019.autoBase.repository.domain.Car;
import com.epam.spring2019.autoBase.web.dto.car.CarCreateRequestDTO;
import com.epam.spring2019.autoBase.web.dto.car.CarDTO;
import com.epam.spring2019.autoBase.web.dto.car.CarUpdateRequestDTO;

public interface CarMapper {

    CarDTO toDTO(Car car);
    Car toCar(CarCreateRequestDTO dto);
    Car toCar(CarUpdateRequestDTO dto);
}
