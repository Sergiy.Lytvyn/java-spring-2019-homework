package com.epam.spring2019.autoBase.services.impl;

import com.epam.spring2019.autoBase.services.mappers.api.CarMapper;
import com.epam.spring2019.autoBase.web.dto.car.CarCreateRequestDTO;
import com.epam.spring2019.autoBase.web.dto.car.CarDTO;
import com.epam.spring2019.autoBase.web.dto.car.CarUpdateRequestDTO;
import com.epam.spring2019.autoBase.repository.api.CarRepository;
import com.epam.spring2019.autoBase.repository.domain.Car;
import com.epam.spring2019.autoBase.services.api.CarService;
import com.epam.spring2019.autoBase.services.mappers.impl.CarMapperImpl;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class DefaultCarService implements CarService {

    private final CarMapper carMapper;
    private final CarRepository carRepository;

    public DefaultCarService(CarMapper carMapper, CarRepository carRepository) {
        this.carMapper = carMapper;
        this.carRepository = carRepository;
    }

    @Override
    public CarDTO create(CarCreateRequestDTO dto) {
        Car created = carRepository.create(carMapper.toCar(dto));
        return carMapper.toDTO(created);
    }

    @Override
    public CarDTO updateById(String id, CarUpdateRequestDTO dto) {
        Car updated = carRepository.updateById(UUID.fromString(id), carMapper.toCar(dto));
        return carMapper.toDTO(updated);
    }

    @Override
    public String deleteById(String id) {
        return carRepository.deleteById(UUID.fromString(id)).toString();
    }

    @Override
    public Collection<CarDTO> getAll() {
        Collection<Car> cars = carRepository.getAll();
        return cars.stream()
                .map(carMapper::toDTO)
                .collect(Collectors.toList());
    }

}
