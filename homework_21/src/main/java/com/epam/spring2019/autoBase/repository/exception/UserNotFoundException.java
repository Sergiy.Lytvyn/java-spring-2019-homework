package com.epam.spring2019.autoBase.repository.exception;

public class UserNotFoundException extends ApplicationException {

    @Override
    public String description() {
        return "user not found";
    }
}
