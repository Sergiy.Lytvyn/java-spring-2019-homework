package com.epam.spring2019.autoBase.repository.domain;

import com.epam.spring2019.autoBase.repository.exception.IllegalCarStateException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
@Getter
public enum CarState {
    SERVICE_AVAILABLE("Service able"), BROKEN("Broken");
    private final String description;

    public static CarState stateOf(String description){
        return Stream.of(CarState.values())
                .filter(carState -> carState.description.equals(description))
                .findFirst().orElseThrow(IllegalCarStateException::new);
    }

}
