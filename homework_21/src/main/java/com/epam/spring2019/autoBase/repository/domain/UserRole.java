package com.epam.spring2019.autoBase.repository.domain;

public enum UserRole {
    USER, ADMIN, SUPER_ADMIN;

    public static boolean isAdmin(String role){
        return ADMIN == UserRole.valueOf(role);
    }

    public static boolean isSuperAdmin(String role){return SUPER_ADMIN == UserRole.valueOf(role);}

}
