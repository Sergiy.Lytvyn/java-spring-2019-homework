package com.epam.spring2019.autoBase.web.advice;

import com.epam.spring2019.autoBase.repository.exception.ApplicationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@ControllerAdvice
public class ExceptionHandlerAdvice {

    @ExceptionHandler
    public ModelAndView error(ApplicationException ex){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("error", ex.description());
        modelAndView.setViewName("error");
        return modelAndView;
    }

    @ExceptionHandler
    public ModelAndView error(AccessDeniedException ignored){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("error", "Access denied. Please, firstly login");
        modelAndView.setViewName("error");
        return modelAndView;
    }

    @ExceptionHandler
    public ModelAndView error(Throwable ex){
        log.error("unexpected exception", ex);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("error", "Something went wrong... We will fix this problem");
        modelAndView.setViewName("error");
        return modelAndView;
    }

}