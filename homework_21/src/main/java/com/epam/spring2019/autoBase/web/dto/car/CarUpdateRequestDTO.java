package com.epam.spring2019.autoBase.web.dto.car;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Builder
@EqualsAndHashCode
@RequiredArgsConstructor
@Getter
public class CarUpdateRequestDTO {

    private final String id;
    private final String brand;
    private final String model;
    private final String state;

}
