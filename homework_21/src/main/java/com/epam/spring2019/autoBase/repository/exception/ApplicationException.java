package com.epam.spring2019.autoBase.repository.exception;

public abstract class ApplicationException extends RuntimeException {

    public abstract String description();

}
