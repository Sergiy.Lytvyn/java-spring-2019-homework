package com.epam.spring2019.autoBase.repository.exception;

public class UserAlreadyExistException extends ApplicationException {
    @Override
    public String description() {
        return "User with this email already exist";
    }
}
