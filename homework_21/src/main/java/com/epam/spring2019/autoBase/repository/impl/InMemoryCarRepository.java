package com.epam.spring2019.autoBase.repository.impl;

import com.epam.spring2019.autoBase.repository.api.CarRepository;
import com.epam.spring2019.autoBase.repository.domain.Car;
import com.epam.spring2019.autoBase.repository.exception.CarNotFoundException;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.UUID;

@Repository
public class InMemoryCarRepository implements CarRepository {

    private final Map<UUID, Car> carMap;

    public InMemoryCarRepository(Map<UUID, Car> carMap) {
        this.carMap = carMap;
    }

    @Override
    public Collection<Car> getAll() {
        return carMap.values();
    }

    @Override
    public Car updateById(UUID id, Car updated) {
        if (carMap.containsKey(id)){
            carMap.put(id, updated);
            return updated;
        } else {
            throw new CarNotFoundException();
        }
    }

    @Override
    public Car create(Car car) {
        carMap.put(car.getId(), car);
        return car;
    }

    @Override
    public UUID deleteById(UUID id) {
        if (carMap.containsKey(id)){
            carMap.remove(id);
            return id;
        } else {
            throw new CarNotFoundException();
        }
    }
}
