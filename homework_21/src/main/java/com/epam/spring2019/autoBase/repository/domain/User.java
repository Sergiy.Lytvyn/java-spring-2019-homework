package com.epam.spring2019.autoBase.repository.domain;

import lombok.*;

import java.util.UUID;

@Builder
@EqualsAndHashCode
@Getter
@Setter
public class User {

    private UUID id;
    private String name;
    private String password;
    private String email;
    private UserRole role;

}
