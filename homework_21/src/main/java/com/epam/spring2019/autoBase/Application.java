package com.epam.spring2019.autoBase;

import com.epam.spring2019.autoBase.repository.domain.CarState;
import com.epam.spring2019.autoBase.repository.domain.Car;
import com.epam.spring2019.autoBase.repository.domain.User;
import com.epam.spring2019.autoBase.repository.domain.UserRole;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

import java.util.*;

@SpringBootApplication
public class Application implements WebMvcConfigurer {

    @Bean
    Map<UUID, Car> carMap(){
        Map<UUID, Car> carMap =  new HashMap<>();
        Car first = Car.builder()
                .id(UUID.randomUUID())
                .brand("Mercedes")
                .model("Benz")
                .carState(CarState.SERVICE_AVAILABLE)
                .build();
        carMap.put(first.getId(), first);
        Car second = Car.builder()
                .id(UUID.randomUUID())
                .brand("Mazda")
                .model("M3")
                .carState(CarState.BROKEN)
                .build();
        carMap.put(second.getId(), second);
        return carMap;
    }

    @Bean
    Map<UUID, User> userMap(){
        User user = User.builder()
                .id(UUID.randomUUID())
                .name("User")
                .email("user@email.com")
                .password(passwordEncoder().encode("user"))
                .role(UserRole.USER)
                .build();
        User admin = User.builder()
                .id(UUID.randomUUID())
                .name("Admin")
                .email("admin@email.com")
                .password(passwordEncoder().encode("admin"))
                .role(UserRole.ADMIN)
                .build();
        User superAdmin = User.builder()
                .id(UUID.randomUUID())
                .name("SuperAdmin")
                .email("super-admin@email.com")
                .role(UserRole.SUPER_ADMIN)
                .password(passwordEncoder().encode("superadmin"))
                .build();
        Map<UUID, User> userMap = new HashMap<>();
        userMap.put(user.getId(), user);
        userMap.put(admin.getId(), admin);
        userMap.put(superAdmin.getId(), superAdmin);
        return userMap;
    }

    @Bean
    PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    ITemplateResolver templateResolver() {
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setPrefix("templates/");
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode("HTML");
        return templateResolver;
    }

    @Bean
    SpringTemplateEngine templateEngine(ITemplateResolver templateResolver) {
        SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);
        templateEngine.setEnableSpringELCompiler(true);
        return templateEngine;
    }

    @Bean
    ThymeleafViewResolver viewResolver(SpringTemplateEngine templateEngine) {
        ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
        viewResolver.setTemplateEngine(templateEngine);
        return viewResolver;
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/car/updateForm").setViewName("car/updateForm");
        registry.addViewController("/car/createForm").setViewName("car/createForm");
        registry.addViewController("/user/createForm").setViewName("user/createForm");
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
