package com.epam.spring2019.autoBase.web.controllers;

import com.epam.spring2019.autoBase.services.api.CarService;
import com.epam.spring2019.autoBase.web.dto.car.CarCreateRequestDTO;
import com.epam.spring2019.autoBase.web.dto.car.CarUpdateRequestDTO;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("car")
public class CarController {

    private final CarService carService;

    public CarController(CarService carService) {
        this.carService = carService;
    }

    @Secured({"ROLE_ADMIN","ROLE_USER", "ROLE_SUPER_ADMIN"})
    @GetMapping("all")
    public String getAll(Model model){
        model.addAttribute("cars", carService.getAll());
        return "car/all";
    }

    @Secured({"ROLE_ADMIN","ROLE_SUPER_ADMIN"})
    @PostMapping
    public String create(CarCreateRequestDTO dto, Model model){
        carService.create(dto);
        return getAll(model);
    }

    @Secured({"ROLE_ADMIN","ROLE_SUPER_ADMIN"})
    @DeleteMapping
    public String deleteById(String id, Model model){
        carService.deleteById(id);
        return getAll(model);
    }

    @Secured({"ROLE_ADMIN", "ROLE_SUPER_ADMIN"})
    @PutMapping
    public String update(CarUpdateRequestDTO dto, Model model){
        carService.updateById(dto.getId(), dto);
        return getAll(model);
    }

}