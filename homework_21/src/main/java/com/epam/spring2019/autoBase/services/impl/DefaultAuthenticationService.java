package com.epam.spring2019.autoBase.services.impl;

import com.epam.spring2019.autoBase.repository.api.UserRepository;
import com.epam.spring2019.autoBase.repository.domain.User;
import com.epam.spring2019.autoBase.repository.exception.UserNotFoundException;
import com.epam.spring2019.autoBase.services.mappers.api.UserMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class DefaultAuthenticationService implements UserDetailsService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public DefaultAuthenticationService(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
        try {
            User user = userRepository.getByEmail(username);
            return userMapper.toUserDetails(user);
        } catch (UserNotFoundException ex){
            throw new UsernameNotFoundException(ex.description());
        }
    }
}
