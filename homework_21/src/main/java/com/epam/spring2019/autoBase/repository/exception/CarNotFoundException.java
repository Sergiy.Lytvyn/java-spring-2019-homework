package com.epam.spring2019.autoBase.repository.exception;

public class CarNotFoundException extends ApplicationException{

    @Override
    public String description() {
        return "can not find car";
    }
}
