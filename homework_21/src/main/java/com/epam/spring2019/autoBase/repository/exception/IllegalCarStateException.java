package com.epam.spring2019.autoBase.repository.exception;

public class IllegalCarStateException extends ApplicationException {

    @Override
    public String description() {
        return "illegal car state";
    }
}
