package com.epam.spring2019.autoBase.services.mappers.impl;

import com.epam.spring2019.autoBase.services.mappers.api.CarMapper;
import com.epam.spring2019.autoBase.web.dto.car.CarCreateRequestDTO;
import com.epam.spring2019.autoBase.web.dto.car.CarDTO;
import com.epam.spring2019.autoBase.web.dto.car.CarUpdateRequestDTO;
import com.epam.spring2019.autoBase.repository.domain.Car;
import com.epam.spring2019.autoBase.repository.domain.CarState;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@NoArgsConstructor
public class CarMapperImpl implements CarMapper {

    @Override
    public CarDTO toDTO(Car car){
        return CarDTO.builder()
                .id(car.getId().toString())
                .brand(car.getBrand())
                .model(car.getModel())
                .state(car.getCarState().getDescription())
                .build();
    }

    @Override
    public Car toCar(CarCreateRequestDTO dto){
        return Car.builder()
                .id(UUID.randomUUID())
                .brand(dto.getBrand())
                .model(dto.getModel())
                .carState(CarState.stateOf(dto.getState()))
                .build();
    }

    @Override
    public Car toCar(CarUpdateRequestDTO dto){
        return Car.builder()
                .id(UUID.fromString(dto.getId()))
                .brand(dto.getBrand())
                .model(dto.getModel())
                .carState(CarState.stateOf(dto.getState()))
                .build();
    }

}
