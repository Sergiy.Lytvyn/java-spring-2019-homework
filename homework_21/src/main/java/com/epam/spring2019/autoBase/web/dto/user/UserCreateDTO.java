package com.epam.spring2019.autoBase.web.dto.user;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Builder
@Getter
@EqualsAndHashCode
@RequiredArgsConstructor
public class UserCreateDTO {
    private final String name;
    private final String password;
    private final String role;
    private final String email;
}
