package com.epam.spring2019.autoBase.services.api;

import com.epam.spring2019.autoBase.web.dto.car.CarCreateRequestDTO;
import com.epam.spring2019.autoBase.web.dto.car.CarDTO;
import com.epam.spring2019.autoBase.web.dto.car.CarUpdateRequestDTO;

import java.util.Collection;

public interface CarService {

    CarDTO create(CarCreateRequestDTO dto);

    CarDTO updateById(String id, CarUpdateRequestDTO dto);

    String deleteById(String id);

    Collection<CarDTO> getAll();

}
