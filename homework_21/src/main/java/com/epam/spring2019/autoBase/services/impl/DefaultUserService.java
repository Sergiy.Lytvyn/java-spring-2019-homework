package com.epam.spring2019.autoBase.services.impl;

import com.epam.spring2019.autoBase.repository.api.UserRepository;
import com.epam.spring2019.autoBase.repository.domain.User;
import com.epam.spring2019.autoBase.repository.exception.UserAlreadyExistException;
import com.epam.spring2019.autoBase.repository.exception.UserNotFoundException;
import com.epam.spring2019.autoBase.services.api.UserService;
import com.epam.spring2019.autoBase.services.mappers.api.UserMapper;
import com.epam.spring2019.autoBase.services.mappers.impl.UserMapperImpl;
import com.epam.spring2019.autoBase.web.dto.user.UserCreateDTO;
import com.epam.spring2019.autoBase.web.dto.user.UserDTO;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class DefaultUserService implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public DefaultUserService(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    @Override
    public UserDTO getByEmail(String email) {
        return userMapper.toDTO(userRepository.getByEmail(email));
    }

    @Override
    public Collection<UserDTO> getAll() {
        return userRepository.getAll().stream()
                .map(userMapper::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    public UserDTO create(UserCreateDTO dto) {
        try {
            userRepository.getByEmail(dto.getEmail());
            throw new UserAlreadyExistException();
        } catch (UserNotFoundException ignored){
            User created = userRepository.create(userMapper.toUser(dto));
            return userMapper.toDTO(created);
        }
    }

}
