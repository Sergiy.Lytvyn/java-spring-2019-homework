package com.epam.spring2019.autoBase.repository.domain;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Builder
@Getter
@Setter
@EqualsAndHashCode
public class Car {

    private UUID id;
    private String brand;
    private String model;
    private CarState carState;

}
