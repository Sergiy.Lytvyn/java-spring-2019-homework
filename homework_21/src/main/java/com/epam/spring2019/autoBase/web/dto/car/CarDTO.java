package com.epam.spring2019.autoBase.web.dto.car;

import lombok.*;

@Builder
@Getter
@RequiredArgsConstructor
@EqualsAndHashCode
public class CarDTO {

    private final String id;
    private final String brand;
    private final String model;
    private final String state;

}
