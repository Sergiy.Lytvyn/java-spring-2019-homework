package com.epam.spring2019.autoBase.web.controllers;

import com.epam.spring2019.autoBase.services.api.UserService;
import com.epam.spring2019.autoBase.web.dto.user.UserCreateDTO;
import com.epam.spring2019.autoBase.web.dto.user.UserDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = UserController.class)
class UserControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserService userService;

    private final UserDTO superAdmin = UserDTO.builder()
            .id(UUID.randomUUID().toString())
            .email("super-admin@email.com")
            .name("any")
            .role("SUPER_ADMIN")
            .build();

    private final UserDTO admin = UserDTO.builder()
            .id(UUID.randomUUID().toString())
            .email("admin@email.com")
            .name("any")
            .role("ADMIN")
            .build();

    private final UserDTO user = UserDTO.builder()
            .id(UUID.randomUUID().toString())
            .email("user@email.com")
            .name("any")
            .role("USER")
            .build();

    @WithMockUser(username = "admin@email.com", roles = "ADMIN")
    @Test
    void adminProfile() throws Exception {
        //GIVEN
        given(userService.getByEmail(admin.getEmail())).willReturn(admin);
        //THEN
        mockMvc.perform(get("/user/profile"))
                .andExpect(status().isOk())
                .andExpect(view().name("admin/profile"))
                .andExpect(model().attribute("user", equalTo(admin)));
        verify(userService, times(1)).getByEmail(admin.getEmail());
    }

    @WithMockUser(username = "user@email.com", roles = "USER")
    @Test
    void userProfile() throws Exception {
        //GIVEN
        given(userService.getByEmail(user.getEmail())).willReturn(user);
        //THEN
        mockMvc.perform(get("/user/profile"))
                .andExpect(status().isOk())
                .andExpect(view().name("user/profile"))
                .andExpect(model().attribute("user", equalTo(user)));
        verify(userService, times(1)).getByEmail(user.getEmail());
    }

    @WithMockUser(username = "super-admin@email.com", roles = "SUPER_ADMIN")
    @Test
    void superAdminProfile() throws Exception {
        //GIVEN
        given(userService.getByEmail(superAdmin.getEmail())).willReturn(superAdmin);
        //THEN
        mockMvc.perform(get("/user/profile"))
                .andExpect(status().isOk())
                .andExpect(view().name("superAdmin/profile"))
                .andExpect(model().attribute("user", equalTo(superAdmin)));
        verify(userService, times(1)).getByEmail(superAdmin.getEmail());
    }

    @WithMockUser(roles = "SUPER_ADMIN")
    @Test
    void create() throws Exception {
        //GIVEN
        final UserCreateDTO createDTO = UserCreateDTO.builder()
                .email("new-user@email.com")
                .name("any")
                .password("any")
                .role("USER")
                .build();
        given(userService.create(createDTO)).willReturn(any());
        //THAN
        mockMvc.perform(post("/user")
                .param("email", createDTO.getEmail())
                .param("name", createDTO.getName())
                .param("password", createDTO.getPassword())
                .param("role", createDTO.getRole()))
                .andExpect(status().isOk())
                .andExpect(view().name("user/all"));
    }
}