package com.epam.spring2019.autoBase.repository.impl;

import com.epam.spring2019.autoBase.repository.domain.Car;
import com.epam.spring2019.autoBase.repository.domain.CarState;
import com.epam.spring2019.autoBase.repository.exception.CarNotFoundException;
import org.junit.jupiter.api.Test;
import org.mockito.internal.util.collections.Sets;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class InMemoryCarRepositoryTest {

    private final Map carMap = mock(Map.class);
    private final InMemoryCarRepository sut = new InMemoryCarRepository(carMap);

    private final Car first = Car.builder()
            .id(UUID.randomUUID())
            .brand("Fiat")
            .model("Doblo")
            .carState(CarState.SERVICE_AVAILABLE)
            .build();

    private final Car second = Car.builder()
            .id(UUID.randomUUID())
            .brand("KIA")
            .model("Rio")
            .carState(CarState.BROKEN)
            .build();

    @Test
    void getAll() {
        //GIVEN
        when(carMap.values()).thenReturn(Arrays.asList(first,second));
        //WHEN
        Collection<Car> cars = sut.getAll();
        //THAN
        assertTrue(cars.contains(first));
        assertTrue(cars.contains(second));
        assertEquals(2, cars.size());
        verify(carMap, times(1)).values();
    }

    @Test
    void updateById() {
        //GIVEN
        UUID random = UUID.randomUUID();
        when(carMap.containsKey(first.getId())).thenReturn(true);
        when(carMap.containsKey(second.getId())).thenReturn(true);
        when(carMap.containsKey(random)).thenReturn(false);
        //THAN
        assertEquals(first, sut.updateById(first.getId(), first));
        assertEquals(second, sut.updateById(second.getId(), second));
        assertThrows(CarNotFoundException.class, () -> sut.updateById(random, first));
        verify(carMap, times(1)).containsKey(first.getId());
        verify(carMap, times(1)).containsKey(second.getId());
    }

    @Test
    void create() {
        //GIVEN
        when(carMap.keySet()).thenReturn(Sets.newSet(first, second));
        first.setId(UUID.randomUUID());
        //THAN
        assertEquals(first, sut.create(first));
    }

    @Test
    void deleteById() {
        //GIVEN
        UUID random = UUID.randomUUID();
        when(carMap.containsKey(first.getId())).thenReturn(true);
        when(carMap.containsKey(random)).thenReturn(false);
        UUID firstId = first.getId();
        //THAN
        assertEquals(firstId, sut.deleteById(first.getId()));
        assertThrows(CarNotFoundException.class, () -> sut.deleteById(random));
        verify(carMap, times(1)).containsKey(firstId);
        verify(carMap, times(1)).containsKey(random);
    }
}