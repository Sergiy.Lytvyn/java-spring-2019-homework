package com.epam.spring2019.autoBase.web.controllers;

import com.epam.spring2019.autoBase.repository.domain.CarState;
import com.epam.spring2019.autoBase.services.api.CarService;
import com.epam.spring2019.autoBase.web.dto.car.CarCreateRequestDTO;
import com.epam.spring2019.autoBase.web.dto.car.CarDTO;
import com.epam.spring2019.autoBase.web.dto.car.CarUpdateRequestDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WithMockUser(roles = "ADMIN")
@WebMvcTest(controllers = CarController.class)
class CarControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    CarService carService;

    private final CarDTO first = CarDTO.builder()
            .id(UUID.randomUUID().toString())
            .brand("Mercedes")
            .model("Benz")
            .state(CarState.SERVICE_AVAILABLE.getDescription())
            .build();
    private final CarDTO second = CarDTO.builder()
            .id(UUID.randomUUID().toString())
            .brand("Mazda")
            .model("M3")
            .state(CarState.BROKEN.getDescription())
            .build();

    @Test
    void getAll() throws Exception {
        //GIVEN
        given(carService.getAll()).willReturn(Arrays.asList(first,second));
        //THEN
        mockMvc.perform(get("/car/all"))
                .andExpect(status().isOk())
                .andExpect(view().name("car/all"))
                .andExpect(model().attribute("cars", hasItems(first,second)))
                .andExpect(model().attribute("cars", hasSize(2)));
        verify(carService, times(1)).getAll();
    }

    @Test
    void create() throws Exception {
        //GIVEN
        final CarCreateRequestDTO dto = CarCreateRequestDTO.builder()
                .brand(first.getBrand())
                .model(first.getModel())
                .state(first.getState())
                .build();
        given(carService.create(dto)).willReturn(first);
        given(carService.getAll()).willReturn(any());
        //THAN
        mockMvc.perform(post("/car")
                .param("brand", first.getBrand())
                .param("model",first.getModel())
                .param("state", "Service able"))
                .andExpect(status().isOk())
                .andExpect(view().name("car/all"));
        verify(carService, times(1)).create(dto);
        verify(carService, times(1)).getAll();
    }

    @Test
    void deleteById() throws Exception {
        //GIVEN
        given(carService.deleteById(first.getId())).willReturn(first.getId());
        given(carService.getAll()).willReturn(Collections.singletonList(second));
        //THAN
        mockMvc.perform(delete("/car")
                .param("id", first.getId()))
                .andExpect(status().isOk())
                .andExpect(view().name("car/all"));
        verify(carService, times(1)).deleteById(first.getId());
        verify(carService, times(1)).getAll();
    }

    @Test
    void update() throws Exception {
        //GIVEN
        final CarUpdateRequestDTO dto = CarUpdateRequestDTO.builder()
                .id(first.getId())
                .model(first.getModel())
                .brand(first.getBrand())
                .state(first.getState())
                .build();
        given(carService.updateById(dto.getId(), dto)).willReturn(first);
        given(carService.getAll()).willReturn(Arrays.asList(first, second));
        //THEN
        mockMvc.perform(put("/car")
                .param("id", dto.getId())
                .param("model", dto.getModel())
                .param("brand", dto.getBrand())
                .param("state", dto.getState()))
                .andExpect(status().isOk())
                .andExpect(view().name("car/all"));
        verify(carService, times(1)).updateById(dto.getId(), dto);
        verify(carService, times(1)).getAll();
    }
}