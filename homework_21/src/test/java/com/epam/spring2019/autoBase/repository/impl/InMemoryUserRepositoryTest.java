package com.epam.spring2019.autoBase.repository.impl;

import com.epam.spring2019.autoBase.repository.domain.User;
import com.epam.spring2019.autoBase.repository.domain.UserRole;
import com.epam.spring2019.autoBase.repository.exception.UserNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class InMemoryUserRepositoryTest {

    private final Map userMap = mock(Map.class);

    private InMemoryUserRepository sut = new InMemoryUserRepository(userMap);

    private final User user = User.builder()
            .id(UUID.randomUUID())
            .email("user@email.com")
            .name("user")
            .password("user")
            .role(UserRole.USER)
            .build();

    private final User admin = User.builder()
            .id(UUID.randomUUID())
            .email("admin@email.com")
            .name("admin")
            .password("admin")
            .role(UserRole.ADMIN)
            .build();

    @Test
    void getByEmail() {
        //GIVEN
        when(userMap.values()).thenReturn(Arrays.asList(user, admin));
        //THAN
        assertEquals(user, sut.getByEmail(user.getEmail()));
        assertEquals(admin, sut.getByEmail(admin.getEmail()));
        assertThrows(UserNotFoundException.class, ()-> sut.getByEmail("bad@email.com"));
        verify(userMap, times(3)).values();
    }

}