package util;

import java.util.NoSuchElementException;

public class CustomLinkedList<E> implements CustomList<E>{

    private int size;
    private Node<E> first;
    private Node<E> last;

    public CustomLinkedList() {

    }

    private void checkIndex(int index){
        if (index < 0 || index >= size()){
            throw new IndexOutOfBoundsException();
        }
    }

    private Node<E> node (int index){
        checkIndex(index);
        if (index < (size >> 1)){
            Node<E> x = first;
            for (int i = 0; i < index; i++){
                x = x.next;
            }
            return x;
        } else {
            Node<E> x = last;
            for (int i = (size - 1); i > index; i--){
                x = x.prev;
            }
            return x;
        }
    }

    private void checkEmpty (){
            if (isEmpty()) {
                throw new NoSuchElementException();
            }
    }

        @Override
        public E get(int index) {
            return node(index).item;
        }

        @Override public void set(int index, E e) {
        final Node<E> node = node(index);
            node.item = e;
        }

        @Override
        public void add(int index, E e) {
            if (index == size) {
                addLast(e);
            } else if(index == 0){
                addFirst(e);
            } else {
                final Node<E> node = node(index);
                final Node<E> prevNode = node.prev;
                Node<E> newNode = new Node<>(prevNode, e, node);
                prevNode.next = newNode;
                node.prev = newNode;
                size++;
            }
        }

        @Override
        public E remove(int index) {
            if(index == (size - 1)){
                return removeLast();
            } else if (index == 0){
                return removeFirst();
            } else {
                Node<E> removedNode = node(index);
                final Node<E> prev = removedNode.prev;
                final Node<E> next = removedNode.next;
                E item = removedNode.item;
                removedNode.prev = null;
                removedNode.next = null;
                prev.next = next;
                next.prev = prev;
                removedNode = null;
                size--;
                return item;
            }
        }

        @Override
        public int indexOf(E e) {
            int index = 0;
            if (e == null) {
                for (Node<E> x = first; x != null; x = x.next) {
                    if (x.item == null)
                        return index;
                    index++;
                }
            } else {
                for (Node<E> x = first; x != null; x = x.next) {
                    if (e.equals(x.item))
                        return index;
                    index++;
                }
            }
            return -1;
        }

        @Override
        public int size() {
            return size;
        }

        @Override
        public boolean add(E e) {
            addLast(e);
            return true;
        }

        @Override
        public boolean remove(E e) {
            int index = indexOf(e);
            if (index == -1){
                return false;
            } else {
                remove(index);
                return true;
            }
        }

        @Override
        public boolean contains(E e) {
            return indexOf(e) != -1;
        }

        @Override
        public boolean isEmpty(){
            return size == 0;
        }

        public void addFirst(E e){
            final Node<E> f = first;
            final Node<E> newNode = new Node<>(null, e, f);
            first = newNode;
            if (f == null){
                last = newNode;
            } else {
                f.prev = newNode;
            }
            size++;
        }

        public void addLast(E e){
            final Node<E> l = last;
            final Node<E> newNode = new Node<>(l, e, null);
            last = newNode;
            if (l == null){
                first = newNode;
            } else {
                l.next = newNode;
            }
            size++;
        }

        public E removeFirst(){
            checkEmpty();
            Node<E> removedNode = first;
            Node<E> nextNode = first.next;
            E item = removedNode.item;
            if(nextNode == null){
                last = null;
                first = null;
            } else {
                nextNode.prev = null;
                removedNode.next = null;
                first = nextNode;
                removedNode = null;  //for GC
            }
            size--;
            return item;
        }

        public E removeLast(){
            checkEmpty();
            Node<E> removedNode = last;
            Node<E> prevNode = last.prev;
            E item = removedNode.item;
            if (prevNode == null){
                first = null;
                last = null;
            } else {
                prevNode.next = null;
                removedNode.prev = null;
                last = prevNode;
                removedNode = null; //for GC
            }
            size--;
            return item;
        }
        public E getFirst(){
            checkEmpty();
        return first.item;
    }

    public E getLast(){
        checkEmpty();
        return last.item;
    }

    private static class Node<E>{

        Node<E> next;
        Node<E> prev;
        E item;

            Node(Node<E> prev, E item , Node<E> next){
                this.next = next;
                this.prev = prev;
                this.item = item;
        }
    }

}
