package util;

public interface CustomCollection<E> {

    int size();

    boolean add(E e);

    boolean remove(E c);

    boolean contains(E e);

    boolean isEmpty();

}

