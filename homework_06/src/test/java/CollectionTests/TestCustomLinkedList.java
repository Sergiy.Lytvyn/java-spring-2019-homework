package CollectionTests;

import org.hamcrest.Matchers;
import org.junit.*;
import org.junit.rules.ExpectedException;
import util.CustomLinkedList;
import java.util.NoSuchElementException;

public class TestCustomLinkedList {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private CustomLinkedList<Integer> listOfInteger = new CustomLinkedList<>();

    private void addSomeIntegers(){
        listOfInteger.add(100);
        listOfInteger.add(200);
        listOfInteger.add(300);
        listOfInteger.add(400);
        listOfInteger.add(500);
        listOfInteger.add(600);
        listOfInteger.add(null);
    }

    @Test
    public void testAddWithoutIndex(){
        //WHEN
        listOfInteger.add(10);
        listOfInteger.add(20);
        listOfInteger.add(30);
        //THEN
        Assert.assertEquals(3, listOfInteger.size());
        Assert.assertEquals(10, listOfInteger.get(0).intValue());
        Assert.assertEquals(20, listOfInteger.get(1).intValue());
        Assert.assertEquals(30, listOfInteger.get(2).intValue());
    }

    @Test
    public void testGet(){
        //WHEN
        addSomeIntegers();
        //THEN
        Assert.assertThat(100, Matchers.is(listOfInteger.get(0)));
        Assert.assertThat(600, Matchers.is(listOfInteger.get(5)));
        Assert.assertThat(300, Matchers.is(listOfInteger.get(2)));
        Assert.assertNull(listOfInteger.get(6));
    }

    @Test
    public void testGetFirst(){
        //WHEN
        addSomeIntegers();
        //THEN
        Assert.assertEquals(100, listOfInteger.get(0).intValue());
    }

    @Test
    public void testGetFirstWithEmptyList(){
        //GIVEN
        expectedException.expect(NoSuchElementException.class);
        //WHEN
        listOfInteger.getFirst();
    }

    @Test
    public void testGetLast(){
        //WHEN
        listOfInteger.add(555);
        //THEN
        Assert.assertThat(555, Matchers.is(listOfInteger.getLast()));
    }

    @Test
    public void testGetLastWithEmptyList(){
        //GIVEN
        expectedException.expect(NoSuchElementException.class);
        //WHEN
        listOfInteger.getLast();
    }

    @Test
    public void testAddFirst(){
        //GIVEN
        Integer afterAddFirst;
        //WHEN
        addSomeIntegers();
        listOfInteger.addFirst(55);
        afterAddFirst = listOfInteger.get(0);
        listOfInteger.addFirst(null);
        //THEN
        Assert.assertThat(55, Matchers.is(afterAddFirst));
        Assert.assertNull(listOfInteger.get(0));
    }

    @Test
    public void testAddLast(){
        //WHEN
        listOfInteger.addLast(0);
        listOfInteger.addLast(null);
        listOfInteger.addLast(99);
        //THEN
        Assert.assertNull(listOfInteger.get(1));
        Assert.assertThat(0, Matchers.is(listOfInteger.get(0)));
        Assert.assertThat(99, Matchers.is(listOfInteger.get(2)));
    }

    @Test
    public void testAddWithIllegalIndex(){
        //GIVEN
        expectedException.expect(IndexOutOfBoundsException.class);
        //WHEN
        listOfInteger.add(-1, 10);
    }

    @Test
    public void testAddWithIndexWithBiggerThanSize(){
        //GIVEN
        expectedException.expect(IndexOutOfBoundsException.class);
        //WHEN
        addSomeIntegers();
        listOfInteger.add(30, 1000);
    }

    @Test
    public void testAddWithIndex(){
        //GIVEN
        addSomeIntegers();
        Integer first = listOfInteger.getFirst();
        Integer last = listOfInteger.getLast();
        int sizeBefore = listOfInteger.size();
        //WHEN
        listOfInteger.add(1, -33);
        listOfInteger.add(1, 66);
        listOfInteger.add(1, null);
        //THEN
        Assert.assertEquals(first, listOfInteger.getFirst());
        Assert.assertEquals(last, listOfInteger.getLast());
        Assert.assertEquals(sizeBefore, (listOfInteger.size() - 3));
        Assert.assertNull(listOfInteger.get(1));
        Assert.assertThat(66, Matchers.is(listOfInteger.get(2)));
        Assert.assertThat(-33, Matchers.is(listOfInteger.get(3)));
    }

    @Test
    public void testSetWithIllegalIndex(){
       //GIVEN
       expectedException.expect(IndexOutOfBoundsException.class);
       //WHEN
        listOfInteger.set(-2, 789);
    }

    @Test
    public void testSetWithIndexBiggerThanSize(){
        //GIVEN
        expectedException.expect(IndexOutOfBoundsException.class);
        addSomeIntegers();
        //WHEN
        listOfInteger.set(999, 789);
    }

    @Test
    public void testSet(){
        //GIVEN
        addSomeIntegers();
        Integer getValue;
        int size = listOfInteger.size();
        //WHEN
        listOfInteger.set(5, null);
        getValue = listOfInteger.get(5);
        listOfInteger.set(5, 55);
        //THEN
        Assert.assertNull(getValue);
        Assert.assertThat(55, Matchers.is(listOfInteger.get(5)));
        Assert.assertEquals(size, listOfInteger.size());
    }

    @Test
    public void testIndexOf(){
        //GIVEN
        addSomeIntegers();
        //WHEN
        listOfInteger.add(5, 999);
        //THEN
        Assert.assertEquals(0, listOfInteger.indexOf(100));
        Assert.assertEquals(7, listOfInteger.indexOf(null));
        Assert.assertEquals(5, listOfInteger.indexOf(999));
        Assert.assertEquals(-1, listOfInteger.indexOf(555));
        Assert.assertEquals(-1, listOfInteger.indexOf(-1000));
    }

    @Test
    public void testRemoveWithIndex(){
        //GIVEN
        addSomeIntegers();
        Integer removedValue;
        int sizeBefore = listOfInteger.size();
        //WHEN
        removedValue = listOfInteger.remove(0);
        listOfInteger.remove(5);
        //THEN
        Assert.assertEquals(sizeBefore, (listOfInteger.size() + 2));
        Assert.assertThat(100, Matchers.is(removedValue));
        Assert.assertEquals(-1, listOfInteger.indexOf(100));
    }

    @Test
    public void testRemoveWithIllegalIndex(){
        //GIVEN
        addSomeIntegers();
        expectedException.expect(IndexOutOfBoundsException.class);
        //WHEN
        listOfInteger.remove(-1);
    }

    @Test
    public void testRemoveWithIndexBiggerThanSize(){
        //GIVEN
        addSomeIntegers();
        expectedException.expect(IndexOutOfBoundsException.class);
        //WHEN
        listOfInteger.remove(555);
    }

    @Test
    public void testRemoveObject(){
        //GIVEN
        addSomeIntegers();
        int sizeBefore = listOfInteger.size();
        boolean markerFalse;
        boolean markerTrue;
        //WHEN
        markerTrue = listOfInteger.remove(Integer.valueOf(100));
        markerFalse = listOfInteger.remove(Integer.valueOf(-999));
        //THEN
        Assert.assertEquals(sizeBefore, (listOfInteger.size() + 1));
        Assert.assertTrue(markerTrue);
        Assert.assertFalse(markerFalse);
        Assert.assertEquals(-1, listOfInteger.indexOf(100));
    }

    @Test
    public void testContains(){
        //GIVEN
        addSomeIntegers();
        boolean markerFalse;
        boolean markerTrue;
        //WHEN
        markerFalse = listOfInteger.contains(-1);
        markerTrue = listOfInteger.contains(100);
        //THEN
        Assert.assertTrue(markerTrue);
        Assert.assertFalse(markerFalse);
    }

    @Test
    public void testIsEmpty(){
        //GIVEN
        boolean markerFalse;
        boolean markerTrue;
        //WHEN
        markerTrue = listOfInteger.isEmpty();
        addSomeIntegers();
        markerFalse = listOfInteger.isEmpty();
        //THEN
        Assert.assertFalse(markerFalse);
        Assert.assertTrue(markerTrue);
    }

    @Test
    public void testRemoveLast(){
        //GIVEN
        addSomeIntegers();
        int sizeBefore = listOfInteger.size();
        Integer removedValue;
        Integer lastValue = listOfInteger.getLast();
        //WHEN
        removedValue = listOfInteger.removeLast();
        //THEN
        Assert.assertEquals(sizeBefore, (listOfInteger.size() + 1));
        Assert.assertNull(removedValue);
        Assert.assertNotEquals(lastValue, listOfInteger.getLast());
    }

    @Test
    public void testRemoveLastInEmptyList(){
        //GIVEN
        expectedException.expect(NoSuchElementException.class);
        //WHEN
        listOfInteger.removeLast();
    }

    @Test
    public void testRemoveFirst(){
        //GIVEN
        addSomeIntegers();
        int sizeBefore = listOfInteger.size();
        Integer removedValue;
        Integer firstValue = listOfInteger.getFirst();
        //WHEN
        removedValue = listOfInteger.removeFirst();
        //THEN
        Assert.assertEquals(sizeBefore, (listOfInteger.size() + 1));
        Assert.assertThat(100, Matchers.is(removedValue));
        Assert.assertNotEquals(firstValue, listOfInteger.getFirst());
    }

    @Test
    public void testRemoveFirstInEmptyList(){
        //GIVEN
        expectedException.expect(NoSuchElementException.class);
        //WHEN
        listOfInteger.removeFirst();
    }
}
