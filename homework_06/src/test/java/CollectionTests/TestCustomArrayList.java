package CollectionTests;

import org.hamcrest.Matchers;
import org.junit.*;
import org.junit.rules.ExpectedException;
import util.CustomArrayList;

public class TestCustomArrayList {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private CustomArrayList<Integer> listOfInteger = new CustomArrayList<>();

    private void addSomeIntegers(){
        listOfInteger.add(100);
        listOfInteger.add(200);
        listOfInteger.add(300);
        listOfInteger.add(400);
        listOfInteger.add(500);
        listOfInteger.add(600);
        listOfInteger.add(null);
    }

    @Test
    public void createNewCustomArrayListWithIllegalCapacity(){
        //GIVEN
        int capacity = -1;
        expectedException.expect(IllegalArgumentException.class);
        //WHEN
        listOfInteger = new CustomArrayList<>(capacity);
    }

    @Test
    public void testGet(){
        //GIVEN
        addSomeIntegers();
        Integer first;
        Integer second;
        Integer third;
        Integer nullValue;
        //WHEN
        first = listOfInteger.get(0);
        second = listOfInteger.get(1);
        third = listOfInteger.get(2);
        nullValue = listOfInteger.get(6);
        //THEN
        Assert.assertThat(100, Matchers.is(first));  // I use Matchers here, because i must use Matchers in this task.
        Assert.assertThat(200, Matchers.is(second)); // But i think this cloud be:
        Assert.assertThat(300, Matchers.is(third));  // Assert.assertEquals(100, first.intValue());
        Assert.assertNull(nullValue);
    }

    @Test
    public void testGetWithIllegalIndex(){
        //GIVEN
        int capacity = -1;
        expectedException.expect(IndexOutOfBoundsException.class);
        //WHEN
        listOfInteger.get(capacity);
    }

    @Test
    public void testAddWithoutIndex(){
        //GIVEN
        int size = listOfInteger.size();
        int sizeAfterAdd;
        Integer value = 0;
        //WHEN
        listOfInteger.add(value);
        sizeAfterAdd = listOfInteger.size();
        //THEN
        Assert.assertThat(1, Matchers.is(sizeAfterAdd - size));
        Assert.assertThat(value, Matchers.is(listOfInteger.get(0)));
    }

    @Test
    public void testAddWithIndex(){
        //GIVEN
        addSomeIntegers();
        int index = 2;
        Integer value = 999;
        Integer valueOfIndexBeforeAdd = listOfInteger.get(index);
        int sizeBefore = listOfInteger.size();
        //WHEN
        listOfInteger.add(index, value);
        //THEN
        Assert.assertThat(listOfInteger.get(index), Matchers.is(value));
        Assert.assertThat(listOfInteger.size(), Matchers.is(sizeBefore + 1));
        Assert.assertThat(valueOfIndexBeforeAdd, Matchers.is(listOfInteger.get(index + 1)));
    }

    @Test
    public void testAddWhenCapacityChanged(){
        //GIVEN
        listOfInteger = new CustomArrayList<>(1);
        //WHEN
        listOfInteger.add(10);
        listOfInteger.add(20);
        listOfInteger.add(30);
        listOfInteger.add(40);
        //THEN
        Assert.assertThat(4, Matchers.is(listOfInteger.size()));
        Assert.assertThat(40, Matchers.is(listOfInteger.get(3)));
    }

    @Test
    public void testAddWithIndexBiggerThenCapacity(){
        //GIVEN
        addSomeIntegers();
        int index = listOfInteger.size();
        Integer value = 300;
        expectedException.expect(IndexOutOfBoundsException.class);
        //WHEN
        listOfInteger.add((index + 1), value);
    }

    @Test
    public void testAddWithIllegalIndex(){
        //GIVEN
        int index = -1;
        Integer value = 300;
        addSomeIntegers();
        expectedException.expect(IndexOutOfBoundsException.class);
        //WHEN
        listOfInteger.add(index, value);
    }

    @Test
    public void testSet(){
        //GIVEN
        addSomeIntegers();
        Integer value = 33;
        int index = 0;
        int sizeBeforeSet = listOfInteger.size();
        //WHEN
        listOfInteger.set(index, value);
        //THEN
        Assert.assertThat(value, Matchers.is(listOfInteger.get(index)));
        Assert.assertThat(sizeBeforeSet, Matchers.is(listOfInteger.size()));
    }

    @Test
    public void testSetWithIllegalIndex(){
        //GIVEN
        expectedException.expect(IndexOutOfBoundsException.class);
        //WHEN
        listOfInteger.set(0, 2);
    }

    @Test
    public void testIndexOf(){
        //GIVEN
        addSomeIntegers();
        listOfInteger.add(0,null);
        Integer indexBefore = 1;
        Integer indexAfter;
        Integer value = listOfInteger.get(indexBefore);
        //WHEN
        indexAfter = listOfInteger.indexOf(value);
        //THEN
        Assert.assertEquals(indexBefore, indexAfter);
        Assert.assertThat(-1, Matchers.is(listOfInteger.indexOf(-50)));
        Assert.assertThat(0, Matchers.is(listOfInteger.indexOf(null)));
    }

    @Test
    public void testRemoveWithIndex(){
        //GIVEN
        addSomeIntegers();
        Integer value = listOfInteger.get(0);
        Integer nextValue = listOfInteger.get(1);
        Integer removedValue;
        int sizeBefore = listOfInteger.size();
        int sizeAfter;
        //WHEN
        removedValue = listOfInteger.remove(0);
        sizeAfter = listOfInteger.size();
        //THEN
        Assert.assertEquals(value, removedValue);
        Assert.assertEquals(nextValue, listOfInteger.get(0));
        Assert.assertEquals(sizeAfter, (sizeBefore - 1));
        Assert.assertNull(listOfInteger.remove(5));
    }

    @Test
    public void testRemoveWithIllegalIndex(){
        //GIVEN
        expectedException.expect(IndexOutOfBoundsException.class);
        //WHEN
        listOfInteger.remove(-1);
    }

    @Test
    public void testRemoveWithIndexBiggerThanCapacity(){
        //GIVEN
        addSomeIntegers();
        expectedException.expect(IndexOutOfBoundsException.class);
        //WHEN
        listOfInteger.remove(100);
    }

    @Test
    public void testRemoveObject(){
        //GIVEN
        addSomeIntegers();
        Integer firstValue = 100;
        Integer secondValue = 200;
        Integer otherValue = -33;
        int sizeBefore = listOfInteger.size();
        int sizeAfter;
        boolean markerTrue;
        boolean markerFalse;
        //WHEN
        markerTrue = listOfInteger.remove(firstValue);
        markerFalse = listOfInteger.remove(otherValue);
        listOfInteger.remove(secondValue);
        listOfInteger.remove(null);
        sizeAfter = listOfInteger.size();
        //THEN
        Assert.assertEquals(sizeAfter, (sizeBefore - 3));
        Assert.assertEquals(-1, listOfInteger.indexOf(null));
        Assert.assertEquals(0, listOfInteger.indexOf(300));
        Assert.assertTrue(markerTrue);
        Assert.assertFalse(markerFalse);
    }

    @Test
    public void testContains(){
        //GIVEN
        addSomeIntegers();
        boolean markerFalse;
        boolean markerTrue;
        //WHEN
        markerTrue = listOfInteger.contains(100);
        markerFalse = listOfInteger.contains(-33);
        //THEN
        Assert.assertTrue(markerTrue);
        Assert.assertFalse(markerFalse);
    }

    @Test
    public void testIsEmpty(){
        //GIVEN
        boolean markerFalse;
        boolean markerTrue;
        //WHEN
        markerTrue = listOfInteger.isEmpty();
        addSomeIntegers();
        markerFalse = listOfInteger.isEmpty();
        //THEN
        Assert.assertFalse(markerFalse);
        Assert.assertTrue(markerTrue);
    }

}


