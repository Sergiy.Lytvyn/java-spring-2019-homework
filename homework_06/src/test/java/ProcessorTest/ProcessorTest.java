package ProcessorTest;

import org.junit.*;
import org.junit.rules.ExpectedException;
import processor.*;
import org.mockito.*;

public class ProcessorTest {

    @Mock
    Producer producerMock;

    @Mock
    Consumer consumerMock;

    @InjectMocks
    Processor processor = new Processor();

    @Captor
    ArgumentCaptor<String> captor;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testProduce() {
        //GIVEN
        String value = "Magic value";
        String result;
        Mockito.when(producerMock.produce()).thenReturn(value);
        //WHEN
        processor.process();
        //THEN
        Mockito.verify(consumerMock).consume(captor.capture());
        result = captor.getValue();
        Mockito.verify(producerMock, Mockito.times(1)).produce();
        Mockito.verify(consumerMock, Mockito.times(1)).consume(value);
        Assert.assertNotNull(result);
        Assert.assertEquals(result, value);
    }

    @Test
    public void testProduceWithIllegalReturn() {
        //GIVEN
        expectedException.expect(IllegalStateException.class);
        Mockito.when(producerMock.produce()).thenReturn(null);
        //WHEN
        processor.process();
    }

}
